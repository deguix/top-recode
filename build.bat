@echo off

echo Compiling top-recode game sources
echo ----------------------------------------
echo (pass --help as a parameter to see help)
echo 

goto :set_param_vars

:help
echo Usage: build.bat [options]
echo - Compiles/Updates all code, language files and bin files,
echo and creates all symlinks for this project.
echo - Requires admin rights only for first run and any arch
echo change (from x86 to x64 for example).
echo 
echo Options:
echo 
echo --help         - Shows this help screen.
echo --buildonly    - Won't rebuild project from scratch.
echo --debug        - Compiles the debug of projects instead
echo                Note: Luajit debug files will replace
echo                release files!
echo --default_mod  - Uses the "default" lua server scripts
echo                (they are in alpha at the moment).
echo                Requires SQL Server to be running!
echo --pull         - Git pulls project before starting.
echo                Won't work if there are modified files.
echo --x64    		- Will use x64 binaries everywhere (WIP).
echo --dx9    		- Will use dx9 binaries everywhere (WIP).
echo                Can't use --dx9ex with this.
echo --dx9ex    	- Will use dx9ex binaries everywhere (WIP).
echo                Can't use --dx9 with this.
echo --nodb    		- Won't rebuild databases.
echo --compshader   - Compiles the shaders from hlsl files.
echo                Only needed if changing them.
echo                Will also convert them to DX8 if
echo                DX9 option is not set. Needs
echo                DXSDK(Aug 2007), otherwise it errors out.
echo --dep          - Uses files from dep folder instead of
echo                top-recode-dep-binaries
echo --comp-deps    - Compiles all dependencies without using
echo                dependency files provided by
echo                top-recode-dep-binaries. Beware, this will
echo                take a long time.
goto :exit

rem From https://stackoverflow.com/questions/3973824/windows-bat-file-optional-argument-parsing

:set_param_vars
set "ICUVERSION=74"
set "BUILDICU=n"
set "BUILDOPENSSL=n"
set "DEBUG="
set "BUILD_METHOD=Release"
set "DEFAULT_MOD=n"
set "GIT_PULL=n"
set "ARCH=x86"
set "ARCH_BUILD=Win32"
set "DX9=n"
set "DX9EX=n"
set "BUILD_METHOD_DX9="
set "BUILD_METHOD_DX9EX="
set "DB=y"
set "BUILDONLY=n"
set "REBUILD_METHOD=rebuild"
set "COMPILE_DEPENDENCIES=n"
set "COMPILE_SHADERS=n"
set "OPENSSL_BUILD=WIN32"
set "DIRECTX_LIB_ARCH_FOLDER="
set "DEP_FOLDER=top-recode-dep-binaries"

:params_parse
if not "%1"=="" (
	if "%1"=="--debug" (
		set "DEBUG=debug"
		set "BUILD_METHOD=Debug"
	)
	if "%1"=="--default_mod" set "DEFAULT_MOD=y"
	if "%1"=="--help" goto :help
	if "%1"=="--pull" set "GIT_PULL=y"
	if "%1"=="--x64" (
		set "ARCH=x64"
		set "ARCH_BUILD=x64"
		set "OPENSSL_BUILD=WIN64A"
		set "DIRECTX_LIB_ARCH_FOLDER=\%ARCH_BUILD%"
	)
	if "%1"=="--nodb" set "DB=n"
	if "%1"=="--dx9" (
		if "%DX9EX%"=="n" (
			set "DX9=y"
			set "BUILD_METHOD_DX9= DX9"
		)
	)
	if "%1"=="--dx9ex" (
		if "%DX9%"=="n" (
			set "DX9EX=y"
			set "BUILD_METHOD_DX9EX= DX9Ex"
		)
	)
	if "%1"=="--buildonly" (
		set "BUILDONLY=y"
		set "REBUILD_METHOD=build"
	)
	if "%1"=="--compshader" (
		set "COMPILE_SHADERS=y"
	)
	if "%1"=="--icu" (
		set "BUILDICU=y"
	)
	if "%1"=="--openssl" (
		set "BUILDOPENSSL=y"
	)
	if "%1"=="--dep" (
		set "DEP_FOLDER=dep"
	)
	if "%1"=="--comp-dep" (
		set "COMPILE_DEPENDENCIES=y"
		set "DEP_FOLDER=dep"
	)
	
	shift
	goto :params_parse
)

rem Check if this bat file is going to be self-modified by a git pull
if "%GIT_PULL%"=="y" (
	echo Git pulling to make sure you're using latest files

	call git diff --exit-code %~f0
	if "%ERRORLEVEL%" NEQ "0" (
		echo This script gets modified on git pull. Git pulling on continue.
		echo Please, restart this script once it finishes.
		pause
		call git pull
		goto :exit
	) else (
		call git pull
	)
)

rem From https://stackoverflow.com/questions/4051883/batch-script-how-to-check-for-admin-rights
set "ADMIN=n"
net session >nul 2>&1
if "%errorLevel%" == "0" (
    set "ADMIN=y"
)

set "FILE="
set "SOURCE_FILE="

echo Loading Microsoft Visual Studio 2022 variables

:: Script to detect proper VS folder
if exist "%PROGRAMFILES%\Microsoft Visual Studio\2022\Community" (
	set "MYEDITION=Community"
) else (
	if exist "%PROGRAMFILES%\Microsoft Visual Studio\2022\Professional" (
		set "MYEDITION=Professional"
	) else (
		if exist "%PROGRAMFILES%\Microsoft Visual Studio\2022\Enterprise" set "MYEDITION=Enterprise"
	)
)

rem Strips quotes from paths in %PATH% - https://stackoverflow.com/questions/33116937/call-with-spaces-and-parenthesis-in-path
@set "PATHTEMP=%PATH%"
@set PATH=%PATHTEMP:"=%

rem if block fixes running vcversall many times in a row in same cmd prompt window.
if not defined DevEnvDir (
    call "%PROGRAMFILES%\Microsoft Visual Studio\2022\%MYEDITION%\VC\Auxiliary\Build\vcvarsall.bat" %ARCH%
)

:: (fix for Windows 8+) Change directory to the one script is on
cd /d %~dp0 

cd source

if "%COMPILE_DEPENDENCIES%"=="n" (
	goto :CLONETOPRECODEDEPBINARIES
)
rem Compile dependencies
cd dep
call compile_deps.bat %ARCH% %BUILD_METHOD% %REBUILD_METHOD%
cd ..
goto :SYMLINKS

:CLONETOPRECODEDEPBINARIES
if "%DEP_FOLDER%"=="dep" (
	goto :SYMLINKS
)
echo Cloning top-recode-dep-binaries
call git clone https://gitlab.com/deguix/top-recode-dep-binaries.git top-recode-dep-binaries
cd top-recode-dep-binaries
call git pull
cd ..

:SYMLINKS
rem Symlink dependencies (always under source folder)
echo Symlink dependencies

cd lib
cd %ARCH_BUILD%

call :MKJUNCTION_DIFFNAME ".\DirectX" "..\..\%DEP_FOLDER%\DirectX\Lib%DIRECTX_LIB_ARCH_FOLDER%"
call :MKJUNCTION_DIFFNAME ".\DirectX9" "..\..\%DEP_FOLDER%\DirectX9\Lib\%ARCH%"
call :MKJUNCTION_DIFFNAME ".\sdl" "..\..\%DEP_FOLDER%\sdl\VisualC\%ARCH_BUILD%\%BUILD_METHOD%"
call :MKJUNCTION_DIFFNAME ".\sdl_mixer" "..\..\%DEP_FOLDER%\sdl_mixer\VisualC\%ARCH_BUILD%\%BUILD_METHOD%"

mkdir luajit
cd luajit
mkdir %BUILD_METHOD%
cd %BUILD_METHOD%
call :MKLINK "..\..\..\..\%DEP_FOLDER%\luajit\lib\%ARCH_BUILD%\%BUILD_METHOD%\lua51.dll"
call :MKLINK "..\..\..\..\%DEP_FOLDER%\luajit\lib\%ARCH_BUILD%\%BUILD_METHOD%\lua51.lib"
call :MKLINK "..\..\..\..\%DEP_FOLDER%\luajit\lib\%ARCH_BUILD%\%BUILD_METHOD%\lua51.pdb"
call :MKLINK "..\..\..\..\%DEP_FOLDER%\luajit\lib\%ARCH_BUILD%\%BUILD_METHOD%\luajit.lib"
call :MKLINK "..\..\..\..\%DEP_FOLDER%\luajit\lib\%ARCH_BUILD%\%BUILD_METHOD%\luajit.exe"
call :MKLINK "..\..\..\..\%DEP_FOLDER%\luajit\lib\%ARCH_BUILD%\%BUILD_METHOD%\luajit.pdb"
call :MKLINK "..\..\..\..\%DEP_FOLDER%\luajit\lib\%ARCH_BUILD%\%BUILD_METHOD%\luajit.ilk"
cd ..\..

mkdir icu
cd icu
set "ICULIBFOLDER=lib"
set "ICUBINFOLDER=bin"
set "ICUDEBUGFILEENDING="
if "%ARCH_BUILD%"=="x64" (
	set "ICULIBFOLDER=lib64"
	set "ICUBINFOLDER=bin64"
)
if "%BUILD_METHOD%"=="Debug" (
	set "ICUDEBUGFILEENDING=d"
)
call :MKLINK "..\..\..\%DEP_FOLDER%\icu\icu4c\%ICUBINFOLDER%\derb.exe"
call :MKLINK "..\..\..\%DEP_FOLDER%\icu\icu4c\%ICUBINFOLDER%\genrb.exe"

call :MKLINK "..\..\..\%DEP_FOLDER%\icu\icu4c\%ICULIBFOLDER%\icudt%ICUDEBUGFILEENDING%.exp"
call :MKLINK "..\..\..\%DEP_FOLDER%\icu\icu4c\%ICULIBFOLDER%\icudt%ICUDEBUGFILEENDING%.lib"
call :MKLINK "..\..\..\%DEP_FOLDER%\icu\icu4c\%ICULIBFOLDER%\icudt%ICUDEBUGFILEENDING%.pdb"
call :MKLINK "..\..\..\%DEP_FOLDER%\icu\icu4c\%ICUBINFOLDER%\icudt%ICUVERSION%.dll"

call :MKLINK "..\..\..\%DEP_FOLDER%\icu\icu4c\%ICULIBFOLDER%\icuin%ICUDEBUGFILEENDING%.exp"
call :MKLINK "..\..\..\%DEP_FOLDER%\icu\icu4c\%ICULIBFOLDER%\icuin%ICUDEBUGFILEENDING%.lib"
call :MKLINK "..\..\..\%DEP_FOLDER%\icu\icu4c\%ICULIBFOLDER%\icuin%ICUDEBUGFILEENDING%.pdb"
call :MKLINK "..\..\..\%DEP_FOLDER%\icu\icu4c\%ICUBINFOLDER%\icuin%ICUVERSION%%ICUDEBUGFILEENDING%.dll"

call :MKLINK "..\..\..\%DEP_FOLDER%\icu\icu4c\%ICULIBFOLDER%\icuio%ICUDEBUGFILEENDING%.exp"
call :MKLINK "..\..\..\%DEP_FOLDER%\icu\icu4c\%ICULIBFOLDER%\icuio%ICUDEBUGFILEENDING%.lib"
call :MKLINK "..\..\..\%DEP_FOLDER%\icu\icu4c\%ICULIBFOLDER%\icuio%ICUDEBUGFILEENDING%.pdb"
call :MKLINK "..\..\..\%DEP_FOLDER%\icu\icu4c\%ICUBINFOLDER%\icuio%ICUVERSION%%ICUDEBUGFILEENDING%.dll"

call :MKLINK "..\..\..\%DEP_FOLDER%\icu\icu4c\%ICULIBFOLDER%\icutu%ICUDEBUGFILEENDING%.exp"
call :MKLINK "..\..\..\%DEP_FOLDER%\icu\icu4c\%ICULIBFOLDER%\icutu%ICUDEBUGFILEENDING%.lib"
call :MKLINK "..\..\..\%DEP_FOLDER%\icu\icu4c\%ICULIBFOLDER%\icutu%ICUDEBUGFILEENDING%.pdb"
call :MKLINK "..\..\..\%DEP_FOLDER%\icu\icu4c\%ICUBINFOLDER%\icutu%ICUVERSION%%ICUDEBUGFILEENDING%.dll"

call :MKLINK "..\..\..\%DEP_FOLDER%\icu\icu4c\%ICULIBFOLDER%\icuuc%ICUDEBUGFILEENDING%.exp"
call :MKLINK "..\..\..\%DEP_FOLDER%\icu\icu4c\%ICULIBFOLDER%\icuuc%ICUDEBUGFILEENDING%.lib"
call :MKLINK "..\..\..\%DEP_FOLDER%\icu\icu4c\%ICULIBFOLDER%\icuuc%ICUDEBUGFILEENDING%.pdb"
call :MKLINK "..\..\..\%DEP_FOLDER%\icu\icu4c\%ICUBINFOLDER%\icuuc%ICUVERSION%%ICUDEBUGFILEENDING%.dll"
cd ..

set "OPENSSLPOSTFIX="
if "%ARCH_BUILD%"=="x64" (
	set "OPENSSLPOSTFIX=-x64"
)
mkdir openssl
cd openssl
rem no easy way to get the debug build unfortunately
call :MKLINK "..\..\..\%DEP_FOLDER%\openssl\lib\%ARCH_BUILD%\libssl-3%OPENSSLPOSTFIX%.dll"
call :MKLINK "..\..\..\%DEP_FOLDER%\openssl\lib\%ARCH_BUILD%\libssl-3%OPENSSLPOSTFIX%.pdb"
call :MKLINK "..\..\..\%DEP_FOLDER%\openssl\lib\%ARCH_BUILD%\libssl.lib"
call :MKLINK "..\..\..\%DEP_FOLDER%\openssl\lib\%ARCH_BUILD%\libcrypto-3%OPENSSLPOSTFIX%.dll"
call :MKLINK "..\..\..\%DEP_FOLDER%\openssl\lib\%ARCH_BUILD%\libcrypto-3%OPENSSLPOSTFIX%.pdb"
call :MKLINK "..\..\..\%DEP_FOLDER%\openssl\lib\%ARCH_BUILD%\libcrypto.lib"
cd ..

cd ..\..
cd inc

call :MKJUNCTION_DIFFNAME ".\DirectX" "..\%DEP_FOLDER%\DirectX\include"
call :MKJUNCTION_DIFFNAME ".\DirectX9" "..\%DEP_FOLDER%\DirectX9\include"
call :MKJUNCTION_DIFFNAME ".\sdl" "..\%DEP_FOLDER%\sdl\include"
call :MKJUNCTION_DIFFNAME ".\sdl_mixer" "..\%DEP_FOLDER%\sdl_mixer\include"
call :MKJUNCTION_DIFFNAME ".\icu" "..\%DEP_FOLDER%\icu\icu4c\include"
call :MKJUNCTION_DIFFNAME ".\openssl" "..\%DEP_FOLDER%\openssl\inc"

mkdir randutils
cd randutils
call :MKLINK "..\..\%DEP_FOLDER%\randutils\randutils.hpp"
cd ..

mkdir luajit
cd luajit
call :MKLINK "..\..\%DEP_FOLDER%\luajit\inc\lauxlib.h"
call :MKLINK "..\..\%DEP_FOLDER%\luajit\inc\lua.h"
call :MKLINK "..\..\%DEP_FOLDER%\luajit\inc\lua.hpp"
call :MKLINK "..\..\%DEP_FOLDER%\luajit\inc\luaconf.h"
call :MKLINK "..\..\%DEP_FOLDER%\luajit\inc\luajit.h"
call :MKLINK "..\..\%DEP_FOLDER%\luajit\inc\lualib.h"
cd ..

cd ..
cd src
mkdir enum_bitmask
cd enum_bitmask
call :MKLINK "..\..\%DEP_FOLDER%\enum_bitmask\EnumBitmask.hpp"
cd ..\..

rem Finish cloning dependencies (always under source folder)

echo Compiling all projects belonging to project (this will take a long time!)
devenv /%REBUILD_METHOD% "%BUILD_METHOD%" ".\src\all.sln"
devenv /%REBUILD_METHOD% "%BUILD_METHOD% Client%BUILD_METHOD_DX9%%BUILD_METHOD_DX9EX%" ".\src\all.sln"
devenv /%REBUILD_METHOD% "%BUILD_METHOD% TradeServer" ".\src\all.sln"
cd ..

echo Make symbolic links for all the binaries created
cd client\system
call :MKLINK "..\..\source\lib\%ARCH_BUILD%\luajit\%BUILD_METHOD%\lua51.dll"
call :MKLINK "..\..\source\lib\%ARCH_BUILD%\SDL\SDL3.dll"
call :MKLINK "..\..\source\lib\%ARCH_BUILD%\SDL_mixer\SDL3_mixer.dll"
call :MKLINK "..\..\source\lib\%ARCH_BUILD%\SDL_mixer\libFLAC-8.dll"
call :MKLINK "..\..\source\lib\%ARCH_BUILD%\SDL_mixer\libmodplug-1.dll"
call :MKLINK "..\..\source\lib\%ARCH_BUILD%\SDL_mixer\libmpg123-0.dll"
call :MKLINK "..\..\source\lib\%ARCH_BUILD%\SDL_mixer\libogg-0.dll"
call :MKLINK "..\..\source\lib\%ARCH_BUILD%\SDL_mixer\libopus-0.dll"
call :MKLINK "..\..\source\lib\%ARCH_BUILD%\SDL_mixer\libopusfile-0.dll"
call :MKLINK "..\..\source\lib\%ARCH_BUILD%\SDL_mixer\libvorbis-0.dll"
call :MKLINK "..\..\source\lib\%ARCH_BUILD%\SDL_mixer\libvorbisfile-3.dll"
rem call :MKLINK "..\..\source\lib\%ARCH_BUILD%\ipss\PAI.dll"
call :MKLINK "..\..\source\lib\%ARCH_BUILD%\openssl\libcrypto-3%OPENSSLPOSTFIX%.dll"
if "%ARCH_BUILD%"=="x64" (
	if "%BUILD_METHOD%"=="Debug" (
		if "%DX9%"=="y" (
			call :MKLINK "..\..\source\lib\%ARCH_BUILD%\MindPower3D\MindPower3DDDX9x64.dll"
			call :MKLINK "..\..\source\lib\%ARCH_BUILD%\MindPower3D\MindPower3DDDX9x64.pdb"
		) else (
			if "%DX9EX%"=="y" (
				call :MKLINK "..\..\source\lib\%ARCH_BUILD%\MindPower3D\MindPower3DDDX9x64Ex.dll"
				call :MKLINK "..\..\source\lib\%ARCH_BUILD%\MindPower3D\MindPower3DDDX9x64Ex.pdb"
			) else (
				call :MKLINK "..\..\source\lib\%ARCH_BUILD%\MindPower3D\MindPower3DDx64.dll"
				call :MKLINK "..\..\source\lib\%ARCH_BUILD%\MindPower3D\MindPower3DDx64.pdb"
			)
		)
	) else (
		if "%DX9%"=="y" (
			call :MKLINK "..\..\source\lib\%ARCH_BUILD%\MindPower3D\MindPower3DDX9x64.dll"
			call :MKLINK "..\..\source\lib\%ARCH_BUILD%\MindPower3D\MindPower3DDX9x64.pdb"
		) else (
			if "%DX9%"=="y" (
				call :MKLINK "..\..\source\lib\%ARCH_BUILD%\MindPower3D\MindPower3DDX9x64Ex.dll"
				call :MKLINK "..\..\source\lib\%ARCH_BUILD%\MindPower3D\MindPower3DDX9x64Ex.pdb"
			) else (
				call :MKLINK "..\..\source\lib\%ARCH_BUILD%\MindPower3D\MindPower3Dx64.dll"
				call :MKLINK "..\..\source\lib\%ARCH_BUILD%\MindPower3D\MindPower3Dx64.pdb"
			)
		)
	)
) else (
	if "%BUILD_METHOD%"=="Debug" (
		if "%DX9%"=="y" (
			call :MKLINK "..\..\source\lib\%ARCH_BUILD%\MindPower3D\MindPower3DDDX9.dll"
			call :MKLINK "..\..\source\lib\%ARCH_BUILD%\MindPower3D\MindPower3DDDX9.pdb"
		) else (
			if "%DX9EX%"=="y" (
					call :MKLINK "..\..\source\lib\%ARCH_BUILD%\MindPower3D\MindPower3DDDX9Ex.dll"
					call :MKLINK "..\..\source\lib\%ARCH_BUILD%\MindPower3D\MindPower3DDDX9Ex.pdb"
			) else (
					call :MKLINK "..\..\source\lib\%ARCH_BUILD%\MindPower3D\MindPower3DD.dll"
					call :MKLINK "..\..\source\lib\%ARCH_BUILD%\MindPower3D\MindPower3DD.pdb"
			)
		)
	) else (
		if "%DX9%"=="y" (
			call :MKLINK "..\..\source\lib\%ARCH_BUILD%\MindPower3D\MindPower3DDX9.dll"
			call :MKLINK "..\..\source\lib\%ARCH_BUILD%\MindPower3D\MindPower3DDX9.pdb"
		) else (
			if "%DX9EX%"=="y" (
				call :MKLINK "..\..\source\lib\%ARCH_BUILD%\MindPower3D\MindPower3DDX9Ex.dll"
				call :MKLINK "..\..\source\lib\%ARCH_BUILD%\MindPower3D\MindPower3DDX9Ex.pdb"
			) else (
				call :MKLINK "..\..\source\lib\%ARCH_BUILD%\MindPower3D\MindPower3D.dll"
				call :MKLINK "..\..\source\lib\%ARCH_BUILD%\MindPower3D\MindPower3D.pdb"
			)
		)
	)
)
cd ..\..

cd server
call :MKLINK "..\source\lib\%ARCH_BUILD%\luajit\%BUILD_METHOD%\lua51.dll"
call :MKLINK "..\source\lib\%ARCH_BUILD%\openssl\libcrypto-3%OPENSSLPOSTFIX%.dll"
cd ..

cd helpers
call :MKLINK "..\source\lib\%ARCH_BUILD%\luajit\%BUILD_METHOD%\luajit.exe"
call :MKLINK "..\source\lib\%ARCH_BUILD%\luajit\%BUILD_METHOD%\lua51.dll"
cd ..

echo Make symbolic links for icu%ICUVERSION%
cd translation
call :MKLINK "..\source\lib\%ARCH_BUILD%\icu\derb.exe"
call :MKLINK "..\source\lib\%ARCH_BUILD%\icu\genrb.exe"
call :MKLINK "..\source\lib\%ARCH_BUILD%\icu\icudt%ICUVERSION%.dll"
call :MKLINK "..\source\lib\%ARCH_BUILD%\icu\icuin%ICUVERSION%.dll"
call :MKLINK "..\source\lib\%ARCH_BUILD%\icu\icuio%ICUVERSION%.dll"
call :MKLINK "..\source\lib\%ARCH_BUILD%\icu\icutu%ICUVERSION%.dll"
call :MKLINK "..\source\lib\%ARCH_BUILD%\icu\icuuc%ICUVERSION%.dll"
cd ..

echo Compiling translation resources...
cd translation
call compile.bat
cd ..

echo Symbolic/hard linking more files...
cd server
call :MKLINK "..\translation\en_US.res"
call :MKLINK "..\translation\pt_BR.res"
if "%BUILD_METHOD%"=="Debug" (
	call :MKLINK "..\source\lib\%ARCH_BUILD%\icu\icudt%ICUVERSION%d.dll"
	call :MKLINK "..\source\lib\%ARCH_BUILD%\icu\icuin%ICUVERSION%d.dll"
	call :MKLINK "..\source\lib\%ARCH_BUILD%\icu\icuuc%ICUVERSION%d.dll"
) else (
	call :MKLINK "..\source\lib\%ARCH_BUILD%\icu\icudt%ICUVERSION%.dll"
	call :MKLINK "..\source\lib\%ARCH_BUILD%\icu\icuin%ICUVERSION%.dll"
	call :MKLINK "..\source\lib\%ARCH_BUILD%\icu\icuuc%ICUVERSION%.dll"
)
rem Remember that derb.exe and genrb.exe are precompiled and don't use debug dlls
call :MKLINK_DIFFNAME  "AccountServer.loc" "..\translation\Game.loc"
call :MKLINK_DIFFNAME  "GameServer.loc" "..\translation\Game.loc"
call :MKLINK_DIFFNAME  "GateServer.loc" "..\translation\Game.loc"
call :MKLINK_DIFFNAME "GroupServer.loc" "..\translation\Game.loc"
if "%ARCH_BUILD%"=="x64" (
if "%BUILD_METHOD%"=="Debug" (
	call :MKLINK "..\source\lib\%ARCH_BUILD%\InfoBill\InfoBillDx64.dll"
	call :MKLINK "..\source\lib\%ARCH_BUILD%\InfoBill\InfoBillDx64.pdb"
) else (
	call :MKLINK "..\source\lib\%ARCH_BUILD%\InfoBill\InfoBillx64.dll"
	call :MKLINK "..\source\lib\%ARCH_BUILD%\InfoBill\InfoBillx64.pdb"
)
) else (
if "%BUILD_METHOD%"=="Debug" (
	call :MKLINK "..\source\lib\%ARCH_BUILD%\InfoBill\InfoBillD.dll"
	call :MKLINK "..\source\lib\%ARCH_BUILD%\InfoBill\InfoBillD.pdb"
) else (
	call :MKLINK "..\source\lib\%ARCH_BUILD%\InfoBill\InfoBill.dll"
	call :MKLINK "..\source\lib\%ARCH_BUILD%\InfoBill\InfoBill.pdb"
)
)
cd ..\client\system
call :MKLINK "..\..\translation\en_US.res"
call :MKLINK "..\..\translation\pt_BR.res"
if "%BUILD_METHOD%"=="Debug" (
	call :MKLINK "..\..\source\lib\%ARCH_BUILD%\icu\icudt%ICUVERSION%d.dll"
	call :MKLINK "..\..\source\lib\%ARCH_BUILD%\icu\icuin%ICUVERSION%d.dll"
	call :MKLINK "..\..\source\lib\%ARCH_BUILD%\icu\icutu%ICUVERSION%d.dll"
	call :MKLINK "..\..\source\lib\%ARCH_BUILD%\icu\icuuc%ICUVERSION%d.dll"
) else (
	call :MKLINK "..\..\source\lib\%ARCH_BUILD%\icu\icudt%ICUVERSION%.dll"
	call :MKLINK "..\..\source\lib\%ARCH_BUILD%\icu\icuin%ICUVERSION%.dll"
	call :MKLINK "..\..\source\lib\%ARCH_BUILD%\icu\icutu%ICUVERSION%.dll"
	call :MKLINK "..\..\source\lib\%ARCH_BUILD%\icu\icuuc%ICUVERSION%.dll"
)
call :MKLINK "..\..\translation\Game.loc"
cd ..\scripts
if "%DEFAULT_MOD%"=="y" (
	call :MKJUNCTION_DIFFNAME "table" "table-default"
	cd ..\..\helpers
	echo Compiling .txt files into .lua to be read by server
	call txttotsv.bat
	cd ..\resource\script
	echo mod = 'default' > def.lua
	cd default
	mkdir raw
	cd ..\..
) else (
	call :MKJUNCTION_DIFFNAME "table" "table-top2"
	cd ..\..\resource\script
	echo mod = 'pko_2.7' > def.lua
	cd ..
	
)
call :MKLINK "..\client\scripts\table\areaset.txt"
call :MKLINK "..\client\scripts\table\characterinfo.txt"
call :MKLINK "..\client\scripts\table\forgeitem.txt"
call :MKLINK "..\client\scripts\table\iteminfo.txt"
call :MKLINK "..\client\scripts\table\shipinfo.txt"
call :MKLINK "..\client\scripts\table\shipiteminfo.txt"
call :MKLINK "..\client\scripts\table\skillinfo.txt"
call :MKLINK "..\client\scripts\table\skilleff.txt"
cd ..

echo Compiling game table files... Press OK on dialog boxes.
cd client
if "%ARCH%"=="x64" (
	call compile%ARCH%.bat
) else (
	call compile.bat
)
cd ..

if "%DB%"=="y" (
	rem already has note in bat
	cd sql_scripts
	call build_db.bat
	cd ..
)

if "%COMPILE_DX8_SHADERS%"=="y" (
	echo Compiling the .vsh shaders
	cd helpers
	if %DX9%==y (
		call hlsl_to_vsh_dx9.bat
	) else (
		call hlsl_to_vsh_dx8.bat
	)
	cd ..
)

echo Finished.
pause
:exit
@echo on
exit /B

rem Procedure definitions

rem Makes additional checks on mklink to make sure script doesn't spit errors non-stop
rem Also prevents script execution if files are missing
rem Also deletes old link if admin to make sure x86/x64 links are appropriate
:MKLINK
	if exist %~nx1 (
		if %ADMIN%==y (
			del /F /Q %~nx1
			mklink %~nx1 %1
		)
	) else (
		if %ADMIN%==n (
			goto :FILE_DOESNT_EXIST
		) else mklink %~nx1 %1
	)
	exit /B
	
:MKLINK_DIFFNAME
	if exist %1 (
		if %ADMIN%==y (
			del /F /Q %1
			mklink %1 %2
		)
	) else (
		if %ADMIN%==n (
			goto :FILE_DOESNT_EXIST
		) else mklink %1 %2
	)
	exit /B

:MKJUNCTION
	if exist %~nx1 (
		if %ADMIN%==y (
			del /F /Q %~nx1
			mklink /J %~nx1 %1
		)
	) else (
		if %ADMIN%==n (
			goto :FILE_DOESNT_EXIST
		) else mklink /J %~nx1 %1
	)
	exit /B


:MKJUNCTION_DIFFNAME
	if exist %1 (
		if %ADMIN%==y (
			rmdir /S /Q %1
			mklink /J %1 %2
		)
	) else (
		if %ADMIN%==n (
			goto :FILE_DOESNT_EXIST
		) else mklink /J %1 %2
	)
	exit /B

rem https://stackoverflow.com/questions/1645843/resolve-absolute-path-from-relative-path-and-or-file-name
:NORMALIZEPATH
  set "RETVAL=%~dpfn1"
  exit /B

:FILE_DOESNT_EXIST
	call :NORMALIZEPATH %2
	echo "File %RETVAL% doesn't exist! Run the script as admin for the symlinking to work!"
	pause
	exit