:: (fix for Windows 8+) Change directory to the one script is on
cd /d %~dp0

winget install --id=Microsoft.VisualStudio.2022.Community -e -h --override "--quiet --add Microsoft.VisualStudio.Workload.NativeDesktop;includeRecommended --add Microsoft.VisualStudio.Component.VC.ATLMFC" && ^
winget install --id=Microsoft.SQLServer.2022.Express -e -h --override "/QUIET /ENU /ACTION=Download /MEDIATYPE=Core" && ^
%USERPROFILE%\Downloads\SQLEXPR_x64_ENU.exe /X:"%TEMP%\mssql" /QUIET /INDICATEPROGRESS /IACCEPTSQLSERVERLICENSETERMS /SUPPRESSPRIVACYSTATEMENTNOTICE /ENU /ACTION=Install /ROLE=AllFeatures_WithDefaults /FEATURES=SQLEngine,Replication,FullText /INSTANCEID="MSSQLSERVER" /INSTANCENAME="MSSQLSERVER" /SECURITYMODE=SQL /SAPWD="weakpass" /ADDCURRENTUSERASSQLADMIN=True /TCPENABLED=1 && ^
winget install --id=Microsoft.SQLServerManagementStudio -e -h && ^
winget install --id=Git.Git -e -h && ^
winget install --id=TortoiseGit.TortoiseGit -e -h