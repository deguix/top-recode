//
// Generated by Microsoft (R) HLSL Shader Compiler 9.29.952.3111
//
//   fxc /Tvs_1_1 /Dvs_1_1 /Fc../client/shader/dx8/eff1.vsh
//    ../client/shader/dx9_hlsl/effect.hlsl
//
//
// Parameters:
//
//   float4 Alpha;
//   float2 VertexUVs[4];
//   float4x4 ViewProjection;
//   float4x4 World;
//
//
// Registers:
//
//   Name           Reg   Size
//   -------------- ----- ----
//   World          c0       4
//   ViewProjection c4       4
//   Alpha          c8       1
//   VertexUVs      c9       4
//

    vs.1.1
    //dcl_position v0
    //dcl_blendindices v2
    dp4 r0.x, v0, c0
    dp4 r0.y, v0, c1
    dp4 r0.z, v0, c2
    dp4 r0.w, v0, c3
    dp4 oPos.x, r0, c4
    dp4 oPos.y, r0, c5
    dp4 oPos.z, r0, c6
    dp4 oPos.w, r0, c7
    mov oD0, c8
    mov a0.x, v2.x
    mov oT0.xy, c[9+a0.x]

// approximately 11 instruction slots used
