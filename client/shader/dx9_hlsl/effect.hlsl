//HLSL with constrained registers adapted to PKO shaders syntax
//Converting ASM to vs_1_1 accepted by DX8:
//  Change "vs_1_1" to "vs.1.1".
//  Comment all dcl_ instructions (// or ;) - these are already defined in code.
//  Change all c#[?] to c[? + #].

//c# General constants
float4x4 World : register(c0);
float4x4 ViewProjection : register(c4);

//Specified constants in ?
float4 Alpha : register(c8);
#ifndef NO_VERTEXUVS
float2 VertexUVs[4] : register(c9);
#endif

struct VertexShaderOutput { //o# Output parameters
    float4 Position : POSITION0;
	float4 Color : COLOR0;
	float2 TexCoord0 : TEXCOORD0;
};

VertexShaderOutput main(
    float4 Position : POSITION0 : register(v0), //uses float3 with added w with 1.0 (DX9 does that auto)
#ifdef NO_VERTEXUVS
	float2 TexCoord0 : TEXCOORD0 : register(v7)
#else
	uint Blendindices : BLENDINDICES0 : register(v2)
#endif
) {
    VertexShaderOutput output;
    float4 worldPosition = mul(Position, World);
    output.Position = mul(worldPosition, ViewProjection); //2D - otherwise objects only appear if character actually looks at them.
	output.Color = Alpha;
#ifndef NO_VERTEXUVS
	output.TexCoord0 = VertexUVs[Blendindices].xy;
#else
	output.TexCoord0 = TexCoord0;
#endif
    return output;
};