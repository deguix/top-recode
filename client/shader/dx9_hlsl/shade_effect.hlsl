//HLSL with constrained registers adapted to PKO shaders syntax
//Converting ASM to vs_1_1 accepted by DX8:
//  Change "vs_1_1" to "vs.1.1".
//  Comment all dcl_ instructions (// or ;) - these are already defined in code.
//  Change all c#[?] to c[? + #].

#ifdef PROPER_TEXCOORD1
//This fix makes this shader much easier to understand
//In CreateShadeModel in I_Effect.cpp of MindPower
//Change the line:
//	int nIndex = 9; //!设置的VS常量从9开始
//to:
//	int nIndex = 0;

//c# General constants
float4x4 World : register(c0);
float4x4 ViewProjection : register(c4);

//Specified constants in ?
float4 Alpha : register(c8);
float4 Pallete[247] : register(c9); //Infinite - From amount of constants found in CMPShadeMap::RenderVS
                                    //depends on radius of effect: ((radius/TILESIZE+1)+1)^2, TILESIZE=1
//vs_1_1 D3DCAPS9.MaxVertexShaderConst: 96
//vs_2+  D3DCAPS9.MaxVertexShaderConst: 256

struct VertexShaderOutput { //o# Output parameters
    float4 Position : POSITION0;
	float4 Color : COLOR0;
	float2 TexCoord0 : TEXCOORD0;
};

VertexShaderOutput main(
	float2 Index : TEXCOORD1 : register(v8)
) {
    VertexShaderOutput output;
    output.Position = mul(mul(Pallete[Index.y], World), ViewProjection); //2D
	output.Color = Alpha;
	output.TexCoord0 = Pallete[Index.x].xy;
    return output;
}
#else
//c# General constants
float4 UV[256] : register(c0);

struct VertexShaderInput { //v# Input parameters
	
};
struct VertexShaderOutput { //o# Output parameters
    float4 Position : POSITION0;
	float4 Color : COLOR0;
	float2 TexCoord0 : TEXCOORD0;
};
VertexShaderOutput main(
    float2 TexCoord1 : TEXCOORD1 : register(v8)
) {
    VertexShaderOutput output;
	
	float4x4 World = { UV[0].x, UV[1].x, UV[2].x, UV[3].x,
					   UV[0].y, UV[1].y, UV[2].y, UV[3].y,
					   UV[0].z, UV[1].z, UV[2].z, UV[3].z,
					   UV[0].w, UV[1].w, UV[2].w, UV[3].w };
	float4x4 ViewProjection = { UV[4].x, UV[5].x, UV[6].x, UV[7].x,
							    UV[4].y, UV[5].y, UV[6].y, UV[7].y,
							    UV[4].z, UV[5].z, UV[6].z, UV[7].z,
							    UV[4].w, UV[5].w, UV[6].w, UV[7].w };
	float4 Alpha = UV[8];
	
    float4 worldPosition = mul(UV[TexCoord1.y], World);
    output.Position = mul(worldPosition, ViewProjection); //2D
	output.Color = Alpha;
	output.TexCoord0 = UV[TexCoord1.x];
    return output;
}
#endif