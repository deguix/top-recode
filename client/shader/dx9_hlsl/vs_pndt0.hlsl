//HLSL with constrained registers adapted to PKO shaders syntax
//Converting ASM to vs_1_1 accepted by DX8:
//  Change "vs_1_1" to "vs.1.1".
//  Comment all dcl_ instructions (// or ;) - these are already defined in code.
//  Change all c#[?] to c[? + #].

//float4 Base : register(c0);
//c# Constants
float4x4 World : register(c0);
float3 LightDir : register(c4);
float4 Ambient : register(c5);
//float4x4 ViewToShadowmap : register(c19);
float4x3 UVMatrix0 : register(c7);

struct VertexShaderOutput { //o# Output parameters
    float4 Position : POSITION0;
	float4 Color : COLOR0;
	float4 Specular : COLOR1;
	float2 TexCoord0 : TEXCOORD0;
	//float4 TexCoord1 : TEXCOORD1;
    //float2 Depth : TEXCOORD4;
    //float4 PosShadowmap : TEXCOORD5;
};
VertexShaderOutput main(
	float4 Position : POSITION0 : register(v0),
	float3 Normal : NORMAL0 : register(v3),
#ifndef NO_DIFFUSE
	float4 Diffuse : COLOR0 : register(v5),
#endif
	float2 TexCoord0 : TEXCOORD0 : register(v7)
	//float4 TexCoord1 : TEXCOORD1 : register(v8)
) {
    VertexShaderOutput output;
    output.Position = mul(Position, World);
	
#ifndef NO_LIGHTING
    float4 lightCoeff = lit(dot(Normal, LightDir), 0.0, 0.0);
#ifndef NO_DIFFUSE
    float4 diffuse = lightCoeff.y  * Diffuse;
#else
    float4 diffuse = lightCoeff.y;
#endif
    diffuse += Ambient;
    output.Color = saturate(diffuse);
#else
#ifndef NO_DIFFUSE
    output.Color = Diffuse;
#else
    output.Color = float4(1.0, 1.0, 1.0, 1.0);
#endif
#endif
	output.Specular = float4(0.0, 0.0, 0.0, 0.0);
	
#ifdef USE_TEX_TRANSFORM
    output.TexCoord0 = mul(float4(TexCoord0, 1.0, 0.0), UVMatrix0).xy;
#else
    output.TexCoord0 = TexCoord0;
#endif
	
	//output.Depth = output.Position.zw;
    //output.PosShadowmap = mul(Position, ViewToShadowmap);
    return output;
};