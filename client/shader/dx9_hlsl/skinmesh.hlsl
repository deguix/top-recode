//HLSL with constrained registers adapted to PKO shaders syntax
//Converting ASM to vs_1_1 accepted by DX8:
//  Change "vs_1_1" to "vs.1.1".
//  Comment all dcl_ instructions (// or ;) - these are already defined in code.
//  Change all c#[?] to c[? + #].

//c# Constants
uint4 Base : register(c0);
float4x4 World : register(c1);
float3 LightDirection : register(c5);
float4 Ambient : register(c6);
float4 Diffuse : register(c7);
float4x4 Texture : register(c8);
float4x4 Texture2 : register(c12);
float4x4 Texture3 : register(c16);
#ifdef vs_1_1
//Palette as implemented gets truncated to 75 or 25 4x3's.
#ifdef NEW_PALETTE_C
float4 Palette[150] : register(c24);
#else
float4 Palette[150] : register(c21);
#endif
#else
#ifdef NEW_PALETTE_C
float4x3 Palette[50] : register(c24);
#else
float4x3 Palette[50] : register(c21);
#endif
#endif
//max is 96 c registers in vs_1_1

struct VertexShaderOutput { //o# Output parameters
    float4 Position : POSITION0;
	float4 Color : COLOR0;
	float4 Specular : COLOR1;
	float2 TexCoord0 : TEXCOORD0;
};
VertexShaderOutput main(
    float4 Position : POSITION0 : register(v0),
	float4 Blendweight : BLENDWEIGHT0 : register(v1),
	uint4 Blendindices : BLENDINDICES0 : register(v2),
	float3 Normal : NORMAL0 : register(v3),
	float2 TexCoord0 : TEXCOORD0 : register(v7)
) {
    VertexShaderOutput output;
	float3 blendPos = float3(0.0,0.0,0.0);
	float3 norm = float3(0.0,0.0,0.0);
	float lastWeight = Base.x;
    [unroll] for(int i = 0; i < NUM_SKIN_WEIGHTS; i++) {
        float weight = lastWeight;
        if(i < NUM_SKIN_WEIGHTS - 1) {
            weight = Blendweight[i];
            lastWeight -= weight;
        }
#ifdef vs_1_1
		//DX8: For some reason, no matter what, without using Base.w this breaks
		//even if using 3.0. So I had no choice but to keep it like this for now:
		uint j = Blendindices[i]*Base.w;
		float4x3 Palette4x3 = { Palette[j].x, Palette[j+1].x, Palette[j+2].x,
		                        Palette[j].y, Palette[j+1].y, Palette[j+2].y,
		                        Palette[j].z, Palette[j+1].z, Palette[j+2].z,
		                        Palette[j].w, Palette[j+1].w, Palette[j+2].w };
		blendPos += weight * mul(Position, Palette4x3);
		norm += weight * mul(Normal, (float3x3)Palette4x3);
#else
		blendPos += weight * mul(Position, Palette[Blendindices[i]]);
		norm += weight * mul(Normal, (float3x3)Palette[Blendindices[i]]);
#endif
    }
    output.Position = mul(float4(blendPos,Base.x),World);
	output.Color = min((lit(dot(normalize(norm),LightDirection),0.0,0.0).y*Diffuse)+Ambient,Base.x); //param 3 of lit is Blendindices.w*3.0, but it's garbage.
	output.Specular = float4(0.0, 0.0, 0.0, 0.0);
	output.TexCoord0 = TexCoord0;
#if USE_TEXTURE == 2
	output.TexCoord1 = mul(float3(TexCoord0,Base.x), Texture);
#elif USE_TEXTURE == 3
	output.TexCoord1 = TexCoord0;
	output.TexCoord2 = mul(float3(TexCoord0,Base.x), Texture2);
#elif USE_TEXTURE == 4
	output.TexCoord1 = TexCoord0;
	output.TexCoord2 = TexCoord0;
	output.TexCoord3 = mul(float3(TexCoord0,Base.x), Texture3);
#endif
    return output;
};