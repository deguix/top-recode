//HLSL with constrained registers adapted to PKO shaders syntax
//Converting ASM to vs_1_1 accepted by DX8:
//  Change "vs_1_1" to "vs.1.1".
//  Comment all dcl_ instructions (// or ;) - these are already defined in code.
//  Change all c#[?] to c[? + #].

//c# General constants
float4x4 World : register(c0);
float4x4 ViewProjection : register(c4);

//Specified constants in MPFont.cpp
float4 Alpha : register(c8);
float4 Palette[12]: register(c9); //From amount of constants found in CMPFont::Draw3DText in MPFont.cpp

struct VertexShaderOutput { //o# Output parameters
    float4 Position : POSITION0;
	float4 Color : COLOR0;
	float2 TexCoord0 : TEXCOORD0;
};
VertexShaderOutput main(
	float Blendweight : BLENDWEIGHT0 : register(v1)
) {
    VertexShaderOutput output;
    float4 worldPosition = mul(Palette[Blendweight], World);
    output.Position = mul(worldPosition, ViewProjection); //2D
	output.Color = Alpha;
	output.TexCoord0 = Palette[Blendweight+1].xy;
    return output;
}