//
// Generated by Microsoft (R) HLSL Shader Compiler 9.29.952.3111
//
//   fxc /Tvs_2_a /Dvs_2_a /DNO_LIGHTING /Fc../client/shader/dx9/vs_pndt0.vsh
//    ../client/shader/dx9_hlsl/vs_pndt0.hlsl
//
//
// Parameters:
//
//   float4x4 World;
//
//
// Registers:
//
//   Name         Reg   Size
//   ------------ ----- ----
//   World        c0       4
//

    vs_2_x
    def c4, 0, 0, 0, 0
    dcl_position v0
    dcl_color v5
    dcl_texcoord v7
    dp4 oPos.x, v0, c0
    dp4 oPos.y, v0, c1
    dp4 oPos.z, v0, c2
    dp4 oPos.w, v0, c3
    mov oD0, v5
    mov oD1, c4.x
    mov oT0.xy, v7

// approximately 7 instruction slots used
