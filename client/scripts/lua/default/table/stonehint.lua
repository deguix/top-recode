--prime number multiplied set table (with 4 colors as primes 2,3,5,7 - for combo color determination)
function Item_Stoneeffect(Color_1, Color_2, Color_3) --not done using table.insert's and calculation because the lua syntax is cumbersome.
	local color_hash = table.make_switch_table({2,3,5,7,6,10,14,15,21,35,30,42,70,105},1) --determine item indecomposibility (can't be done in same line)
	local result = table.inv_func(color_hash)[lfunc.foldl(kw.mul,1,table.unique({color_hash[Color_1], color_hash[Color_2], color_hash[Color_3]}))] --determine item uniqueness
	_lg("Item_Stoneeffect result: ".. result .. ". Colors: {" .. Color_1 .. "," .. Color_2 .. "," .. Color_3 .. "}\n")
	return result
end