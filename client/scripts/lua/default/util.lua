function _te(table,else_part)
	local mt = {__index = function(table,key)
		local v = rawget(table, key)
		if v == nil then
			if else_part == nil then
				v = key
			else
				v = else_part
			end
		end
		return v
	end}
	
	return setmetatable(table,mt)
end

switch_table = _te
table.make_switch_table = _te

table.inv_func = function (ot)
	local nt = {}
	for i,v in pairs(ot) do
		nt[v]=i
	end
	return nt
end


table.unique = function (t)
	local hash, result = {},{}
	for _,v in ipairs(t) do
		if (not hash[v]) then
			result[#result+1] = v
			hash[v] = true
		end
	end
	return result
end