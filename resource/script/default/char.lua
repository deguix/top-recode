AI_NONE        =  0  --不移动
AI_N_ATK       =  1  --不攻击
AI_FLEE        =  2  --被攻击后反方向逃跑 
AI_MOVETOHOST  =  4  --跟随主人
AI_R_ATK       =  5  --被攻击后反击
AI_ATK         = 10  --主动攻击 
AI_ATK_FLEE    = 11  --主动攻击+敌人逼近后逃跑(只有远程攻击的怪物可以填成此类型)
AI_32          = 32

AI_PATROL_FORWARDS = 0
AI_PATROL_BACKWARDS = 1
AI_PATROL_FORWARDS_END = 2
AI_PATROL_BACKWARDS_END = 3
AI_PATROL_MOVING_BACK = 9

--ai focused functions
function math.distance(x1, y1, x2, y2)
	return math.sqrt(((x2-x1)^2)+((y2-y1)^2))
end

function point_distance(p1, p2)
	if p1 ~= nil and p2 ~= nil then
		return math.distance(p1.x, p1.y, p2.x, p2.y)
	end
end

function select_skill(c)
   local s = 0
   local r = Rand(100)
   local skill_id, ratio = 0
   for i = 0, GetChaSkillNum(c) - 1 do         
	   skill_id, ratio = GetChaSkillInfo(c, i)
	   local e = s + ratio
	   if (e > r) and (r > s) then
		  break
	   end
	   s = e
   end
   return skill_id
end

--local temp_boolean = func_set[1](table.unpack(func_set[2]))

--[[
char.myindex = {
	mapname = thunk{GetChaMapName,{self.role}}, -- GetChaMapName(self.role)
	mobid = thunk{GetChaID,{self.role}},
	id = thunk{GetCharID,{self.role}},
	type_id = thunk{GetChaTypeID,{self.role}},
	mapcopy = thunk{GetChaMapCopy,{self.role}},
	partyid = thunk{GetChaTeamID,{self.role}},
	ai = thunk{GetChaAIType,{self.role}},
	target = thunk{char.wrap,thunk{GetChaTarget,{self.role}}}, -- char.wrap(GetChaTarget(self.role))
	reveal_players_on_sight = thunk{ClearHideChaByRange,{self.role, 0, 0, self.vision, 0}}, -- ClearHideChaByRange(self.role, 0, 0, self.vision, 0)
	blocked_moves = thunk{GetChaBlockCnt,{self.role}},
	face_pos = thunk{GetChaFacePos,{self.role}},
	pos = thunk{GetChaPos,{self.role}},
	spawn_pos = thunk{GetChaSpawnPos,{self.role}},
	first_target = thunk{char.wrap,thunk{GetChaFirstTarget,{self.role}}},
	patrol_state = thunk{GetChaPatrolState,{self.role}},
	chase_range = thunk{GetChaChaseRange,{self.role}},
	host = thunk{char.wrap,{GetChaHost,{self.role}}},
	name = thunk{GetChaDefaultName,{self.role}},
	stats = thunk{char_stats.wrap,{char_stats, self.role}},
	is_position_nearby = thunk{
		function (_self, position, radius)
			if position ~= nil then
				return point_distance(_self.pos, position) < radius
			end
		end, self},
	is_target_nearby = thunk{
		function (_self, target_char, radius)
			if target_char==nil then
				LG("ai_error", "is_target_nearby(): target_char doesn't exist")
				return false
			end
			return _self:is_position_nearby(_self.pos, target_char.pos)
		end, self},
	nb_is_target_nearby = thunk{
		function (_self, target_char, radius)
			if _self:is_target_nearby(target_char, radius) then
				return target_char
			end
		end, self},
	flee = thunk{
		function (_self, target_char)
		   _self:move(target_char:face_pos())
		end, self},
	move_randomly_within_range = thunk{
		function (_self, range)
			_self:move{	["x"]=_self.pos.x + range - Rand(range * 2),
						["y"]=_self.pos.y + range - Rand(range * 2)}
		end, self},
	move_randomly_within_spawn_range = thunk{
		function (_self, range)
			if Rand(20) <= 1 then --won't fire as often
				_self:move{	["x"]=_self.spawn_pos.x + range - Rand(range * 2),
							["y"]=_self.spawn_pos.y + range - Rand(range * 2)}
			end
		end, self},
	find_target = thunk{
		function (_self, flag)
			return char:wrap(GetChaByRange(_self.role, _self.pos.x, _self.pos.y, _self.vision, flag))
		end, self},
	seek_target = thunk{
		function (_self)
			if _self.ai>AI_R_ATK then
				local target_char = _self:nb_is_target_nearby(_self.host, _self.vision) or _self:find_target(0)
				if target_char ~= nil then
					_self:target(target_char)
					return true
				end
			end
			return false
		end, self},
	use_skill = thunk{
		function (_self, target_char, skill_id)
			return ChaUseSkill(_self.role, target_char.role, skill_id)
		end, self},
	move = thunk{
		function (_self, pos)
			if (IsPosValid(_self.role, pos.x, pos.y)~= 0) then
				return ChaMove(_self.role, pos.x, pos.y)
			end
		end, self},
	move_to_sleep = thunk{
		function (_self, pos)
			if (IsPosValid(_self.role, pos.x, pos.y)~= 0) then
				return ChaMoveToSleep(_self.role, pos.x, pos.y)
			end
		end, self}
}
--]]

char = {}

--[[
function char.__index(self, key)
	print('abc1')
	char.index = {
		mapname = thunk{GetChaMapName,{self.role}}, -- GetChaMapName(self.role)
		mobid = thunk{GetChaID,{self.role}},
		id = thunk{GetCharID,{self.role}},
		type_id = thunk{GetChaTypeID,{self.role}},
		mapcopy = thunk{GetChaMapCopy,{self.role}},
		partyid = thunk{GetChaTeamID,{self.role}},
		ai = thunk{GetChaAIType,{self.role}},
		target = thunk{char.wrap,thunk{GetChaTarget,{self.role}}}, -- char.wrap(GetChaTarget(self.role))
		reveal_players_on_sight = thunk{ClearHideChaByRange,{self.role, 0, 0, self.vision, 0}}, -- ClearHideChaByRange(self.role, 0, 0, self.vision, 0)
		blocked_moves = thunk{GetChaBlockCnt,{self.role}},
		face_pos = thunk{GetChaFacePos,{self.role}},
		pos = thunk{GetChaPos,{self.role}},
		spawn_pos = thunk{GetChaSpawnPos,{self.role}},
		first_target = thunk{char.wrap,thunk{GetChaFirstTarget,{self.role}}},
		patrol_state = thunk{GetChaPatrolState,{self.role}},
		chase_range = thunk{GetChaChaseRange,{self.role}},
		host = thunk{char.wrap,{GetChaHost,{self.role}}},
		name = thunk{GetChaDefaultName,{self.role}},
		stats = thunk{char_stats.wrap,{char_stats, self.role}},
		is_position_nearby = thunk{
			function (_self, position, radius)
				if position ~= nil then
					return point_distance(_self.pos, position) < radius
				end
			end, self},
		is_target_nearby = thunk{
			function (_self, target_char, radius)
				if target_char==nil then
					LG("ai_error", "is_target_nearby(): target_char doesn't exist")
					return false
				end
				return _self:is_position_nearby(_self.pos, target_char.pos)
			end, self},
		nb_is_target_nearby = thunk{
			function (_self, target_char, radius)
				if _self:is_target_nearby(target_char, radius) then
					return target_char
				end
			end, self},
		flee = thunk{
			function (_self, target_char)
			   _self:move(target_char:face_pos())
			end, self},
		move_randomly_within_range = thunk{
			function (_self, range)
				_self:move{	["x"]=_self.pos.x + range - Rand(range * 2),
							["y"]=_self.pos.y + range - Rand(range * 2)}
			end, self},
		move_randomly_within_spawn_range = thunk{
			function (_self, range)
				if Rand(20) <= 1 then --won't fire as often
					_self:move{	["x"]=_self.spawn_pos.x + range - Rand(range * 2),
								["y"]=_self.spawn_pos.y + range - Rand(range * 2)}
				end
			end, self},
		find_target = thunk{
			function (_self, flag)
				return char:wrap(GetChaByRange(_self.role, _self.pos.x, _self.pos.y, _self.vision, flag))
			end, self},
		seek_target = thunk{
			function (_self)
				if _self.ai>AI_R_ATK then
					local target_char = _self:nb_is_target_nearby(_self.host, _self.vision) or _self:find_target(0)
					if target_char ~= nil then
						_self:target(target_char)
						return true
					end
				end
				return false
			end, self},
		use_skill = thunk{
			function (_self, target_char, skill_id)
				return ChaUseSkill(_self.role, target_char.role, skill_id)
			end, self},
		move = thunk{
			function (_self, pos)
				if (IsPosValid(_self.role, pos.x, pos.y)~= 0) then
					return ChaMove(_self.role, pos.x, pos.y)
				end
			end, self},
		move_to_sleep = thunk{
			function (_self, pos)
				if (IsPosValid(_self.role, pos.x, pos.y)~= 0) then
					return ChaMoveToSleep(_self.role, pos.x, pos.y)
				end
			end, self}
	}
	
	print('abc2')
	rawset(self, key, self.index[key]()) -- will run all thunks inside.
	print('abc3')
end
--]]


function char.__index(self, key)
	if 	key == "mapname" then
		rawset(self, key, GetChaMapName(self.role))
	elseif 	key == "mobid" then
		rawset(self, key, GetChaID(self.role))
	elseif 	key == "id" then
		local id = GetCharID(self.role)
		if id ~= nil then
			rawset(self, key, id)
		end
	elseif 	key == "type_id" then
		rawset(self, key, GetChaTypeID(self.role))
	elseif 	key == "mapcopy" then
		rawset(self, key, GetChaMapCopy(self.role))
	elseif 	key == "partyid" then
		rawset(self, key, GetChaTeamID(self.role))
	elseif 	key == "ai" then
		rawset(self, key, GetChaAIType(self.role))
	elseif 	key == "target" then
		local _char = char:wrap(GetChaTarget(self.role))
		if _char ~= nil then
			rawset(self, key, _char)
		end
	elseif key == "reveal_players_on_sight" then
		rawset(self, key, function () return ClearHideChaByRange(self.role, 0, 0, t.vision, 0) end)
	elseif 	key == "blocked_moves" then
		rawset(self, key, GetChaBlockCnt(self.role))
	elseif 	key == "face_pos" then
		local x,y = GetChaFacePos(self.role)
		rawset(self, key, {["x"]=x,["y"]=y})
	elseif key == "pos" then
		local x,y = GetChaPos(self.role)
		rawset(self, key, {["x"]=x,["y"]=y})
	elseif key == "spawn_pos" then
		local x,y = GetChaSpawnPos(self.role)
		rawset(self, key, {["x"]=x,["y"]=y})
	elseif key == "patrol_pos" then
		local x,y = GetChaPatrolPos(self.role)
		rawset(self, key, {["x"]=x,["y"]=y})
	elseif 	key == "first_target" then
		local _char = char:wrap(GetChaFirstTarget(self.role))
		if _char ~= nil then
			rawset(self, key, _char)
		end
	elseif 	key == "patrol_state" then
		rawset(self, key, GetChaPatrolState(self.role))
	elseif 	key == "chase_range" then
		rawset(self, key, GetChaChaseRange(self.role))
	elseif 	key == "host" then
		local _char = char:wrap(GetChaHost(self.role))
		if _char ~= nil then
			rawset(self, key, _char)
		end
	elseif 	key == "name" then
		rawset(self, key, GetChaDefaultName(self.role))
	elseif 	key == "stats" then
		local _char_stats = char_stats:wrap(self.role)
		if _char_stats ~= nil then
			rawset(self, key, _char_stats)
		end
	elseif key == "is_position_nearby" then
		rawset(self, key, function (_self, position, radius)
			if position ~= nil then
				return point_distance(_self.pos, position) < radius
			end
		end)
	elseif key == "is_target_nearby" then
		rawset(self, key, function (_self, target_char, radius)
			if target_char==nil then
				LG("ai_error", "is_target_nearby(): target_char doesn't exist")
				return false
			end
			return _self:is_position_nearby(_self.pos, target_char.pos)
		end)
	elseif key == "nb_is_target_nearby" then
		rawset(self, key, function (_self, target_char, radius)
			if _self:is_target_nearby(target_char, radius) then
				return target_char
			end
		end)
	elseif key == "flee" then
		rawset(self, key, function (_self, target_char)
		   _self:move(target_char:face_pos())
		end)
	elseif key == "move_randomly_within_range" then
		rawset(self, key, function (_self, range)
			_self:move{	["x"]=_self.pos.x + range - Rand(range * 2),
						["y"]=_self.pos.y + range - Rand(range * 2)}
		end)
	elseif key == "move_randomly_within_spawn_range" then
		rawset(self, key, function (_self, range)
			if Rand(20) <= 1 then --won't fire as often
				_self:move{	["x"]=_self.spawn_pos.x + range - Rand(range * 2),
							["y"]=_self.spawn_pos.y + range - Rand(range * 2)}
			end
		end)
	elseif key == "find_target" then
		rawset(self, key, function (_self, flag)
			return char:wrap(GetChaByRange(_self.role, _self.pos.x, _self.pos.y, _self.vision, flag))
		end)
	elseif key == "seek_target" then
		rawset(self, key, function (_self)
			if _self.ai>AI_R_ATK then
				local target_char = _self:nb_is_target_nearby(_self.host, _self.vision) or _self:find_target(0)
				if target_char ~= nil then
					_self:target(target_char)
					return true
				end
			end
			return false
		end)
	elseif key == "use_skill" then
		rawset(self, key, function (_self, target_char, skill_id)
			return ChaUseSkill(_self.role, target_char.role, skill_id)
		end)
	elseif key == "move" then
		rawset(self, key, function (_self, pos)
			if (IsPosValid(_self.role, pos.x, pos.y)~= 0) then
				return ChaMove(_self.role, pos.x, pos.y)
			end
		end)
	elseif key == "move_to_sleep" then
		rawset(self, key, function (_self, pos)
			if (IsPosValid(_self.role, pos.x, pos.y)~= 0) then
				return ChaMoveToSleep(_self.role, pos.x, pos.y)
			end
		end)
	end
	return rawget(self, key)
end

function char.__newindex(self, key, value)
	if key == "name" then
		print("Character Structure: Can't set name of character.")
	elseif key == "blocked_moves" then
		SetChaBlockCnt(self.role, value)
	elseif key == "life_time" then
		SetChaLifeTime(self.role, value)
	elseif key == "patrol_state" then
		SetChaPatrolState(self.role, value)
	elseif key == "target" then
		SetChaTarget(self.role, value.role)
	end
	rawset(self, key, value)
end

function char:wrap(role)
	if role ~= nil and role ~= 0 then --role needs to exist
		local o = {}
		o.role = role
		setmetatable(o, self)
		
		return o
	end
	--return nil
end