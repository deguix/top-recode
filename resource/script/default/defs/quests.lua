function quest_init(id, player, char, selindex)
	--Initialization action has player and char == nil. Please check if player is nil before using it.
	--Cancel action has char == nil. Please check if char is nil before using it.
	--TODO: Add sub-index for the "series quests" as in "random" quests (they're actually supposed to be a series number).
	local q = quests[id] -- no "random" quests yet
	if id == 1 then
		q[1].name = 'Normal quest test' --..' - series number '..sub_id
		q[1].type = QUEST_TYPE_NORMAL
		q[1].states[QUEST_STATE_START].text = 'This quest only appears after you get a Ticket to Argent. Testing if quest is initialized/cancelled/ended/viewed properly. Also testing for conditonals and actions. 2 short swords are being given out once you accept this. They are going to be automatically removed if mission is cancelled. The reward gives 1 ticket to argent. Also testing other indicators.'
		q[1].states[QUEST_STATE_END].text = 'This is the end of the quest. You are going to be rewarded for patiently testing this.'
		q[1].states[QUEST_STATE_ONGOING].text = 'This text appears on the on going log. To be able to finish or cancel the quest, you need the 2 short swords that were given to you. If you want more details, abandon the quest and start it again.'
		q[1].states[QUEST_STATE_LOG].text = 'This text appears on quest log. You cannot abandon this if you do not have the 2 short swords that were given to you. If you want more details, abandon the quest and start it again.'
		
		--q[1].indicators --global indicators go in this table. Most will use this unless NPC won't reveal rewards until later.
		q[1].indicators.needs[1] = {type=MIS_NEED_ITEM,1,2}
		q[1].indicators.prizes.seltype = PRIZE_SELONE
		q[1].indicators.prizes[1] = {type=MIS_PRIZE_ITEM,4602,1}
		q[1].indicators.prizes[2] = {type=MIS_PRIZE_ITEM,4603,1}
		
		--TODO: make selection groups of indicator prizes (example: give 1 prize always, select 1 or 2 prizes from the remaining). More complex because it might need some C++ changes.
		q[1].states[QUEST_STATE_START].indicators = {needs={}, prizes={}}
		--q[1].states[QUEST_STATE_START].indicators.needs[1] = {type=MIS_NEED_ITEM,4602,1}
		--q[1].states[QUEST_STATE_START].indicators.prizes[1] = {type=MIS_PRIZE_ITEM,1,2}
		--q[1].states[QUEST_STATE_START].indicators.needs[1] = {type=MIS_NEED_ITEM,1,2}
		q[1].states[QUEST_STATE_START].indicators.prizes.seltype = PRIZE_SELALL
		q[1].states[QUEST_STATE_START].indicators.prizes[1] = {type=MIS_PRIZE_ITEM,4602,1}
		q[1].states[QUEST_STATE_START].indicators.prizes[2] = {type=MIS_PRIZE_ITEM,4603,1}
		----q[1].states[QUEST_STATE_START].indicators = needs={{type=MIS_NEED_ITEM,4602,1}}, prizes={seltype=PRIZE_SELALL, {type=MIS_PRIZE_ITEM,1,2}}} --compact form
		--q[1].states[QUEST_STATE_END].indicators = {needs={}, prizes={}}
		--q[1].states[QUEST_STATE_END].indicators.needs[1] = {type=MIS_NEED_ITEM,1,2}
		--q[1].states[QUEST_STATE_END].indicators.prizes[1] = {type=MIS_PRIZE_ITEM,4602,1}
		--q[1].states[QUEST_STATE_LOG].indicators = {needs={}, prizes={}}
		--q[1].states[QUEST_STATE_LOG].indicators.needs[1] = {type=MIS_NEED_ITEM,1,2} --canceling quest will not return the Ticket to Argent.
		
		--q[1].states[QUEST_STATE_ONGOING].indicators = {needs={}, prizes={}}
		--other prize indicators:
		--{type=MIS_PRIZE_MONEY, 0, 1 } --gives money
		--{type=MIS_PRIZE_ITEM, 4602, 1} --gives item
		--{type=MIS_PRIZE_FAME, 0, 1} --gives reputation
		--{type=MIS_PRIZE_CESS, 0, 1} --gives sea reputation
		
		print("SelIndex: ",selindex)
		if ((player ~= nil) and (char ~= nil)) then --meaning: while talking to a npc or picking up quest - and not before (everything else is static - don't change per player).
			--for quest to appear at npc requires QUEST_STATE_START conditions to happen - actions give items after player starts mission.
			q[1].states[QUEST_STATE_START].conditions[1]={HasItem,{player.role, 4602, 1}} --GetMissionIndex gives error if selected quest doesn't exist (if these conditions are not met) - made error msg blank on en_US.res
			q[1].states[QUEST_STATE_START].actions[1]={GiveItem,{player.role, char.role, 1, 2, 1}}
			--to be able to finish quest, player needs to satisfy QUEST_STATE_END conditions - actions happen once complete is clicked.
			q[1].states[QUEST_STATE_END].conditions[1]={HasItem,{player.role, 1, 2}}
			q[1].states[QUEST_STATE_END].actions[1]={TakeItem,{player.role, char.id, 1, 2}}
			q[1].states[QUEST_STATE_END].actions[2]={GiveItem,{player.role, char.role, _te({[2]=4603},4602)[selindex], 1, 1}}
			--to be able to abandon quest, player needs to satisfy QUEST_STATE_LOG conditions - actions happen once abandon is clicked.
			q[1].states[QUEST_STATE_LOG].conditions[1]={HasItem,{player.role, 1, 2}}
			q[1].states[QUEST_STATE_LOG].actions[1]={TakeItem,{player.role, char.id, 1, 2}}
		end
	end
	if id == 2 then
		q[1].name = 'Multiple char normal quest test'
		q[1].type = QUEST_TYPE_NORMAL
		q[1].states[QUEST_STATE_START].text = 'starttext'
		q[1].states[QUEST_STATE_END].text = 'endtext'
		q[1].states[QUEST_STATE_ONGOING].text = 'pendingtext'
		q[1].states[QUEST_STATE_LOG].text = 'logtext'

		if char ~= nil then -- this allows multiple npcs to have the same start/end quest section (unlike most other games).
			if char.name == 'Blacksmith - Goldie' then
				q[1].show = ACCEPT_SHOW
			elseif char.name == 'Forbei' then
				q[1].show = COMPLETE_SHOW
				q[1].states[QUEST_STATE_ONGOING].text = 'pendingtext2'
			end
		end
		
		q[1].indicators.needs[1] = {type=MIS_NEED_ITEM,1,10} --needs 10 swords to finish

		if ((player ~= nil) and (char ~= nil)) then
			--q.states[QUEST_STATE_START].actions[1]={GiveItem,{player.role, char.role, 1, 1, 4} }
			q[1].states[QUEST_STATE_END].conditions[1]={HasItem,{player.role, 1, 10} }
			--q[1].states[QUEST_STATE_END].actions[1]={TakeItem,{player.role, char.id, 1, 10}}
		end
	end
	if id == 3 then
		q[1].name = 'Global quest'
		q[1].type = QUEST_TYPE_GLOBAL
		q[1].states[QUEST_STATE_START].text = 'starttext'
		q[1].states[QUEST_STATE_END].text = 'endtext'
		q[1].states[QUEST_STATE_ONGOING].text = 'pendingtext'
		q[1].states[QUEST_STATE_LOG].text = 'logtext'
		
		q[1].indicators.needs[1] = {type=MIS_NEED_ITEM,1,10} --needs 10 swords to finish

		if ((player ~= nil) and (char ~= nil)) then
			--q.states[QUEST_STATE_START].actions[1]={GiveItem,{player.role, char.role, 1, 1, 4} }
			q[1].states[QUEST_STATE_END].conditions[1]={HasItem,{player.role, 1, 10} }
			--q[1].states[QUEST_STATE_END].actions[1]={TakeItem,{player.role, char.id, 1, 10}}
		end
	end
end