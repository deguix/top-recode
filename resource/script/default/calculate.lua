function recalculate(self) --stats --namespace pollution but no choice in this case -- dunno why it only works in skills...
	print('recalculate')
	AttrRecheck(self.role)
end

function char_stats_getcalcbase(key)
	if ((key >= ATTR_COUNT_BASE1) and (key <= (ATTR_COUNT_BASE1 + 21))) then -- full stat
		return key -- if a gm tries to set player stats manually, this will step-in, reversing the process automatically.
	elseif ((key >= ATTR_COUNT_BASE2) and (key <= (ATTR_COUNT_BASE2 + 21))) then -- base stat
		return (key - ATTR_COUNT_BASE2) + ATTR_COUNT_BASE1
	elseif ((key >= ATTR_COUNT_BASE3) and (key <= (ATTR_COUNT_BASE3 + 21))) then --
		return item_char_stats_conv[(key - ATTR_COUNT_BASE3)] + ATTR_COUNT_BASE1
	elseif ((key >= ATTR_COUNT_BASE4) and (key <= (ATTR_COUNT_BASE4 + 21))) then
		return item_char_stats_conv[(key - ATTR_COUNT_BASE4)] + ATTR_COUNT_BASE1
	elseif ((key >= ATTR_COUNT_BASE5) and (key <= (ATTR_COUNT_BASE5 + 21))) then
		return item_char_stats_conv[(key - ATTR_COUNT_BASE5)] + ATTR_COUNT_BASE1
	elseif ((key >= ATTR_COUNT_BASE6) and (key <= (ATTR_COUNT_BASE6 + 21))) then
		return item_char_stats_conv[(key - ATTR_COUNT_BASE6)] + ATTR_COUNT_BASE1
	end
	return -1
end

function resetme (role)
	local player_skills = {}
	--[[
	for i,v in ipairs(range(1,500)) do --max number of skills
		--player_skills[v] = GetSkillLv(role, v)
		local skill_level = GetSkillLv(role, v)
		if skill_level > 0 then
			skill(PLAYER_SKILL, v, SK_EVENT_UNUSE, GetSkillLv(role, v), role)
		end
	end
	--]]
	ClearFightSkill(role)
	RefreshCha ( role )
	--BUG: Need to call all passive skills of the player with "unuse" event. Attack getting negative for some reason.
end

function relearn_main_skills(role)
	local skill_list = { --TODO: Add skill requirements to advanced skill guide in top
	--TODO: Include Rebirth skills
	--TODO: Also include passive weapon skills
		"Taunt",
		"Roar", --SK_TAUNT lvl 10
		"Concentration",
		"Sword Mastery", --SK_CONCENTRATION lvl 2
		"Will of Steel", --SK_CONCENTRATION lvl 2
		"Illusion Slash", --SK_SWORDMASTERY lvl 3
		"Break Armor", --SK_SWORDMASTERY lvl 3
		"Berserk", --SK_ILLUSIONSLASH lvl 4
		"Tiger Roar", --SK_BREAKARMOR lvl 4
		"Dual Sword Mastery", --SK_BERSERK lvl 5
		"Deftness", --SK_DUALSWORDMASTERY lvl 2
		"Blood Frenzy", --SK_DUALSWORDMASTERY lvl 2
		"Poison Dart", --SK_BLOODFRENZY lvl 2
		"Shadow Slash", --SK_BLOODFRENZY lvl 5
		"Stealth", --SK_DEFTNESS lvl 3
		"The Windrider's Grace", --SK_SHADOWSLASH lvl 5
		"Greatsword Mastery", --SK_TIGERROAR lvl 5
		"Strengthen", --SK_GREATSWORDMASTERY lvl 2
		"Bloodbull", --SK_GREATSWORDMASTERY lvl 3
		"Primal Rage", --SK_BLOODBULL lvl 5
		"Warrior's Rage", --SK_BLOODBULL lvl 5, lvl 125
		"Mighty Strike", --SK_STRENGTHEN lvl 3
		"Howl", --SK_MIGHTYSTRIKE lvl 2
		
		"Range Mastery",
		"Eagle's Eye",
		"Windwalk", --SK_RANGEMASTERY lvl 2
		"Dual Shot", --SK_RANGEMASTERY lvl 3
		"Firegun Mastery", --SK_RANGEMASTERY lvl 10
		"Rousing", --SK_WINDWALK lvl 4
		"Frozen Arrow", --SK_DUALSHOT lvl 2
		"Venom Arrow", --SK_DUALSHOT lvl 4
		"Blunting Bolt", --SK_DUALSHOT lvl 4, lvl 125
		"Flaming Dart", --SK_DUALSHOT lvl 4, lvl 125
		"Meteor Shower", --SK_FROZENARROW lvl 5
		"Cripple", --SK_FROZENARROW lvl 5
		"Magma Bullet", --SK_FIREGUNMASTERY lvl 5
		"Enfeeble", --SK_CRIPPLE lvl 5
		"Headshot", --SK_ENFEEBLE lvl 5
		"Eye of Precision", --SK_HEADSHOT lvl 4, lvl 125
		"Deadeye Blood", --SK_HEADSHOT lvl 4, lvl 125
		
		"Heal",
		"Spiritual Bolt",
		"Vigor",
		"Crystalline Blessing",
		"Intense Magic",
		"Harden", --SK_HEAL lvl 3
		"Recover", --SK_HARDEN lvl 3
		"Revival", --SK_RECOVER lvl 4
		"Spiritual Fire", --SK_SPIRITUALBOLT lvl 2
		"Tempest Boost", --SK_SPIRITUALFIRE lvl 4
		"Divine Grace", --SK_VIGOR lvl 8
		"True Sight", --SK_DIVINEGRACE lvl 2
		"Tornado Swirl", --SK_TRUESIGHT lvl 2
		"Cursed Fire", --SK_TRUESIGHT lvl 3
		"Angelic Shield", --SK_TORNADOSWIRL lvl 2
		"Energy Shield", --SK_TORNADOSWIRL lvl 3
		"Healing Spring", --SK_ANGELICSHIELD lvl 2
		"Soulkeeper", --SK_ENERGYSHIELD lvl 10
		"Abyss Mire", --SK_CURSEDFIRE lvl 4
		"Shadow Insignia", --SK_CURSEDFIRE lvl 4
		"Bane of Waning", --SK_CURSEDFIRE lvl 5, lvl 125
		"Curse of Weakening", --SK_CURSEDFIRE lvl 10, lvl 125
		"Seal of Elder", --SK_SHADOWINSIGNIA lvl 3
		"Excrucio", --SK_SHADOWINSIGNIA lvl 10, lvl 125
		"Petrifying Pummel", --SK_ABYSSMIRE lvl 8, lvl 125
		
		"Diligence",
		"Lightning Bolt", --SK_DILIGENCE lvl 1
		"Current", --SK_DILIGENCE lvl 2
		"Lightning Curtain", --SK_LIGHTNINGBOLT lvl 8
		"Conch Armor", --SK_CURRENT lvl 1
		"Tornado", --SK_CURRENT lvl 2
		"Conch Ray", --SK_CONCHARMOR lvl 5
		"Omnis Immunity", --SK_CONCHRAY lvl 10, lvl 125
		"Tail Wind", --SK_TORNADO lvl 4
		"Algae Entanglement", --SK_TORNADO lvl 5
		"Fog", -- SK_TAILWIND lvl 2
		"Whirlpool", --SK_TAILWIND lvl 4
	}
	
	for i,skill_id in ipairs(skill_list) do
		if tsv.skillinfo[skill_id] ~= nil then 
			print('Added skill name: '..skill_id..', id: '..tsv.skillinfo[skill_id]['id'])
			AddChaSkill(role,tsv.skillinfo[skill_id]['id'],10,1,0) --TODO: Remove class restriction from skillinfo.
		end
	end
end

function relearn_weapon_skills(role)
	--passive weapon skills

	local skill_list = {
		"Bare Hand",
		"Sword",
		"Greatsword",
		"Bow",
		"Firegun",
		"Blade",
		"Boxing Gloves",
		"Dagger",
		"Short Staff",
		"Hammer",
		"Dual Weapon",
		"Woodcutting",
		"Mining"
	}
	
	for i,skill_id in ipairs(skill_list) do
		AddChaSkill(role,tsv.skillinfo[skill_id]['id'],1,1,0)
	end
end

function relearn_new_main_skills(role)
	local skill_list = {
		"The Warrior's Rage",
		"The Windrider's Grace",
		"Blunting Bolt",
		"Flaming Dart",
		"Eye of Precision",
		"Deadeye Blood",
		"Excrucio",
		"Petrifying Pummel",
		"Curse of Weakening",
		"Bane of Waning",
		"Omnis Immunity"
	}
	
	for i,skill_id in ipairs(skill_list) do
		AddChaSkill(role,tsv.skillinfo[skill_id]['id'],10,1,0) --TODO: Remove class restriction from skillinfo.
	end
	
	local skill_list = {
		"The Warrior's Rage",
		"The Windrider's Grace",
		"Soulkeeper"
	}
	
	for i,skill_id in ipairs(skill_list) do
		AddChaSkill(role,tsv.skillinfo[skill_id]['id'],1,1,0) --TODO: Remove class restriction from skillinfo.
	end
end

function relearn_unreleased_skills(role) --only learning those with effects right now - also most of these are hidden
	local skill_list = {
		"Eagle's Eye",
		"Angel Blessing",
		"Cure",
		"Frost Shield",
		"Traversing",
		"Inferno Blast",
		"Evasion",
		"Beast Strength",
		"Recuperate",
		"Shield Mastery",
		"Ranger",
		"Hunter Disguise",
		"Backstab",
		"Numb",
		"Hunter Strike",
		"Astro Strike",
		"Barbaric Crush",
		"Parry",
		"Dispersion Bullet",
		"Penetrating Bullet",
		"Greater Heal",
		"Greater Recover",
		"Counterguard",
		--ship skils - they work, but Bombardment replaces the auto attack of any other weapon skill.
--					"Cannon Mastery",
--					"Toughen Wood",
--					"Sail Mastery",
--					"Reinforce Ship",
--					"Oil Tank Upgrade",
--					"Bombardment"
	}
	
	for i,skill_id in ipairs(skill_list) do
		print(tsv.skillinfo[skill_id]['id'])
		AddChaSkill(role,tsv.skillinfo[skill_id]['id'],1,1,0) --TODO: Remove class restriction from skillinfo.
	end
end

function increase_luck(p)
	local ps = p.stats
	if ps[ATTR_AP] > 0 then
		ps[ATTR_BLUK] = ps[ATTR_BLUK] + 1
		ps[ATTR_AP] = ps[ATTR_AP] - 1
		jtp(1)
		AttrRecheck(p.role)
		RefreshCha(p.role) --Refreshes client.
	end
end

--Char creation function
function CreatCha(role)
	local stats = player:wrap(role).stats
	stats[ATTR_AP] = stats[ATTR_AP] + 4
	print('testing-3')
	AttrRecheck(role)
	stats[ATTR_HP] = stats[ATTR_MXHP]
	stats[ATTR_SP] = stats[ATTR_MXSP]
	stats[ATTR_BSTR]=5
	stats[ATTR_BACC]=5
	stats[ATTR_BAGI]=5
	stats[ATTR_BCON]=5
	stats[ATTR_BSPR]=5
	stats[ATTR_BLUK]=5
	print('testing-2')
	AttrRecheck(role)
	
	--Same as init_cha_item.txt file:
	if IsPlayer(role) then
		print('testing-1')
		GiveItem(role, 0, 0289, 1, 0)
		GiveItem(role, 0, 0641, 1, 0)
		GiveItem(role, 0, 0008, 1, 0)
		print('testing0')
		
		--Make it a pk server:
		print('testing1')
		stats[ATTR_LV]=1
		print('testing2')
		stats[ATTR_TP]=0
		print('testing3')
		
		AttrRecheck(role)
		relearn_skills(role)
	end
end

function Shengji_Shuxingchengzhang( role ) --LVL UP
	print('Shengji_Shuxingchengzhang')
	local stats = player:wrap(role).stats
	
	stats[ATTR_BSTR]=stats[ATTR_BSTR]
	stats[ATTR_BACC]=stats[ATTR_BACC]
	stats[ATTR_BAGI]=stats[ATTR_BAGI]
	stats[ATTR_BCON]=stats[ATTR_BCON]
	stats[ATTR_BSPR]=stats[ATTR_BSPR]
	stats[ATTR_BLUK]=stats[ATTR_BLUK]

	local lv = stats[ATTR_LV]
	local ap_extre = 0
	
	if (math.floor(lv/10)-math.floor((lv-1)/10)) == 1 then 
		ap_extre = 5 
	else
		ap_extre = 1 
	end 

	if lv >= 60 then
		ap_extre = ap_extre + 1 
	end 
	if lv >= 105 then 
		ap_extre = 1
	end

	stats[ATTR_AP]=stats[ATTR_AP]+ap_extre
	
	local tp_extre = 0
	
	if lv > 9 then 
		tp_extre = 1 
	end 

	if lv >= 65 then 
		if (math.floor (lv/5)-math.floor ((lv-1)/5)) == 1 then 
			tp_extre = 2
		else
			tp_extre = 1
		end
	end
	
	stats[ATTR_TP]=stats[ATTR_TP]+tp_extre

	AttrRecheck(role)
end

function ALLExAttrSet ( role )				--根据角色形态分别刷新当前属性
	print('ALLExAttrSet')
	--[[
	if IsPlayer ( role ) == 0 then				--角色为怪物
		ExAttrSet ( role ) 
		return 
	end 
	if ChaIsBoat ( role ) == 0 then			--角色形态为人物
		AttrRecheck ( role ) 
	else								--角色形态为船只
		cha_role = GetMainCha ( role ) 
		ShipAttrRecheck ( cha_role , role ) 
	end 
	--]]
	AttrRecheck ( role ) 
end 

--Stat recheck function
function AttrRecheck(role)
	print('AttrRecheck')
	--[[
	Function called whenever stats are recalculated. The following must apply in order:
	- Base primary stats should not be set here, because player is the one that customizes it.
	- Current primary stats have to be recalculated first. Current secondary stats are based on them.
	- Base secondary stats are set. These don't change with class.
	- Then those are updated with class based stats.
	- In the end, secondary stats are recalculated with the updated base secondary stats.
	--]]

	local stats = player:wrap(role).stats
	
	--make main stats recalculate themselves by setting them to something (because main ones have to be recalculated from parts).
	local i=ATTR_COUNT_BASE1
	for i=ATTR_COUNT_BASE1,ATTR_COUNT_BASE1+5 do
		stats[i]=0
	end

	function sr(ratio)
		return class_sr[stats[ATTR_CLASS]][ratio+1]
	end
	function rsr(ratio, relation)
		return class_sr[class_relation(stats[ATTR_CLASS],relation)][ratio+1]
	end

	--Base stats stick after logout, so don't set base values.
	--TODO: Put this stat initialization on EudemonMission (or on a function for that startup).
	--stats[ATTR_BSTR]=5
	--stats[ATTR_BACC]=5
	--stats[ATTR_BAGI]=5
	--stats[ATTR_BCON]=5
	--stats[ATTR_BSPR]=5
	--stats[ATTR_BLUK]=5
	stats[ATTR_BMXHP]=40
	stats[ATTR_BMXSP]=5
	stats[ATTR_BATK]=1
	stats[ATTR_BDEF]=0
	stats[ATTR_BMATK]=1
	stats[ATTR_BMDEF]=0
	stats[ATTR_BHIT]=0
	stats[ATTR_BFLEE]=0
	stats[ATTR_BCRT]=0
	stats[ATTR_BCRTR]=0
	stats[ATTR_BDROP]=100
	stats[ATTR_BHREC]=5 -- was 0
	stats[ATTR_BSREC]=5 -- was 0
	stats[ATTR_BASPD]=100
	stats[ATTR_BMSPD]=450
	stats[ATTR_BEXP]=100

	--put class specific stats here

	--Main stats (additive main stats with level):
	--Disabled because everytime base stats change, this adds tons of stats to player.
	--[[
	if (class_level(stats[ATTR_CLASS]) >= 1) and (stats[ATTR_LV] >= class_req_plvl(stats[ATTR_CLASS])) then
	for main_stat_idx=ATTR_BSTR, ATTR_BLUK do
	stats[main_stat_idx]=stats[main_stat_idx]+
	sr(CLASS_RATIO_STR_LVL+(main_stat_idx-ATTR_BSTR))*(stats[ATTR_LV]-class_req_plvl(stats[ATTR_CLASS]))

	if (class_level(stats[ATTR_CLASS]) == 2) then
	stats[main_stat_idx]=stats[main_stat_idx]+
	rsr(CLASS_RATIO_STR_LVL+(main_stat_idx-ATTR_BSTR),"<")*(class_req_plvl(stats[ATTR_CLASS])-class_req_plvl(class_relation(stats[ATTR_CLASS],"<")))
	end
	end
	end
	--]]

--[[
CLASS_RATIO_STR_LVL		= 0
CLASS_RATIO_ACC_LVL		= 1
CLASS_RATIO_AGI_LVL		= 2
CLASS_RATIO_CON_LVL		= 3
CLASS_RATIO_SPR_LVL		= 4
CLASS_RATIO_LUK_LVL		= 5
CLASS_RATIO_HP_CON		= 6
CLASS_RATIO_HP_LVL 		= 7
CLASS_RATIO_SP_SPR 		= 8
CLASS_RATIO_SP_LVL		= 9
CLASS_RATIO_ATK_STR		= 10
CLASS_RATIO_ATK_ACC		= 11
CLASS_RATIO_ATK_LUK		= 12
CLASS_RATIO_MATK_SPR	= 13
CLASS_RATIO_MATK_LUK	= 14
CLASS_RATIO_DEF_CON		= 15
CLASS_RATIO_MDEF_LUK	= 16
CLASS_RATIO_HIT_ACC		= 17
CLASS_RATIO_HIT_LUK		= 18
CLASS_RATIO_FLEE_AGI	= 19
CLASS_RATIO_FLEE_LUK	= 20
CLASS_RATIO_DROP_LUK	= 21
CLASS_RATIO_CRT_LUK		= 22
CLASS_RATIO_CRTR_CON	= 23
CLASS_RATIO_CRTR_SPR	= 24
CLASS_RATIO_HREC_BMXHP	= 25
CLASS_RATIO_HREC_CON	= 26
CLASS_RATIO_SREC_BMXSP	= 27
CLASS_RATIO_SREC_SPR	= 28
CLASS_RATIO_ASPD_AGI	= 29
CLASS_RATIO_MSPD_AGI	= 30
--]]

--									STATS					HP		SP			ATK						MATK			DEF		MDEF	HIT				FLEE			DROP	CRT		CRTR			HREC			SREC			APSD	MSPD
--	class_sr[CLASS_NEWBIE]={		0,	0,	0,	0,	0,	0,	1,	15,	0.1,	3,	0.3,	0.3,	0.1,	0.3,	0.1,	0.10,	0.10,	0.5,	0.1,	0.5,	0.1,	0.20,	0.10,	0.10,	0.15,	0.005,	0.125,	0.005,	0.050,	0.9,	0.20}
--	class_sr[CLASS_BERSERKER]={		2,	0,	0,	0,	0,	0,	3,	25,	0.5,	5,	1.0,	0.0,	0.3,	0.0,	0.0,	0.18,	0.18,	0.7,	0.1,	0.7,	0.1,	0.45,	0.40,	0.40,	0.0,	0.015,	0.375,	0.015,	0.100,	1.1,	0.20}

	--uses, for example, ATTR_CON instead of ATTR_BCON, because secondary stats are based on what the current CON is, not base CON.
	stats[ATTR_BMXHP]=stats[ATTR_BMXHP]+
	5*stats[ATTR_CON]*3+
	5*(math.floor(stats[ATTR_CON]/5)^2)+
	50*stats[ATTR_LV]

	stats[ATTR_BMXSP]=stats[ATTR_BMXSP]+
	2*stats[ATTR_SPR]*3+
	1*(math.floor(stats[ATTR_SPR]/5)^2)+
	4*stats[ATTR_LV]

	stats[ATTR_BATK]=
	(
	1*stats[ATTR_STR]+
	0.33*stats[ATTR_ACC]+
	0.20*stats[ATTR_LUK]
	)+
	(
	1*(math.floor(stats[ATTR_STR]/5)^2)+
	0.33*(math.floor(stats[ATTR_ACC]/5)^2)+
	0.20*(math.floor(stats[ATTR_LUK]/5)^2)
	)

	stats[ATTR_BDEF]=5+
	0.15*stats[ATTR_CON]*5+
	0.15*(math.floor(stats[ATTR_CON]/5)^2)

	stats[ATTR_BMATK]=
	(
	1*stats[ATTR_SPR]+
	0.33*stats[ATTR_ACC]+
	0.20*stats[ATTR_LUK]
	)+
	(
	1*(math.floor(stats[ATTR_SPR]/5)^2)+
	0.33*(math.floor(stats[ATTR_ACC]/5)^2)+
	0.20*(math.floor(stats[ATTR_LUK]/5)^2)
	)

	stats[ATTR_BMDEF]=5+
	0.15*stats[ATTR_LUK]*5+
	0.15*(math.floor(stats[ATTR_LUK]/5)^2)

	stats[ATTR_BHIT]=5+
	1*stats[ATTR_ACC]+
	stats[ATTR_LV]*2

	stats[ATTR_BFLEE]=5+
	1*stats[ATTR_AGI]+
	stats[ATTR_LV]*2

	stats[ATTR_BCRT]=11+
	0.3*stats[ATTR_LUK]*3

	stats[ATTR_BCRTR]=11+
	0.15*stats[ATTR_CON]*3+
	0.15*stats[ATTR_SPR]*3

	stats[ATTR_BDROP]=100+
	0.10*stats[ATTR_LUK]*3

	stats[ATTR_BEXP]=100

	stats[ATTR_BHREC]=math.max(
	0.0125*stats[ATTR_CON]*3+
	0.0125*stats[ATTR_BMXHP]*2
	,1)

	stats[ATTR_BSREC]=math.max(
	0.01*stats[ATTR_SPR]*3/2+
	0.01*stats[ATTR_BMXSP]/2
	,1)

	stats[ATTR_BASPD]=math.floor(math.min(math.floor(65+
	1.1*stats[ATTR_AGI]
	,300)))

	print('testing0')
	stats[ATTR_BMSPD]=450+
	1*stats[ATTR_AGI]

	--checking fusion:

	--[[
	local cha = TurnToCha ( role )
	local Cha_Num = GetChaTypeID( cha )


	local body = GetChaItem ( role , 1 , 2 )
	local hand = GetChaItem ( role , 1 , 3 )
	local foot = GetChaItem ( role , 1 , 4 )


	local Body_ID = GetItemID ( body )
	local Hand_ID = GetItemID ( hand )
	local Foot_ID = GetItemID ( foot )
	local body_gem_id = GetItemAttr ( body , ITEMATTR_VAL_FUSIONID )
	local hand_gem_id = GetItemAttr ( hand , ITEMATTR_VAL_FUSIONID )
	local foot_gem_id = GetItemAttr ( foot , ITEMATTR_VAL_FUSIONID )
	--]]

	--GetChaItem function description:
	--2nd param: 1 = Body, 2 = Bag, 3 = Temp Bag (probably).
	--print(players[0].role)
	--GiveItemX(role,0,825,1,20)

	--SetItemAttr(GetChaItem(role,1,2),ITEMATTR_VAL_FUSIONID,0))
	--local test = FusionItem(GetChaItem(role,1,2), GetChaItem2(role,2,825))
	--SetItemAttr(GetChaItem(role,1,2),ITEMATTR_VAL_LEVEL,50)
	--SetItemAttr (GetChaItem(role,1,2), ITEMATTR_MAXURE , 25000 )
	--SetItemAttr (GetChaItem(role,1,2), ITEMATTR_URE , 25000 )

	--SetItemAttr(GetChaItem(role,2,1),ITEMATTR_VAL_FUSIONID,5000))
	---SetItemAttr(GetChaItem(role,2,1),ITEMATTR_VAL_LEVEL,50)
	---SetItemAttr (GetChaItem(role,2,1), ITEMATTR_MAXURE , 25000 )
	--SetItemAttr (GetChaItem(role,2,1), ITEMATTR_URE , 25000 )

	--print(GetItemName(GetItemID(GetChaItem(role,1,2))))

	--for i=ITEMATTR_COUNT_BASE0,ITEMATTR_VAL_FUSIONID do
	--	print(GetItemAttr(GetChaItem(role,1,2) , i))
	--SetItemAttr ( GetChaItem(role,1,2) , ITEMATTR_VAL_STR , 1 )
	--end
	--SetItemFinalAttr( GetChaItem(role,1,2) , ITEMATTR_VAL_STR , 1 )

	--SetItemAttr can edit the following (of equips):
	--ENERGY,MAXENERGY,ATK,MATK,DEF,MDEF.
	--All other items can have everything editted.

	--ResetItemFinalAttr(GetChaItem(role,1,2))

	--print(DealAllPlayerInMap())

	--SynChaKitbag(role,13)

	--print(GetItemAttrRange(GetChaItem(role,1,2), ITEMATTR_VAL_MAXENERGY))

	--check if main glove weapon is equipped, then remove any other weapon equipped with it except the left glove gem holder.
	local main_hand_weapon = item_wrap(GetChaItem(role,1,9),player)
	local off_hand_weapon = item_wrap(GetChaItem(role,1,6),player)
	
	print('test2')
	if (off_hand_weapon ~= -1) and (off_hand_weapon.type == 6) then
		if (main_hand_weapon == -1) then
			RemoveChaItem(player.role, 0, 1, 1, 6, 1, 0, 1)
		elseif ((main_hand_weapon ~= 1) and ((main_hand_weapon.type ~= 6) or (main_hand_weapon.id ~= (off_hand_weapon.id-139)))) then
			RemoveChaItem(player.role, 0, 1, 1, 9, 1, 0, 1)
		end
	end
	
	print('test3')
	if (main_hand_weapon ~= -1) and (main_hand_weapon.type == 6) then
		if (off_hand_weapon ~= -1) and ((off_hand_weapon.type ~= 6) or (off_hand_weapon.id ~= (main_hand_weapon.id+139))) then
			RemoveChaItem(player.role, 0, 1, 1, 6, 1, 0, 1)
		end
	end
	print('test4')
	
	-- Skill list: Has to follow requirements, but char already starts with lvl 130, and will have no class or char requirements.

		--SK_EVASION,
--		SK_GREATERHEAL,
--		SK_GREATERRECOVER,
	--SK_REBIRTHMYSTICPOWER,
	
	--make main stats recalculate themselves by setting them to something (because main ones have to be recalculated from parts).
	local i=ATTR_COUNT_BASE1
	for i=ATTR_COUNT_BASE1+6,ATTR_COUNT_BASE1+21 do
		stats[i]=0
	end
end


function Check_Baoliao(ATKER, DEFER, ... ) --[[判定是否暴料,传入攻击者等级、受击者等级、攻击者暴料率、受击者暴料率]]--
	--local arg = ...
	--LuaPrint("Enter function Check_Baoliao(Atker,Defer,mf_atker,mf_defer) --[[determine if it is drop item]]--".."\n" ) 
	--LG("Drop List", "Enter function Check_Baoliao(Atker,Defer,mf_atker,mf_defer) --[[determine if it is drop item]]--","\n" ) 
	--Atker = TurnToCha ( ATKER ) 
	--Defer = TurnToCha ( DEFER ) 
	--local lv_atker = Lv(Atker)
	--local lv_defer = Lv(Defer)
	--local count = #arg
	print('Check_Baoliao')
	--[[
	local end_items = {}
	for i, chance in ipairs(arg) do
		print(10000/chance)
		if (math_percent_random(10000/chance)) == true then
			table.insert(end_items,i) --used to be table.insertfix (which adds to .n separately)
		end
	end
	SetItemFall(end_items.n,table.unpack(end_items))
	--]]
	--SetItemFall:
	--1st param: amount of items to drop - maximum is the amount of different items the char is defined to drop (which has a maximum of 10).
	--2-infinite amount of parameters (although max ends up being 10 because of 1st parameter's limit):
	--1 = first item in mob drop index to drop, 2 = second... and so forth.
	--[[
	local itemid = 4602
	local amount = 1
	print('abcghy1')
    GiveItem(DEFER,0,itemid,amount,4)
    --- Drop the item ---
	print('abcghy2')
    RemoveChaItem(DEFER, itemid , amount , 2 , -1, 0 , 1  )
	print('abcghy3')
	--]]
	local _char = char:wrap(DEFER)
	print('printing char:'.._char.name)
	
	local i,v
	for i,v in ipairs(tsv.characterinfo[_char.name]['drop_item_ids']) do
		if math_percent_random(100/tsv.characterinfo[_char.name]['drop_item_rates'][i]) then
			print(i,v,'teststdfts ')
			GiveItem(DEFER,0,v,1,4) --amount is 1
			RemoveChaItem(DEFER, v, 1, 2 , -1, 0 , 1)
		end
	end
end

function Check_SpawnResource ( ATKER, DEFER , lv_skill , count , ...) --called whenever a resource is hit
	--local arg = ...
	--item = {}
	--local count = #arg
	
	print('SpawnResource')
	--make it same as killing regular mobs
	--if char:wrap(DEFER).stats[ATTR_HP] <= 0 then
		Check_Baoliao( ATKER, DEFER , ...)
	--end
end

function AskGuildItem(role,Guild_type)
	return 1
end

function DeductGuildItem(role,Guild_type)
end

function AskJoinGuild(role,guild_type) 					-- 申请加入工会判定   工会类型 0－海军，1－海盗
	local attr_guild = HasGuild(role)  
	if attr_guild ~= 0 then 
		HelpInfo(role,0,"You are already in a guild.")
		return 0 
	end 
	return 1
end

function GetExp_New(dead , atk) -- Gives "atk" the exp returned. "dead" is the player "atk" killed.
	print('GetExp_New')
	local stats = player:wrap(atk).stats
	local statsD = player:wrap(dead).stats
	
	local total_gained_exp = statsD[ATTR_CEXP]
	if stats[ATTR_LV] >= 80 then
		total_gained_exp = total_gained_exp / 50
	end
	if stats[ATTR_LV] == 130 then
		total_gained_exp = 0
	end
	
	--print('blargh',stats[ATTR_LV],total_gained_exp, statsD[ATTR_CEXP], stats[ATTR_CEXP], stats[ATTR_NLEXP], stats[ATTR_CLEXP], (stats[ATTR_NLEXP] - stats[ATTR_CLEXP]))
	
	--(stats[ATTR_NLEXP] > 0) check is important because if setting lvl manually, next level exp might be 0, so this will become an infinite loop
	while (stats[ATTR_NLEXP] > 0) and (total_gained_exp > (stats[ATTR_NLEXP] - stats[ATTR_CLEXP])) and (stats[ATTR_LV] < 130) do
		stats[ATTR_CEXP] = stats[ATTR_CEXP] + (stats[ATTR_NLEXP] - stats[ATTR_CLEXP])
		total_gained_exp = total_gained_exp - (stats[ATTR_NLEXP] - stats[ATTR_CLEXP])
		--stats[ATTR_LV] = stats[ATTR_LV] + 1 --already gains level automatically
		
		if stats[ATTR_LV] == 80 then
			total_gained_exp = total_gained_exp / 50
		end
		if stats[ATTR_LV] == 130 then
			total_gained_exp = 0
		end
	end
	
	local frac = 0
	total_gained_exp, frac = math.modf(total_gained_exp)
	if (frac > 0) and math_percent_random(frac) then
		total_gained_exp = total_gained_exp + 1
	end
	
	stats[ATTR_CEXP] = stats[ATTR_CEXP] + total_gained_exp
	print('GetExp_New2')
	AttrRecheck(atk)
	print('GetExp_New3')
end

function EightyLv_ExpAdd ( cha , expadd )
	print('EightyLv_ExpAdd')
	local stats = player:wrap(cha).stats
	print(expadd)
	stats[ATTR_CEXP] = stats[ATTR_CEXP] + expadd / 50
end

--TODO:
--Check callback functions starting at dofile(GetResPath("script\\calculate\\functions.lua")).

function is_teammate(cha1, cha2) 
    if cha1 == 0 or cha2 == 0 then 
        return 0 
    end 
    if cha1 == cha2 then 
        return 1 
    end
    local ply1 = GetChaPlayer(cha1) 
    local ply2 = GetChaPlayer(cha2) 
    if ply1 ~= 0 and ply2 ~= 0 then 
        if ply1 == ply2 then 
            return 1 
        end 
        local team_id1, team_id2 
        team_id1 = GetChaTeamID(cha1) 
        team_id2 = GetChaTeamID(cha2) 
        if team_id1 ~= 0 and team_id2 ~= 0 and team_id1 == team_id2 then 
            return 1 
        end 
    end 

    return 0 
end 

function is_friend(pDrole, prole) --all skills seem to obey this
	--Map type is actually implemented here (that hard-coded function actually can be applied!)
	--GetChaMapType( cha1 )
	--TODO: Code party/guild PKs.
	if (pDrole == prole) then --makes char itself friendly to itself - seems dumb.
		return 1
	end
	return 0
--[[
--	SystemNotice ( cha1, "transferis_friend" ) 
    local friend_target = 1 
    local Map_type = GetChaMapType( cha1 )
    if CheckChaRole( cha1 ) == 0 and Map_type ~= 2 then			--如果cha1为怪物且不在工会地图中，则cha2为怪物为友方，cha2为人为敌方
	if CheckChaRole( cha2 ) == 0 then							
		return 1 
	else 
		return 0 
	end 

    end
	   
--		SystemNotice ( cha1 , "1" ) 
--		SystemNotice ( cha1 , "Map_type ="..Map_type ) 
--	    if Map_type == 4 then									--迷宫PK地图友方判断
--		local team = is_teammate(cha1, cha2) 
--		if team == 1 then 
--			return 1 
--		end 
--	    else
--		return 0
--	    end
		if Map_type == 1 then 
			if CheckChaRole ( cha1 ) == 1 then 
					if CheckChaRole( cha2 ) == 0 then							
						return 0 
					else 
						return 1 
					end 

			end 
		end 

		if Map_type == 4 then									--迷宫PK地图友方判断
			if Is_NormalMonster (cha1) == 1 then						--如果是普通怪物则与怪物是友方
				if Is_NormalMonster (cha2) == 1 then 
					return 1 
				end 
			end 
			local team = is_teammate(cha1,cha2) 
			if team == 1 then 
				return 1 
			else 
				local guild_id1, guild_id2 
				guild_id1 = get_cha_guild_id(cha1) 
				guild_id2 = get_cha_guild_id(cha2) 
				if guild_id1 ~= 0 and guild_id2 ~= 0 and guild_id1 == guild_id2 then 
					return 1  
				else
					return 0 
				end
			end
			--local team = is_teammate(cha1,cha2) 
			--		if team == 1 then 
			--			return 1 
			--		else 
			--			return 0 
			--		end 
		end 

--	        SystemNotice ( cha1 , "2" ) 

	    if  Map_type ==3 then									--是队伍战地图则队友为友方
--		SystemNotice ( cha1 , "1" ) 
		    local team = is_teammate(cha1, cha2) 
--		    		SystemNotice ( cha1 , "2" ) 

		    if team == 1 then 
--		    		SystemNotice ( cha1 , "3" ) 
		        return 1 
		    else 
			return 0 
		    end
		   
	    end

	    if cha1 == 0 or cha2 == 0 then							--指针为空
	        return 0 
	    end 
--		SystemNotice ( cha1 , "3" ) 
	    if Map_type == 2 then									--工会PK地图判断
		if Is_NormalMonster (cha1) == 1 then						--如果是普通怪物则与怪物是友方
			if Is_NormalMonster (cha2) == 1then 
				return 1 
			end 
		end 
		local guild_id1, guild_id2 
		guild_id1 = get_cha_guild_id(cha1) 
		guild_id2 = get_cha_guild_id(cha2) 
		if guild_id1 ~= 0 and guild_id2 ~= 0 and guild_id1 == guild_id2 then 
			return 1 
		else 
			return 0 
		end

	    end 
	    if Map_type == 5 then									--工会PK地图判断
--		if Is_NormalMonster (cha1) == 1 then						--如果是普通怪物则与怪物是友方
--			if Is_NormalMonster (cha2) == 1then 
--				return 1 
--			end 
--		end 
		local guild_side_1, guild_side_2 
		guild_side_1 = GetChaSideID(cha1) 
		guild_side_2 = GetChaSideID(cha2) 
		--if guild_side_1 <= 100 and guild_side_1 > 0 and guild_side_2 <= 100 and guild_side_2 > 0 then
		if guild_side_1 == guild_side_2 then
			return 1 
		--elseif
		   --guild_side_1 > 100 and guild_side_1 <= 200 and guild_side_2 > 100 and guild_side_2 <= 200 then
			--return 1
	        else
		        return 0
		end

	    end 
--		SystemNotice ( cha1 , "4" ) 

	    return friend_target 
--]]
 end

--各种实体类型信息
BASE_ENTITY			= 0		--基本实体
RESOURCE_ENTITY	= 1		--资源实体
TRANSIT_ENTITY		= 2		--传送实体
BERTH_ENTITY		= 3		--停泊实体

--创建事件实体
function CreateBerthEntity( name, cid, infoid, xpos1, ypos1, dir1, berth, xpos2, ypos2, dir2 )
	local ret, submap = GetCurSubmap()
	if ret ~= LUA_TRUE then
		return
	end
	local ret, e = CreateEventEntity( BERTH_ENTITY, submap, name, cid, infoid, xpos1, ypos1, dir1 )
	if ret ~= LUA_TRUE then
		return
	end
	ret = SetEntityData( e, berth, xpos2, ypos2, dir2 )
	if ret ~= LUA_TRUE then
		return
	end
end

--创建资源实体
function CreateResourceEntity( name, cid, infoid, xpos, ypos, dir, itemid, count, time )
	local ret, submap = GetCurSubmap()
	if ret ~= LUA_TRUE then
		return
	end
	local ret, e = CreateEventEntity( RESOURCE_ENTITY, submap, name, cid, infoid, xpos, ypos, dir )
	if ret ~= LUA_TRUE then
		return
	end
	ret = SetEntityData( e, itemid, count, time )
	if ret ~= LUA_TRUE then
		return
	end
end

function ShipAttrRecheck ( cha_role , ship_role )
	print('ShipAttrRecheck')
end

function Lifelv_Up ( cha_role )
	print('Lifelv_Up')
end

function Saillv_Up ( cha_role )
	print('Saillv_Up')
end

function BoatLevelUp( character, boat, levelup )
	print('BoatLevelUp')
	return C_TRUE
end

function Ship_Tran (  buyer , boat ) 
	print('Ship_Tran')
end 

function Ship_ExAttrSet ( cha_role , ship_role )									--船只属性设置
	print('Ship_ExAttrSet')
end

function WGPrizeBegin( role , rightCount)
	print('WGPrizeBegin')
end

function Ship_ExAttrCheck (player_role, ship_role )
	local stats = player:wrap(role).stats
	print('Ship_ExAttrCheck')

	SetCharaAttr(stats[ATTR_JOB], ship_role, ATTR_JOB )											--[[赋值船只职业]]--
	SetCharaAttr(stats[ATTR_SPR], ship_role, ATTR_SPR )											--[[赋值船只精神]]--
	--TODO: Set up ship stats
	SetCharaAttr(1000, ship_role, ATTR_ATK )									--[[赋值最终mnatk]]--
	SetCharaAttr(1000, ship_role, ATTR_MATK )									--[[赋值最终mxatk]]--
	SetCharaAttr(1000, ship_role, ATTR_CRTR )									--[[赋值最终adis]]--
	SetCharaAttr(1000, ship_role, ATTR_BOAT_CSPD )								--[[赋值最终船只炮弹飞行速度]]--
	SetCharaAttr(1000, ship_role, ATTR_ASPD )									--[[赋值最终船只攻击速度]]--
	SetCharaAttr(1000, ship_role, ATTR_BOAT_CRANGE )								--[[赋值最终船只炮弹爆炸范围]]--
	SetCharaAttr(1000, ship_role, ATTR_DEF )										--[[赋值最终船只防御]]--
	SetCharaAttr(1000, ship_role, ATTR_MDEF )									--[[赋值最终船只抵抗]]--
	SetCharaAttr(1000, ship_role, ATTR_MXHP )									--[[赋值最终船只最大耐久]]--
	SetCharaAttr(1000, ship_role, ATTR_HREC )									--[[赋值最终船只耐久回复速度]]--
	SetCharaAttr(1000, ship_role, ATTR_SREC )									--[[赋值最终船只补给消耗速度]]--
	SetCharaAttr(1000, ship_role, ATTR_MSPD )									--[[赋值最终船只移动速度]]--
	SetCharaAttr(1000, ship_role, ATTR_MXSP )									--[[赋值最终船只最大补给值]]--
	SetCharaAttr(1000, ship_role , ATTR_FLEE )	
end

BerthPortList = {
	"Argent Harbor",
	"Thundoria Harbor",
	"Laboratory Harbor",
	"Icicle Harbor",
	"Zephyr Harbor",
	"Glacier Harbor",
	"Outlaw Harbor",
	"Harbor of Chill",
	"Canary Harbor",
	"Cupid Harbor",
	"Harbor of Fortune",
	"Mystery Harbor",
	"Spring Harbor",
	"Summer Southern Harbor",
	"Pirate Cove Harbor"
}
BerthPortList[34] = "Phantom Harbor"

function AddBerthPort( id, name )
	BerthPortList[id] = {}
	BerthPortList[id].name = name
end

function GetBerthData( id )
	if id == nil or BerthPortList[id] == nil then
		return "Harbor "..id
	end
	return BerthPortList[id]
end

function CheckExpShare ( ti , atk )
	print('CheckExpShare')
	return 1 
end

function PackBagGoods( character, boat, tp, level )
	print('PackBagGoods')
	return C_TRUE
end

function Resume ( role )
	print('Resume')
end