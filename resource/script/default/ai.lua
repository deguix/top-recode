function ai_is_looking_for_target(char)
	--is_chase
	if char.type_id==350 then return true end --turrets always look for targets

	AI_AI_SDK_LUA_000004 = GetResString("AI_AI_SDK_LUA_000004")
	LG("lua_ai", AI_AI_SDK_LUA_000004, char.chase_range)
	if char.chase_range==0 then --if chase_range is 0, it will keep looking for it?
		return true
	end
	
	local starting_position = char.pos
	if char.host ~= nil then
		starting_position = char.host.pos
	end
	
	local dis = point_distance(starting_position, char.pos)
	AI_AI_SDK_LUA_000005 = GetResString("AI_AI_SDK_LUA_000005")
	LG("lua_ai", AI_AI_SDK_LUA_000005, dis)
	if dis > char.chase_range then
		if char.patrol_pos.x~=0 then
			if char:is_position_nearby(char.patrol_pos, char.chase_range) then
				return true
			end
		end
		return false
	end
	return true
end

CHANGE_TARGET_RATIO = 50
function ai_changed_target(char)
	--ai_update_target
    if char.ai<=AI_N_ATK or char.target==char.host then
       char.target = 0
	   char.patrol_state = 0
       return true
    end
	
    if Rand(100) < CHANGE_TARGET_RATIO then
    	local tNew = char.first_target
      	if tNew~=nil and tNew~=char.target then
			char.target = tNew
			char.patrol_state = 0
            return true
    	end
	end
    
    if not char:is_target_nearby(char.target, char.vision) then
		char.target = 0
		char.patrol_state = 0
		return true
    end
    
    if not ai_is_looking_for_target(char) then
		char.target = 0
		char:move_to_sleep(char.spawn_pos)
		char.patrol_state = AI_PATROL_MOVING_BACK
        return true
    end

    return false

end

function ai_target(char)
	--ai_target
	if char.ai == AI_MOVETOHOST then
		if ai_changed_target(char) then
			return
		end
	end

	if char.ai == AI_FLEE then
		char:flee(char.target)
		return
	elseif char.ai == AI_MOVETOHOST then
		--star_move_to
		return
	elseif char.ai == AI_ATK_FLEE then
		if char:is_target_nearby(char.target, 400) then
			char:flee(char.target)
			return
		end
	end

	if char.ai > 20 then -- bosses special ai
		--special_ai(c, t,ai_type)
	else
		char:use_skill(target_char, select_skill(char))
	end

	--if (char.patrol_state < 9) and (ai_flag_summon[GetChaTypeID(c1)] ~= nil) then --can summon
	--	summon_monster(c, t)         --召集其他同类怪来攻击目标
	--end
end

function ai_loop(role)
	local char = char:wrap(role)
	if char.ai~=AI_NONE then
		if char.target~=nil then
			ai_target(char)
		else
			--ai_no_target
			if char.ai==AI_MOVETOHOST then
				char:life_time(1)
				return
			end
			if char:is_position_nearby(char.spawn_pos, 100) then
				char.patrol_state = 0
			end
			
			if not char:seek_target() then --ai_seek_target
				if char.patrol_state~=AI_PATROL_MOVING_BACK then
					if point_distance(char.spawn_pos, char.pos) > 5000 then
						char:move(char.spawn_pos)
						--AI_AI_LUA_000001 = GetResString("AI_AI_LUA_000001")
						--debugout(char.role, AI_AI_LUA_000001)
					else
						--ai_idle
						--ai_host
						if char.host ~= nil then
							if point_distance(char.pos, char.host.pos) > 400 then
								char:move(char.host.pos.x + 200 - Rand(400), char.host.pos.y + 200 - Rand(400))
							end
						else
							--ai_idle (after the ai_host check)
							if char.ai == AI_32 then
								char.stats[ATTR_MXHP] = math.floor(char.stats[ATTR_MXHP])
								char.stats:sync(ATTR_MXHP)
							end
							if char.patrol_pos.x==0 then
								char:move_randomly_within_spawn_range(600)
							else
								if char.patrol_state==AI_PATROL_FORWARDS then
									char:move(char.patrol_pos)
									char.patrol_state = AI_PATROL_FORWARDS_END
								elseif char.patrol_state==AI_PATROL_FORWARDS_END then
									if char:is_position_nearby(char.patrol_pos, 40) then
										char.patrol_state = AI_PATROL_BACKWARDS
									end
								elseif char.patrol_state==AI_PATROL_BACKWARDS then
									char:move(char.spawn_pos)
									char.patrol_state = AI_PATROL_BACKWARDS_END
								elseif char.patrol_state==AI_PATROL_BACKWARDS_END then
									if char:is_position_nearby(char.spawn_pos, 40) then
										char.patrol_state = AI_PATROL_FORWARDS
									end
								end
							end
						end
					end
				else
					if Rand(2)==1 then
						char:move(char.spawn_pos)
						--AI_AI_LUA_000002 = GetResString("AI_AI_LUA_000002")
						--debugout(char.role, AI_AI_LUA_000002)
					end
				end
			end
			
			--TODO: More stuff
		end
		
		--ai_tick
		--if ai_flag_nohide[char.type_id]==1 then
		--	char:reveal_players_on_sight()
		--end
		
		--ai_block
		if char.blocked_moves > 0 then
			if char.target ~= nil and Rand(10) > 5 then
			   char:flee(char.target)
			else
			   char:move_randomly_within_range(400)
			end
			
			char.blocked_moves = 0		
		end
	end
end