--TODO: Adapt everything to use char instead of player for anything that isn't a player.
player = {}

function player.__index(self, key)
	return char.__index(self, key)
end

function player.__newindex(self, key, value)
	return char.__newindex(self, key, value)
	--player.interacting_with_npc -- for interactions with npc
end

function player:wrap(role, o)
	o = o or char:wrap(role)
	if o ~= nil then
		setmetatable(o, self)
		self.id = -1
		
		--self.__index = self
		--self.__newindex = self
		return o
	end
end

players = {} --will contain "player" tables

function players:next_available_id()
	--[[
	Regular ID's: Player ID is always the same one after he joins. If he leaves, the ID is freed for the next player that joins.
	Used for everything basically.
	--]]
	local i = 0
	repeat
		i = i + 1
	until (self[i] ~= nil)
	return i
end

function player:items_report()
	local _bag={}
	local _equip={}
	for i=0,GetKbCap(self.role)-1 do
		_bag[i] = GetChaItem(self.role,2,i)
	end
	for i=0, 9 do --max equippable items = 10 (id 1 could be ammo, which is non-existent) TODO: Set to top 2 standards?
		_equip[i] = GetChaItem(self.role,1,i)
	end
	return {bag=_bag,equip=_equip}
end

--TODO: Implement player functions related to quests.