attr = {}
attr[1] = {0,0} --���Ըı��б�, ���ı�10��
attr[2] ={0,0} 
attr[3] ={0,0} 
attr[4] ={0,0} 
attr[5] ={0,0}  
attr[6] ={0,0} 
attr[7] ={0,0} 
attr[8] ={0,0} 
attr[9] ={0,0} 
attr[10]={0,0} 

-- TODO (iteminfo edit):
-- Add bullets/arrows to a ring slot - thus remove 1 ring slot for it.
-- Crusaders/Champs/Clerics/SMs will have 1 item here with a stat for the based class.

-- TODO, in order:
-- Stat changes. Done.
-- Adapt old code to new, otherwise rewrite the whole thing.

-- Attribute changes:
-- No own stat increase. Buy stat increasing items (with the change below).
-- LUK added (check LUK stat increasing items).
-- Min/Max Attack merged into simply Attack (not many stat slots are available).
-- Max Attack slot replaced with Magic Attack (many changes are applied to magic attacks thus they are comparable).
-- Replaced Physical Resist with Magical Defense (physical resist is too strong vs melee).
-- Replaced Treasure Rate with real Drop Rate (useless stat).
-- Replaced Life Treasure Drop Rate with Experience Rate (useless stat).
-- Replaced Attack Distance with Critical Resist Rate (game doesn't allow custom attack distances).

-- Maybe by default, best stats should be increased. Then the user should be able to reset them.

-- TODO:
-- Gems don't add glows, what adds glows is the elemental stance or weapon mode...

-- NEW PLAN:
-- Remove classes from attributes menu. Instead, necklaces identify the class each character belongs. That means, whenever changing
-- necks, the person changes class (which includes skills, exp, lvl, stats).

-- Make each "class" reachable by certain actions - Main classes:
-- Berserker (STR): Attacks boss from near with a weapon that is too heavy for him - thus damaging him per second (2h sword).
-- Champion (CON): After luring the boss, attacks using sword and a shield.
-- Assassin (AGI): Attacks with light weapons behind the boss (like dagger).
-- Cleric (SPR): Heals those who are hurt with a healing staff.
-- Hunter (ACC): Attacks from very far with bows.
-- Voyager (LUK): Places buffs in allies for them to perform better in battle. Uses lucky charms only (seen as fists only).

-- Special hybrid classes:
-- (STR, CON): Attacks with 2h and shield only. Great attack and defense, but too much weight which hurts.
-- Crusader (STR, AGI): Attacks using dual-swords. Less damaging than 2h sword (even when using 2 swords), but can lightly stun.
-- Oracle (STR, SPR): Attacks using fists only. Initially they hurt and low dmg, but they rapidly master non-hurtful techniques.
-- (STR, ACC): Using a gun and a sword, provides greater accuracy when close.
-- (STR, LUK): Uses a 2h sword with lucky charms to provide the greatest attack, likewise with the damage on himself.

-- (CON, STR): Attacks with fists and shield.
-- (CON, AGI): Attacks with dagger and shield.
-- (CON, SPR): Attacks with damaging staff and shield.
-- (CON, ACC): Attacks with gun and shield.
-- (CON, LUK): Attacks with lucky charms and shield.

-- (AGI, STR): Attacks with dual daggers.
-- (AGI, CON): Attacks with dagger and shield.
-- (AGI, SPR): Attacks with dagger and a damaging staff.
-- (AGI, ACC): Attacks with dagger and gun.
-- (AGI, LUK): Attacks with dagger and lucky charm.

-- Mage (SPR, STR): Attacks with dual damaging staff. Attacks will also hurt the caster.
-- (SPR, CON): Attacks with healing staff and shield.
-- (SPR, AGI): Attacks with dagger and a healing staff.
-- (SPR, ACC): Attacks with a single damaging staff.
-- Angel (SPR, LUK): Combines healing staff with lucky charms, so that healing will happen unpredictably after cast.

-- Sharpshooter (ACC, STR): Attacks boss from far with a gun. They are less precise, but can be dual-wielded, so more powerful.
-- (ACC, CON): Attacks with bow and shield.
-- (ACC, AGI): Attacks with bow and dagger.
-- (ACC, SPR): Attacks with bow and healing staff.
-- Sniper (ACC, LUK): Attacks with bow and lucky charms. Causes criticals on head shots. Non-crits make way less damage than normal.

-- (LUK, STR): Attacks with dual lucky charms.
-- (LUK, CON): Attacks with lucky charms and shield.
-- (LUK, AGI): Attacks with lucky charms and dagger.
-- (LUK, SPR): Attacks with lucky charms and damaging staff.
-- (LUK, ACC): Attacks with lucky charms and gun.

--[[
-- Class constants
CLASS_NEWBIE		= 0;	-- NO STAT

CLASS_WARRIOR		= 1;	-- STR/Attack/Aspd|Def -- 1
CLASS_HUNTER		= 2;	-- ACC/Hit/Attack|Aspd -- 2
CLASS_THIEF			= 16;	-- AGI/Aspd/Hit|Attack -- 16
CLASS_KNIGHT		= 10;	-- CON/Def/Crit|MAttack -- 10
CLASS_HERBALIST		= 5;	-- SPR/MAttack/Def|Crit -- 5
CLASS_EXPLORER		= 4;	-- LUK/Crit/MAttack|Hit -- 4
CLASS_ARTISAN		= 6;	-- -- 6
CLASS_MERCHANT		= 7;	-- -- 7
CLASS_SAILOR		= 3;	-- -- 3

CLASS_BERSERKER		= 17;	-- STR+STR -- 17 -- Sacrifices himself for his own beliefs.
CLASS_SHARPSHOOTER	= 12;	-- ACC+ACC -- 12 -- Punishes while revealing what others are.
CLASS_ASSASSIN		= 9;	-- AGI+AGI -- 9 -- Stealthly uses enemies' weaknesses to his advantage.
CLASS_CHAMPION		= 8;	-- CON+CON -- 8 -- Carries heavy armor to uphold his strong belief of justice.
CLASS_CLERIC		= 13;	-- SPR+SPR -- 13 -- Always finds ways to make good happen to allies.
CLASS_VOYAGER		= 16;	-- LUK+LUK -- 16 -- Uses trinkets he finds in the world to bend luck his way.
CLASS_BLACKSMITH	= 18;	-- -- 18 -- Makes weapons for all other classes.
CLASS_TAILOR		= 11;	-- -- 11 -- Creates clothing for all other classes.
CLASS_CAPTAIN		= 15;	-- -- 15 -- Creates ships for other classes.

CLASSES_1STCLASS		= {CLASS_WARRIOR,CLASS_HUNTER,CLASS_THIEF,CLASS_KNIGHT,CLASS_HERBALIST,CLASS_EXPLORER,CLASS_ARTISAN,CLASS_MERCHANT,CLASS_SAILOR}
CLASSES_2NDCLASS		= {CLASS_BERSERKER,CLASS_SHARPSHOOTER,CLASS_ASSASSIN,CLASS_CHAMPION,CLASS_CLERIC,CLASS_VOYAGER,CLASS_BLACKSMITH,CLASS_TAILOR,CLASS_CAPTAIN}

CLASS_RATIO_STR_LVL		= 0
CLASS_RATIO_ACC_LVL		= 1
CLASS_RATIO_AGI_LVL		= 2
CLASS_RATIO_CON_LVL		= 3
CLASS_RATIO_SPR_LVL		= 4
CLASS_RATIO_LUK_LVL		= 5
CLASS_RATIO_HP_CON		= 6
CLASS_RATIO_HP_LVL 		= 7
CLASS_RATIO_SP_SPR 		= 8
CLASS_RATIO_SP_LVL		= 9
CLASS_RATIO_ATK_STR		= 10
CLASS_RATIO_ATK_ACC		= 11
CLASS_RATIO_ATK_LUK		= 12
CLASS_RATIO_MATK_SPR	= 13
CLASS_RATIO_MATK_LUK	= 14
CLASS_RATIO_DEF_CON		= 15
CLASS_RATIO_MDEF_LUK	= 16
CLASS_RATIO_HIT_ACC		= 17
CLASS_RATIO_HIT_LUK		= 18
CLASS_RATIO_FLEE_AGI	= 19
CLASS_RATIO_FLEE_LUK	= 20
CLASS_RATIO_DROP_LUK	= 21
CLASS_RATIO_CRT_LUK		= 22
CLASS_RATIO_CRTR_CON	= 23
CLASS_RATIO_CRTR_SPR	= 24
CLASS_RATIO_HREC_BMXHP	= 25
CLASS_RATIO_HREC_CON	= 26
CLASS_RATIO_SREC_BMXSP	= 27
CLASS_RATIO_SREC_SPR	= 28
CLASS_RATIO_ASPD_AGI	= 29
CLASS_RATIO_MSPD_AGI	= 30

--									STATS					HP		SP			ATK						MATK			DEF		MDEF	HIT				FLEE			DROP	CRT		CRTR			HREC			SREC			APSD	MSPD
class_sr = {}
class_sr[CLASS_NEWBIE]={			0,	0,	0,	0,	0,	0,	1,	15,	0.1,	3,	0.3,	0.3,	0.1,	0.3,	0.1,	0.10,	0.10,	0.5,	0.1,	0.5,	0.1,	0.20,	0.10,	0.10,	0.15,	0.005,	0.125,	0.005,	0.050,	0.9,	0.20}

class_sr[CLASS_WARRIOR]={			1,	0,	0,	0,	0,	0,	2,	20,	0.2,	3,	0.7,	0.0,	0.2,	0.0,	0.0,	0.14,	0.14,	0.6,	0.1,	0.6,	0.1,	0.30,	0.20,	0.20,	0.0,	0.010,	0.250,	0.010,	0.080,	1.0,	0.20}
class_sr[CLASS_HUNTER]={			0,	1,	0,	0,	0,	0,	3,	25,	0.2,	3,	0.0,	0.5,	0.2,	0.0,	0.0,	0.14,	0.14,	0.8,	0.1,	0.6,	0.1,	0.30,	0.20,	0.20,	0.0,	0.006,	0.150,	0.010,	0.080,	1.1,	0.21}
class_sr[CLASS_THIEF]={				0,	0,	1,	0,	0,	0,	3,	25,	0.2,	3,	0.0,	0.4,	0.2,	0.0,	0.0,	0.14,	0.14,	0.6,	0.1,	0.8,	0.1,	0.30,	0.20,	0.20,	0.0,	0.006,	0.150,	0.010,	0.080,	1.2,	0.22}
class_sr[CLASS_KNIGHT]={			0,	0,	0,	1,	0,	0,	5,	35,	0.2,	3,	0.5,	0.0,	0.2,	0.0,	0.0,	0.16,	0.13,	0.6,	0.1,	0.6,	0.1,	0.30,	0.20,	0.30,	0.0,	0.006,	0.150,	0.010,	0.080,	1.0,	0.20}
class_sr[CLASS_HERBALIST]={			0,	0,	0,	0,	1,	0,	3,	25,	1.0,	5,	0.0,	0.0,	0.0,	0.6,	0.2,	0.13,	0.16,	0.6,	0.1,	0.6,	0.1,	0.30,	0.20,	0.0,	0.25,	0.006,	0.150,	0.017,	0.100,	1.0,	0.20}
class_sr[CLASS_EXPLORER]={			0,	0,	0,	0,	0,	1,	3,	25,	0.2,	3,	0.0,	0.0,	0.0,	0.2,	0.4,	0.14,	0.14,	0.6,	0.1,	0.6,	0.1,	0.60,	0.30,	0.0,	0.20,	0.006,	0.150,	0.010,	0.080,	1.0,	0.20}
class_sr[CLASS_ARTISAN]={			0,	0,	0,	0,	0,	0,	3,	25,	0.2,	3,	0.3,	0.3,	0.1,	0.3,	0.1,	0.14,	0.14,	0.6,	0.1,	0.6,	0.1,	0.30,	0.20,	0.20,	0.20,	0.006,	0.150,	0.010,	0.080,	1.0,	0.20}
class_sr[CLASS_MERCHANT]={			0,	0,	0,	0,	0,	0,	3,	25,	0.2,	3,	0.3,	0.3,	0.1,	0.3,	0.1,	0.14,	0.14,	0.6,	0.1,	0.6,	0.1,	0.30,	0.20,	0.20,	0.20,	0.006,	0.150,	0.010,	0.080,	1.0,	0.20}
class_sr[CLASS_SAILOR]={			0,	0,	0,	0,	0,	0,	3,	25,	0.2,	3,	0.3,	0.3,	0.1,	0.3,	0.1,	0.14,	0.14,	0.6,	0.1,	0.6,	0.1,	0.30,	0.20,	0.20,	0.20,	0.006,	0.150,	0.010,	0.080,	1.0,	0.20}

class_sr[CLASS_BERSERKER]={			2,	0,	0,	0,	0,	0,	3,	25,	0.5,	5,	1.0,	0.0,	0.3,	0.0,	0.0,	0.18,	0.18,	0.7,	0.1,	0.7,	0.1,	0.45,	0.40,	0.40,	0.0,	0.015,	0.375,	0.015,	0.100,	1.1,	0.20}
class_sr[CLASS_SHARPSHOOTER]={		0,	2,	0,	0,	0,	0,	5,	35,	0.5,	5,	0.0,	0.8,	0.3,	0.0,	0.0,	0.18,	0.18,	1.0,	0.1,	0.7,	0.1,	0.45,	0.40,	0.40,	0.0,	0.007,	0.175,	0.015,	0.100,	1.3,	0.23}
class_sr[CLASS_ASSASSIN]={			0,	0,	2,	0,	0,	0,	5,	35,	0.5,	5,	0.0,	0.7,	0.3,	0.0,	0.0,	0.14,	0.14,	0.7,	0.1,	1.0,	0.1,	0.45,	0.40,	0.40,	0.0,	0.007,	0.175,	0.015,	0.100,	1.4,	0.25}
class_sr[CLASS_CHAMPION]={			0,	0,	0,	2,	0,	0,	7,	50,	0.5,	5,	0.8,	0.0,	0.3,	0.0,	0.0,	0.20,	0.16,	0.7,	0.1,	0.7,	0.1,	0.45,	0.40,	0.40,	0.0,	0.007,	0.175,	0.015,	0.100,	1.1,	0.20}
class_sr[CLASS_CLERIC]={			0,	0,	0,	0,	2,	0,	5,	35,	1.5,	7,	0.0,	0.0,	0.0,	0.9,	0.3,	0.16,	0.20,	0.7,	0.1,	0.7,	0.1,	0.45,	0.40,	0.0,	0.50,	0.007,	0.175,	0.025,	0.120,	1.1,	0.20}
class_sr[CLASS_VOYAGER]={			0,	0,	0,	0,	0,	2,	5,	35,	0.5,	5,	0.0,	0.0,	0.0,	0.3,	0.8,	0.18,	0.18,	0.7,	0.1,	0.7,	0.1,	0.80,	0.55,	0.0,	0.40,	0.007,	0.175,	0.015,	0.100,	1.1,	0.20}
class_sr[CLASS_BLACKSMITH]={		0,	0,	0,	0,	0,	0,	5,	35,	0.5,	5,	0.8,	0.8,	0.2,	0.8,	0.2,	0.18,	0.18,	0.7,	0.1,	0.7,	0.1,	0.45,	0.40,	0.40,	0.40,	0.007,	0.175,	0.015,	0.100,	1.1,	0.20}
class_sr[CLASS_TAILOR]={			0,	0,	0,	0,	0,	0,	5,	35,	0.5,	5,	0.8,	0.8,	0.2,	0.8,	0.2,	0.18,	0.18,	0.7,	0.1,	0.7,	0.1,	0.45,	0.40,	0.40,	0.40,	0.007,	0.175,	0.015,	0.100,	1.1,	0.20}
class_sr[CLASS_CAPTAIN]={			0,	0,	0,	0,	0,	0,	5,	35,	0.5,	5,	0.8,	0.8,	0.2,	0.8,	0.2,	0.18,	0.18,	0.7,	0.1,	0.7,	0.1,	0.45,	0.40,	0.40,	0.40,	0.007,	0.175,	0.015,	0.100,	1.1,	0.20}
--]]

-- Attribute constants
ATTR_COUNT_BASE0    = 0;
ATTR_LV             = ATTR_COUNT_BASE0 + 0; -- ��ɫ�ȼ�
ATTR_HP             = ATTR_COUNT_BASE0 + 1; -- ��ǰHP�����ڴ�ֻ����ʾ�;ã�
ATTR_SP             = ATTR_COUNT_BASE0 + 2; -- ��ǰSP�����ڴ�ֻ����ʾ����
ATTR_TITLE          = ATTR_COUNT_BASE0 + 3; -- ��ɫ�ƺ�
ATTR_CLASS          = ATTR_COUNT_BASE0 + 4; -- ��ɫְҵ
ATTR_FAME           = ATTR_COUNT_BASE0 + 5; -- ����
ATTR_AP             = ATTR_COUNT_BASE0 + 6; -- ���Ե�
ATTR_TP             = ATTR_COUNT_BASE0 + 7; -- ���ܵ�
ATTR_GD             = ATTR_COUNT_BASE0 + 8; -- ��Ǯ
ATTR_SPRI           = ATTR_COUNT_BASE0 + 9; -- �ڵ������ٶ�
ATTR_CHATYPE        = ATTR_COUNT_BASE0 + 10; -- ��ɫ�������ͣ���ң�NPC������ȣ�
ATTR_SAILLV         = ATTR_COUNT_BASE0 + 11; -- �����ȼ�
ATTR_LIFELV         = ATTR_COUNT_BASE0 + 12; -- ���ȼ�
ATTR_LIFETP         = ATTR_COUNT_BASE0 + 13; -- ���ܵ�
-- ���·�Χ�ڵĸ����Ե�˳�򲻿ɸı�
ATTR_CEXP           = ATTR_COUNT_BASE0 + 15; -- ��ǰ���飨���ڴ�ֻ����ʾ�����ȣ�
ATTR_NLEXP          = ATTR_COUNT_BASE0 + 16; -- ��һ�����辭��
ATTR_CLEXP          = ATTR_COUNT_BASE0 + 17; -- ��ǰ�ȼ��ľ���
ATTR_CLEFT_SAILEXP  = ATTR_COUNT_BASE0 + 18; -- ��ǰʣ�ຽ������
ATTR_CSAILEXP       = ATTR_COUNT_BASE0 + 19; -- �ۻ���飨��ǰ�������飩
ATTR_CLV_SAILEXP    = ATTR_COUNT_BASE0 + 20; -- ��ǰ�ȼ���������
ATTR_NLV_SAILEXP    = ATTR_COUNT_BASE0 + 21; -- ��һ�ȼ��ȼ���������
ATTR_CLIFEEXP       = ATTR_COUNT_BASE0 + 22; -- ��ǰ����
ATTR_CLV_LIFEEXP    = ATTR_COUNT_BASE0 + 23; -- ��ǰ�ȼ�����
ATTR_NLV_LIFEEXP    = ATTR_COUNT_BASE0 + 24; -- ��һ�ȼ�����
--

ATTR_COUNT_BASE1    = 25;
ATTR_STR            = ATTR_COUNT_BASE1 + 0; -- ����
ATTR_ACC            = ATTR_COUNT_BASE1 + 1; -- רע
ATTR_AGI            = ATTR_COUNT_BASE1 + 2; -- ����
ATTR_CON            = ATTR_COUNT_BASE1 + 3; -- ����
ATTR_SPR            = ATTR_COUNT_BASE1 + 4; -- ����
ATTR_LUK            = ATTR_COUNT_BASE1 + 5; -- ����
ATTR_MXHP           = ATTR_COUNT_BASE1 + 6; -- ���HP
ATTR_MXSP           = ATTR_COUNT_BASE1 + 7; -- ���SP
ATTR_ATK            = ATTR_COUNT_BASE1 + 8; -- ��С������
ATTR_MATK           = ATTR_COUNT_BASE1 + 9; -- ��󹥻���
ATTR_DEF            = ATTR_COUNT_BASE1 + 10; -- ������
ATTR_HIT            = ATTR_COUNT_BASE1 + 11; -- ������
ATTR_FLEE           = ATTR_COUNT_BASE1 + 12; -- ������
ATTR_DROP           = ATTR_COUNT_BASE1 + 13; -- Ѱ����
ATTR_CRT            = ATTR_COUNT_BASE1 + 14; -- ������
ATTR_HREC           = ATTR_COUNT_BASE1 + 15; -- hp�ָ��ٶ�
ATTR_SREC           = ATTR_COUNT_BASE1 + 16; -- sp�ָ��ٶ�
ATTR_ASPD           = ATTR_COUNT_BASE1 + 17; -- �������
ATTR_CRTR           = ATTR_COUNT_BASE1 + 18; -- ��������
ATTR_MSPD           = ATTR_COUNT_BASE1 + 19; -- �ƶ��ٶ�
ATTR_EXP            = ATTR_COUNT_BASE1 + 20; -- ��Դ�ɼ��ٶ�
ATTR_MDEF           = ATTR_COUNT_BASE1 + 21; -- ����ֿ�
ATTR_BOAT_CRANGE    = ATTR_COUNT_BASE1 + 22; -- vessels of the shells exploded 
ATTR_BOAT_CSPD      = ATTR_COUNT_BASE1 + 23; -- artillery shells flying speed vessels
ATTR_BOAT_PRICE     = ATTR_COUNT_BASE1 + 24; -- the value of vessels 

ATTR_COUNT_BASE2    = 50;
ATTR_BSTR           = ATTR_COUNT_BASE2 + 0; -- ������
ATTR_BACC           = ATTR_COUNT_BASE2 + 1; -- ��רע
ATTR_BAGI           = ATTR_COUNT_BASE2 + 2; -- ������
ATTR_BCON           = ATTR_COUNT_BASE2 + 3; -- ������
ATTR_BSPR           = ATTR_COUNT_BASE2 + 4; -- ����
ATTR_BLUK           = ATTR_COUNT_BASE2 + 5; -- ������
ATTR_BMXHP          = ATTR_COUNT_BASE2 + 6; -- �����HP
ATTR_BMXSP          = ATTR_COUNT_BASE2 + 7; -- �����SP
ATTR_BATK           = ATTR_COUNT_BASE2 + 8; -- ����С������
ATTR_BMATK          = ATTR_COUNT_BASE2 + 9; -- ����󹥻���
ATTR_BDEF           = ATTR_COUNT_BASE2 + 10; -- �������
ATTR_BHIT           = ATTR_COUNT_BASE2 + 11; -- ��������
ATTR_BFLEE          = ATTR_COUNT_BASE2 + 12; -- ��������
ATTR_BDROP          = ATTR_COUNT_BASE2 + 13; -- ��Ѱ����
ATTR_BCRT           = ATTR_COUNT_BASE2 + 14; -- ������
ATTR_BHREC          = ATTR_COUNT_BASE2 + 15; -- ��hp�ָ��ٶ�
ATTR_BSREC          = ATTR_COUNT_BASE2 + 16; -- ��(��ɫsp�ָ��ٶ�)(��ֻΪ����ٶ�)
ATTR_BASPD          = ATTR_COUNT_BASE2 + 17; -- �������
ATTR_BCRTR          = ATTR_COUNT_BASE2 + 18; -- ��������
ATTR_BMSPD          = ATTR_COUNT_BASE2 + 19; -- ���ƶ��ٶ�
ATTR_BEXP           = ATTR_COUNT_BASE2 + 20; -- ����Դ�ɼ��ٶ�
ATTR_BMDEF          = ATTR_COUNT_BASE2 + 21; -- ������ֿ�
ATTR_BOAT_BCRANGE   = ATTR_COUNT_BASE2 + 22; -- basic vessels explosion of shells
ATTR_BOAT_BCSPD     = ATTR_COUNT_BASE2 + 23; -- basic shells flying speed vessels 

ATTR_COUNT_BASE3    = 74;
ATTR_ITEMC_STR      = ATTR_COUNT_BASE3 + 0; -- ��������ϵ��item coefficient���ӳ�
ATTR_ITEMC_AGI      = ATTR_COUNT_BASE3 + 1; -- ���ݵ���ϵ��ӳ�
ATTR_ITEMC_ACC      = ATTR_COUNT_BASE3 + 2; -- רע����ϵ��ӳ�
ATTR_ITEMC_CON      = ATTR_COUNT_BASE3 + 3; -- ���ʵ���ϵ��ӳ�
ATTR_ITEMC_SPR      = ATTR_COUNT_BASE3 + 4; -- ��������ϵ��ӳ�
ATTR_ITEMC_LUK      = ATTR_COUNT_BASE3 + 5; -- ���˵���ϵ��ӳ�
ATTR_ITEMC_ASPD     = ATTR_COUNT_BASE3 + 6; -- �����������ϵ��ӳ�
ATTR_ITEMC_CRTR     = ATTR_COUNT_BASE3 + 7; -- �����������ϵ��ӳ�
ATTR_ITEMC_ATK      = ATTR_COUNT_BASE3 + 8; -- ��С����������ϵ��ӳ�
ATTR_ITEMC_MATK     = ATTR_COUNT_BASE3 + 9; -- ��󹥻�������ϵ��ӳ�
ATTR_ITEMC_DEF      = ATTR_COUNT_BASE3 + 10; -- ����������ϵ��ӳ�
ATTR_ITEMC_MXHP     = ATTR_COUNT_BASE3 + 11; -- ���HP����ϵ��ӳ�
ATTR_ITEMC_MXSP     = ATTR_COUNT_BASE3 + 12; -- ���SP����ϵ��ӳ�
ATTR_ITEMC_FLEE     = ATTR_COUNT_BASE3 + 13; -- �����ʵ���ϵ��ӳ�
ATTR_ITEMC_HIT      = ATTR_COUNT_BASE3 + 14; -- �����ʵ���ϵ��ӳ�
ATTR_ITEMC_CRT      = ATTR_COUNT_BASE3 + 15; -- �����ʵ���ϵ��ӳ�
ATTR_ITEMC_DROP     = ATTR_COUNT_BASE3 + 16; -- Ѱ���ʵ���ϵ��ӳ�
ATTR_ITEMC_HREC     = ATTR_COUNT_BASE3 + 17; -- hp�ָ��ٶȵ���ϵ��ӳ�
ATTR_ITEMC_SREC     = ATTR_COUNT_BASE3 + 18; -- sp�ָ��ٶȵ���ϵ��ӳ�
ATTR_ITEMC_MSPD     = ATTR_COUNT_BASE3 + 19; -- �ƶ��ٶȵ���ϵ��ӳ�
ATTR_ITEMC_EXP      = ATTR_COUNT_BASE3 + 20; -- ��Դ�ɼ��ٶȵ���ϵ��ӳ�
ATTR_ITEMC_MDEF     = ATTR_COUNT_BASE3 + 21; -- ����ֿ�����ϵ��ӳ�

ATTR_COUNT_BASE4    = 96;
ATTR_ITEMV_STR      = ATTR_COUNT_BASE4 + 0; -- ����������ֵ��item value���ӳ�
ATTR_ITEMV_AGI      = ATTR_COUNT_BASE4 + 1; -- ���ݵ�����ֵ�ӳ�
ATTR_ITEMV_ACC      = ATTR_COUNT_BASE4 + 2; -- רע������ֵ�ӳ�
ATTR_ITEMV_CON      = ATTR_COUNT_BASE4 + 3; -- ���ʵ�����ֵ�ӳ�
ATTR_ITEMV_SPR      = ATTR_COUNT_BASE4 + 4; -- ����������ֵ�ӳ�
ATTR_ITEMV_LUK      = ATTR_COUNT_BASE4 + 5; -- ���˵�����ֵ�ӳ�
ATTR_ITEMV_ASPD     = ATTR_COUNT_BASE4 + 6; -- �������������ֵ�ӳ�
ATTR_ITEMV_CRTR     = ATTR_COUNT_BASE4 + 7; -- �������������ֵ�ӳ�
ATTR_ITEMV_ATK      = ATTR_COUNT_BASE4 + 8; -- ��С������������ֵ�ӳ�
ATTR_ITEMV_MATK     = ATTR_COUNT_BASE4 + 9; -- ��󹥻���������ֵ�ӳ�
ATTR_ITEMV_DEF      = ATTR_COUNT_BASE4 + 10; -- ������������ֵ�ӳ�
ATTR_ITEMV_MXHP     = ATTR_COUNT_BASE4 + 11; -- ���HP������ֵ�ӳ�
ATTR_ITEMV_MXSP     = ATTR_COUNT_BASE4 + 12; -- ���SP������ֵ�ӳ�
ATTR_ITEMV_FLEE     = ATTR_COUNT_BASE4 + 13; -- �����ʵ�����ֵ�ӳ�
ATTR_ITEMV_HIT      = ATTR_COUNT_BASE4 + 14; -- �����ʵ�����ֵ�ӳ�
ATTR_ITEMV_CRT      = ATTR_COUNT_BASE4 + 15; -- �����ʵ�����ֵ�ӳ�
ATTR_ITEMV_DROP     = ATTR_COUNT_BASE4 + 16; -- Ѱ���ʵ�����ֵ�ӳ�
ATTR_ITEMV_HREC     = ATTR_COUNT_BASE4 + 17; -- hp�ָ��ٶȵ�����ֵ�ӳ�
ATTR_ITEMV_SREC     = ATTR_COUNT_BASE4 + 18; -- sp�ָ��ٶȵ�����ֵ�ӳ�
ATTR_ITEMV_MSPD     = ATTR_COUNT_BASE4 + 19; -- �ƶ��ٶȵ�����ֵ�ӳ�
ATTR_ITEMV_EXP      = ATTR_COUNT_BASE4 + 20; -- ��Դ�ɼ��ٶȵ�����ֵ�ӳ�
ATTR_ITEMV_MDEF     = ATTR_COUNT_BASE4 + 21; -- ����ֿ�������ֵ�ӳ�

ATTR_COUNT_BASE5    = 118;
ATTR_STATEC_STR     = ATTR_COUNT_BASE5 + 0; -- ����״̬ϵ��state coefficient���ӳ�
ATTR_STATEC_AGI     = ATTR_COUNT_BASE5 + 1; -- ����״̬ϵ��ӳ�
ATTR_STATEC_ACC     = ATTR_COUNT_BASE5 + 2; -- רע״̬ϵ��ӳ�
ATTR_STATEC_CON     = ATTR_COUNT_BASE5 + 3; -- ����״̬ϵ��ӳ�
ATTR_STATEC_SPR     = ATTR_COUNT_BASE5 + 4; -- ����״̬ϵ��ӳ�
ATTR_STATEC_LUK     = ATTR_COUNT_BASE5 + 5; -- ����״̬ϵ��ӳ�
ATTR_STATEC_ASPD    = ATTR_COUNT_BASE5 + 6; -- �������״̬ϵ��ӳ�
ATTR_STATEC_CRTR    = ATTR_COUNT_BASE5 + 7; -- ��������״̬ϵ��ӳ�
ATTR_STATEC_ATK     = ATTR_COUNT_BASE5 + 8; -- ��С������״̬ϵ��ӳ�
ATTR_STATEC_MATK    = ATTR_COUNT_BASE5 + 9; -- ��󹥻���״̬ϵ��ӳ�
ATTR_STATEC_DEF     = ATTR_COUNT_BASE5 + 10; -- ������״̬ϵ��ӳ�
ATTR_STATEC_MXHP    = ATTR_COUNT_BASE5 + 11; -- ���HP״̬ϵ��ӳ�
ATTR_STATEC_MXSP    = ATTR_COUNT_BASE5 + 12; -- ���SP״̬ϵ��ӳ�
ATTR_STATEC_FLEE    = ATTR_COUNT_BASE5 + 13; -- ������״̬ϵ��ӳ�
ATTR_STATEC_HIT     = ATTR_COUNT_BASE5 + 14; -- ������״̬ϵ��ӳ�
ATTR_STATEC_CRT     = ATTR_COUNT_BASE5 + 15; -- ������״̬ϵ��ӳ�
ATTR_STATEC_DROP    = ATTR_COUNT_BASE5 + 16; -- Ѱ����״̬ϵ��ӳ�
ATTR_STATEC_HREC    = ATTR_COUNT_BASE5 + 17; -- hp�ָ��ٶ�״̬ϵ��ӳ�
ATTR_STATEC_SREC    = ATTR_COUNT_BASE5 + 18; -- sp�ָ��ٶ�״̬ϵ��ӳ�
ATTR_STATEC_MSPD    = ATTR_COUNT_BASE5 + 19; -- �ƶ��ٶ�״̬ϵ��ӳ�
ATTR_STATEC_EXP     = ATTR_COUNT_BASE5 + 20; -- ��Դ�ɼ��ٶ�״̬ϵ��ӳ�
ATTR_STATEC_MDEF    = ATTR_COUNT_BASE5 + 21; -- ����ֿ�״̬ϵ��ӳ�

ATTR_COUNT_BASE6    = 140;
ATTR_STATEV_STR     = ATTR_COUNT_BASE6 + 0; -- ����״̬��ֵ��state value���ӳ�
ATTR_STATEV_AGI     = ATTR_COUNT_BASE6 + 1; -- ����״̬��ֵ�ӳ�
ATTR_STATEV_ACC     = ATTR_COUNT_BASE6 + 2; -- רע״̬��ֵ�ӳ�
ATTR_STATEV_CON     = ATTR_COUNT_BASE6 + 3; -- ����״̬��ֵ�ӳ�
ATTR_STATEV_SPR     = ATTR_COUNT_BASE6 + 4; -- ����״̬��ֵ�ӳ�
ATTR_STATEV_LUK     = ATTR_COUNT_BASE6 + 5; -- ����״̬��ֵ�ӳ�
ATTR_STATEV_ASPD    = ATTR_COUNT_BASE6 + 6; -- �������״̬��ֵ�ӳ�
ATTR_STATEV_CRTR    = ATTR_COUNT_BASE6 + 7; -- ��������״̬��ֵ�ӳ�
ATTR_STATEV_ATK     = ATTR_COUNT_BASE6 + 8; -- ��С������״̬��ֵ�ӳ�
ATTR_STATEV_MATK    = ATTR_COUNT_BASE6 + 9; -- ��󹥻���״̬��ֵ�ӳ�
ATTR_STATEV_DEF     = ATTR_COUNT_BASE6 + 10; -- ������״̬��ֵ�ӳ�
ATTR_STATEV_MXHP    = ATTR_COUNT_BASE6 + 11; -- ���HP״̬��ֵ�ӳ�
ATTR_STATEV_MXSP    = ATTR_COUNT_BASE6 + 12; -- ���SP״̬��ֵ�ӳ�
ATTR_STATEV_FLEE    = ATTR_COUNT_BASE6 + 13; -- ������״̬��ֵ�ӳ�
ATTR_STATEV_HIT     = ATTR_COUNT_BASE6 + 14; -- ������״̬��ֵ�ӳ�
ATTR_STATEV_CRT     = ATTR_COUNT_BASE6 + 15; -- ������״̬��ֵ�ӳ�
ATTR_STATEV_DROP    = ATTR_COUNT_BASE6 + 16; -- Ѱ����״̬��ֵ�ӳ�
ATTR_STATEV_HREC    = ATTR_COUNT_BASE6 + 17; -- hp�ָ��ٶ�״̬��ֵ�ӳ�
ATTR_STATEV_SREC    = ATTR_COUNT_BASE6 + 18; -- sp�ָ��ٶ�״̬��ֵ�ӳ�
ATTR_STATEV_MSPD    = ATTR_COUNT_BASE6 + 19; -- �ƶ��ٶ�״̬��ֵ�ӳ�
ATTR_STATEV_EXP     = ATTR_COUNT_BASE6 + 20; -- ��Դ�ɼ��ٶ�״̬��ֵ�ӳ�
ATTR_STATEV_MDEF    = ATTR_COUNT_BASE6 + 21; -- ����ֿ�״̬��ֵ�ӳ�

ATTR_LHAND_ITEMV    = ATTR_COUNT_BASE6 + 22; -- ���ֵ��߼ӳ�

ATTR_COUNT_BASE7        = 163;
ATTR_BOAT_SHIP          = ATTR_COUNT_BASE7 + 0; -- ��ֻID
ATTR_BOAT_HEADER        = ATTR_COUNT_BASE7 + 1; -- ��ͷ����
ATTR_BOAT_BODY          = ATTR_COUNT_BASE7 + 2; -- ��������
ATTR_BOAT_ENGINE        = ATTR_COUNT_BASE7 + 3; -- ����������
ATTR_BOAT_CANNON        = ATTR_COUNT_BASE7 + 4; -- ����������
ATTR_BOAT_PART          = ATTR_COUNT_BASE7 + 5; -- ���������
ATTR_BOAT_DBID          = ATTR_COUNT_BASE7 + 6; -- ������ݿ�洢ID
ATTR_BOAT_DIECOUNT      = ATTR_COUNT_BASE7 + 7; -- �ô���������
ATTR_BOAT_ISDEAD	    = ATTR_COUNT_BASE7 + 8; -- �ô�Ŀǰ�Ƿ�Ϊ����״̬

ATTR_COUNT_BASE8        = 172;
ATTR_BOAT_SKILLC_MNATK  = ATTR_COUNT_BASE8 + 0; -- ��ֻMNATK ����ϵ��Ӱ��
ATTR_BOAT_SKILLC_MXATK  = ATTR_COUNT_BASE8 + 1; -- ��ֻMXATK ����ϵ��Ӱ��
ATTR_BOAT_SKILLC_ADIS   = ATTR_COUNT_BASE8 + 2; -- ��ֻ��������atkrange ����ϵ��Ӱ��
ATTR_BOAT_SKILLC_MSPD   = ATTR_COUNT_BASE8 + 3; -- ��ֻ�ƶ��ٶȼ���ϵ��Ӱ��
ATTR_BOAT_SKILLC_CSPD   = ATTR_COUNT_BASE8 + 4; -- �ڵ������ٶ� ����ϵ��Ӱ��
ATTR_BOAT_SKILLC_ASPD   = ATTR_COUNT_BASE8 + 5; -- ��ֻASPD ����ϵ��Ӱ��
ATTR_BOAT_SKILLC_CRANGE = ATTR_COUNT_BASE8 + 6; -- �ڵ���ը��Χ ����ϵ��Ӱ��
ATTR_BOAT_SKILLC_DEF    = ATTR_COUNT_BASE8 + 7; -- ��ֻDEF ����ϵ��Ӱ��
ATTR_BOAT_SKILLC_RESIST = ATTR_COUNT_BASE8 + 8; -- ��ֻRESIST ����ϵ��Ӱ��
ATTR_BOAT_SKILLC_MXUSE  = ATTR_COUNT_BASE8 + 9; -- ��ֻ����;� ����ϵ��Ӱ��
ATTR_BOAT_SKILLC_USEREC = ATTR_COUNT_BASE8 + 10; -- ��ֻ�;ûظ��ٶ� ����ϵ��Ӱ��
ATTR_BOAT_SKILLC_EXP    = ATTR_COUNT_BASE8 + 11; -- ��ֻ����� ����ϵ��Ӱ��
ATTR_BOAT_SKILLC_CPT    = ATTR_COUNT_BASE8 + 12; -- �������� ����ϵ��Ӱ��
ATTR_BOAT_SKILLC_SPD    = ATTR_COUNT_BASE8 + 13; -- ���� ����ϵ��Ӱ��
ATTR_BOAT_SKILLC_MXSPLY = ATTR_COUNT_BASE8 + 14; -- ��󲹸�ֵ ����ϵ��Ӱ��

ATTR_COUNT_BASE9        = 187;
ATTR_BOAT_SKILLV_MNATK  = ATTR_COUNT_BASE9 + 0; -- ��ֻMNATK ���ܳ���Ӱ��
ATTR_BOAT_SKILLV_MXATK  = ATTR_COUNT_BASE9 + 1; -- ��ֻMXATK ���ܳ���Ӱ��
ATTR_BOAT_SKILLV_ADIS   = ATTR_COUNT_BASE9 + 2; -- ��ֻ��������atkrange ���ܳ���Ӱ��
ATTR_BOAT_SKILLV_MSPD   = ATTR_COUNT_BASE9 + 3; -- ��ֻ�ƶ��ٶȼ��ܳ���Ӱ��
ATTR_BOAT_SKILLV_CSPD   = ATTR_COUNT_BASE9 + 4; -- �ڵ������ٶ� ���ܳ���Ӱ��
ATTR_BOAT_SKILLV_ASPD   = ATTR_COUNT_BASE9 + 5; -- ��ֻASPD ���ܳ���Ӱ��
ATTR_BOAT_SKILLV_CRANGE = ATTR_COUNT_BASE9 + 6; -- �ڵ���ը��Χ ���ܳ���Ӱ��
ATTR_BOAT_SKILLV_DEF    = ATTR_COUNT_BASE9 + 7; -- ��ֻDEF ���ܳ���Ӱ��
ATTR_BOAT_SKILLV_RESIST = ATTR_COUNT_BASE9 + 8; -- ��ֻRESIST ���ܳ���Ӱ��
ATTR_BOAT_SKILLV_MXUSE  = ATTR_COUNT_BASE9 + 9; -- ��ֻ����;� ���ܳ���Ӱ��
ATTR_BOAT_SKILLV_USEREC = ATTR_COUNT_BASE9 + 10; -- ��ֻ�;ûظ��ٶ� ���ܳ���Ӱ��
ATTR_BOAT_SKILLV_EXP    = ATTR_COUNT_BASE9 + 11; -- ��ֻ����� ���ܳ���Ӱ��
ATTR_BOAT_SKILLV_CPT    = ATTR_COUNT_BASE9 + 12; -- �������� ���ܳ���Ӱ��
ATTR_BOAT_SKILLV_SPD    = ATTR_COUNT_BASE9 + 13; -- ���� ���ܳ���Ӱ��
ATTR_BOAT_SKILLV_MXSPLY = ATTR_COUNT_BASE9 + 14; -- ��󲹸�ֵ ���ܳ���Ӱ��

ATTR_COUNT_BASE10        = 202;
ATTR_EXTEND0  = ATTR_COUNT_BASE10 + 0;			--��¼ռ����BUFF����
ATTR_EXTEND1  = ATTR_COUNT_BASE10 + 1;			--��¼��BUFF����
ATTR_EXTEND2  = ATTR_COUNT_BASE10 + 2;			--��¼ռ��ʱ�Ǽ���
ATTR_EXTEND3  = ATTR_COUNT_BASE10 + 3;			--��¼�ڱ�����
ATTR_EXTEND4  = ATTR_COUNT_BASE10 + 4;			--��ȡ�ڱ�����ʱ��
ATTR_EXTEND5  = ATTR_COUNT_BASE10 + 5;			--��ȡ˫��ʱ��
ATTR_EXTEND6  = ATTR_COUNT_BASE10 + 6;			--��¼ʣ��˫��ʱ��
ATTR_EXTEND7  = ATTR_COUNT_BASE10 + 7;			--DEBUFF��ʱ��Ϣλ
ATTR_EXTEND8  = ATTR_COUNT_BASE10 + 8;			--�����ճ��������������
ATTR_EXTEND9  = ATTR_COUNT_BASE10 + 9;			--�����ճ�����������ʱ��

ATTR_MAX_NUM                = 213;

--[[
SetChaAttrMax( ATTR_LV		,	120			)	-- ��ɫ�ȼ�
SetChaAttrMax( ATTR_HP		,	2000000000	)	-- ��ǰHP
SetChaAttrMax( ATTR_SP		,	2000000000	)	-- ��ǰSP
SetChaAttrMax( ATTR_CLASS		,	100			)	-- ��ɫְҵ
SetChaAttrMax( ATTR_CEXP		,	6800000000	)	-- ��ǰ����
SetChaAttrMax( ATTR_NLEXP	,	6800000000	)	-- ��һ�����辭��
SetChaAttrMax( ATTR_AP		,	300			)	-- ���Ե�
SetChaAttrMax( ATTR_TP		,	250			)	-- ���ܵ�
SetChaAttrMax( ATTR_GD		,	2000000000	)	-- ��Ǯ
SetChaAttrMax( ATTR_CLEXP	,	6800000000	)	--��ǰ�ȼ��ľ���
SetChaAttrMax( ATTR_MXHP	,	2000000000	)	--���hp
SetChaAttrMax( ATTR_MXSP		,	2000000000	)	--���sp
SetChaAttrMax( ATTR_BSTR		,	10000			)	-- ������ (orig was 120)
SetChaAttrMax( ATTR_BACC		,	10000			)	-- ������ (orig was 120)
SetChaAttrMax( ATTR_BAGI		,	10000			)	-- ������ (orig was 120)
SetChaAttrMax( ATTR_BCON	,	10000			)	-- ������ (orig was 120)
SetChaAttrMax( ATTR_BSPR		,	10000			)	-- ���� (orig was 120)
SetChaAttrMax( ATTR_BLUK		,	10000			)	-- ������ (orig was 120)
SetChaAttrMax( ATTR_BMXHP	,	2000000000	)	-- �����HP
SetChaAttrMax( ATTR_BMXSP	,	2000000000	)	-- �����SP
SetChaAttrMax( ATTR_BATK	,	13000			)	-- ����С������
SetChaAttrMax( ATTR_BMATK	,	13000			)	-- ����󹥻���
SetChaAttrMax( ATTR_BDEF		,	9999			)	-- �������
SetChaAttrMax( ATTR_BHIT		,	9999			)	-- ��������
SetChaAttrMax( ATTR_BFLEE	,	9999			)	-- ��������
SetChaAttrMax( ATTR_BDROP		,	9999			)	-- ��Ѱ����
SetChaAttrMax( ATTR_BCRT		,	9999			)	-- ������
SetChaAttrMax( ATTR_BHREC	,	9999			)	-- ��hp�ָ��ٶ�
SetChaAttrMax( ATTR_BSREC	,	9999			)	-- ��sp�ָ��ٶ�
SetChaAttrMax( ATTR_BASPD	,	9999			)	-- �������
SetChaAttrMax( ATTR_BCRTR	,	9999			)	-- ��������
SetChaAttrMax( ATTR_BMSPD	,	9999			)	-- ���ƶ��ٶ�
SetChaAttrMax( ATTR_BEXP		,	9999			)	-- ����Դ�ɼ��ٶ�
SetChaAttrMax( ATTR_MSPD	,	9999			)	--�ƶ��ٶ�
SetChaAttrMax( ATTR_LHAND_ITEMV	,	9999			)	--���ֵ��߼ӳ�
--]]

--NOTE: Player Param 1: Player timer.

--Cha_timer: Callback function executed every 1 second (internally as 20 FPS).

char_stats_autocalc = true -- Auto stat calculation is enabled so that everytime the stats are manually editted (by gms)
--they get reset right after. If want to disable it for a code section, set it to false, then put it back to true.

char_stats = {}

char_item_stats_conv = {
	[0]=0,
	[1]=2,
	[2]=1,
	[3]=3,
	[4]=4,
	[5]=5,
	[6]=11,
	[7]=12,
	[8]=8,
	[9]=9,
	[10]=10,
	[11]=14,
	[12]=13,
	[13]=16,
	[14]=15,
	[15]=17,
	[16]=18,
	[17]=6,
	[18]=7,
	[19]=19,
	[20]=20,
	[21]=21
}
item_char_stats_conv = table.inv_func(char_item_stats_conv)

function char_stats.__index(self, key)
	if key == "role" then
		return rawget(self, key)
	else
		if ((key == ATTR_ASPD) or (key == ATTR_BASPD)) then
			return math.floor(100000/GetChaAttr(self.role,key))
		end
		if (((key >= ATTR_COUNT_BASE3) and (key <= (ATTR_COUNT_BASE3 + 21))) or
		((key >= ATTR_COUNT_BASE5) and (key <= (ATTR_COUNT_BASE5 + 21)))) then
			return GetChaAttr(self.role,key)/1000
		else
			return GetChaAttr(self.role,key)
		end
	end
end

function char_stats.__newindex(self, key, value)
	if key == "role" then
		rawset(self, key, value)
	else
		local k_main = char_stats_getcalcbase(key)
		if (k_main ~= -1) and char_stats_autocalc then
			-- ATTR_COUNT_BASE1 = Base modifiable stats. Calculated as follows: (BASE2 * BASE3 + BASE4 ) * BASE5 + BASE6
			--                    or in another way - (Base * Item Mult + Item Add) * Skill Mult + Skill Add
			if 	(((key >= ATTR_COUNT_BASE3) and (key <= (ATTR_COUNT_BASE3 + 21))) or
			((key >= ATTR_COUNT_BASE5) and (key <= (ATTR_COUNT_BASE5 + 21)))) then
				SetChaAttr(self.role,key,value*1000)
			else
				SetChaAttr(self.role,key,value)
			end
			local v_base = GetChaAttr(self.role,(k_main - ATTR_COUNT_BASE1) + ATTR_COUNT_BASE2)
			local v_imult = GetChaAttr(self.role,char_item_stats_conv[(k_main - ATTR_COUNT_BASE1)] + ATTR_COUNT_BASE3)/1000
			local v_iadd = GetChaAttr(self.role,char_item_stats_conv[(k_main - ATTR_COUNT_BASE1)] + ATTR_COUNT_BASE4)
			local v_smult = GetChaAttr(self.role,char_item_stats_conv[(k_main - ATTR_COUNT_BASE1)] + ATTR_COUNT_BASE5)/1000
			local v_sadd = GetChaAttr(self.role,char_item_stats_conv[(k_main - ATTR_COUNT_BASE1)] + ATTR_COUNT_BASE6)
			local v_main = (v_base*v_imult + v_iadd)*v_smult + v_sadd
			if (k_main == ATTR_ASPD) then
				v_main = math.floor(100000/((v_base*v_imult + v_iadd)*v_smult + v_sadd))
			end
			SetChaAttr(self.role,k_main,v_main)
			-- Thus, anytime someone sets one of the values that give the main value, the main value will be set with
			-- the correct value all the time, without exceptions. Will correct all stat related bugs with 1 blow.
		else
			SetChaAttr(self.role,key,value)
		end
		--Automated stat update
		--TODO: Per stat update (not all stats all at once)
		--AttrRecheck(self.role)
	end
end

function char_stats:sync(stat)
    return SyncChar(self.role, stat)
end

function char_stats:wrap(role)
	if role ~= nil and role ~= 0 then
		local o = {}
		o.role = role
		setmetatable(o, self)
		return o
	end
end