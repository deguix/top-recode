#pragma once

#include "publicfun.h"

class CPointTrack {
public:
	CPointTrack()
		: _fSpeed(0.0f),
		  _fProgressRate(0.0f) {
		_fheight = 0;
	}

	void Reset(float fX, float fY, float fZ);
	void AddPoint(float fX, float fY, float fZ, DWORD dwTimeParam);
	void FrameMove(DWORD dwTimeParam);
	void SetSpeed(float fSpeed) { _fSpeed = fSpeed; }

	float GetCurX() { return _vecCur.x; }
	float GetCurY() { return _vecCur.y; }
	float GetCurZ() { return _vecCur.z; }

	int GetPointCnt() { return (int)(_PointList.size()); }

protected:
	struct STrackPoint {
		float x;
		float y;
		float z;
		STrackPoint() : x(-1), y(-1), z(0) {
		}
	};

	STrackPoint _vecLast;
	STrackPoint _vecCur;
	STrackPoint _vecNext;
	float _fProgressRate;
	float _fSpeed;

	float _fheight; //平滑处理高度
	bool _bUpdateHei;

	std::list<STrackPoint> _PointList;
};