#pragma once
#include "MindPower.h"

class AnimCtrlLight;
struct SceneLight {
	enum {
		SL_AMBIENT = 0,
		SL_LIGHT = 1,
		SL_INVALID = LW_INVALID_INDEX,

	};

	DWORD type;
	D3DCOLORVALUE dif;
	D3DCOLORVALUE amb;
	D3DVECTOR pos;
	float range;
	float attenuation0;
	float attenuation1;
	float attenuation2;

	lwPlayPoseInfo ppi;
	AnimCtrlLight* anim_light;

	SceneLight() {
		Reset();
	}

	void Reset() {
		type = SL_INVALID;
		anim_light = nullptr;
		ppi.bit_mask = lwPlayPoseInfoMask::DEFAULT;
		ppi.pose = 0;
		ppi.type = lwPlayPoseEnum::LOOP;
		ppi.velocity = 1.0f;
		ppi.data = lwPlayPoseEnum::INVALID;

		// added by roger {
		ppi.proc = nullptr;
		ppi.proc_param = nullptr;

		ZeroMemory(&ppi.blend_info, sizeof(ppi.blend_info));
		// }
	}
	int GetSquareMagnitude(int xo, int yo, int zo) {
		int i = (int)(xo - pos.x);
		int j = (int)(yo - pos.y);
		int k = (int)(zo - pos.z);
		return (i * i + j * j + k * k);
	}

	void GetLightData(D3DLIGHTX* lgt) {
		memset(lgt, 0, sizeof(D3DLIGHTX));
		lgt->Type = D3DLIGHT_POINT;
		lgt->Range = range;
		lgt->Position = pos;
		lgt->Ambient = amb;
		lgt->Diffuse = dif;
		lgt->Attenuation0 = attenuation0;
		lgt->Attenuation1 = attenuation1;
		lgt->Attenuation2 = attenuation2;
	}

	int UpdateAnimLight();
};

struct IndexDataSceneLight {
	DWORD id;
	SceneLight light;
};

class AnimCtrlLight {
private:
	IndexDataSceneLight* _data_seq{nullptr};
	DWORD _data_num{0};
	std::shared_ptr<lwIPoseCtrl> _pose_ctrl{nullptr};

public:
	~AnimCtrlLight() {
		SAFE_DELETE_ARRAY(_data_seq);
	}

	int Load(const char* file);
	int UpdateObject(SceneLight* ret_obj);
	void SetData(IndexDataSceneLight* buf, DWORD num);
};
