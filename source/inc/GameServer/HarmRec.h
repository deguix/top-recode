#pragma once

// 仇恨度系统 - 伤害值管理
class CCharacter;

#define MAX_HARM_REC 5
#define MAX_VALID_CNT 8

extern BOOL g_bLogHarmRec;

struct SHarmRec // 伤害记录
{
	CCharacter* pAtk{nullptr}; // 攻击者指针
	DWORD sHarm{0};			   // 累加伤害值
	DWORD sHate{0};			   // 仇恨度
	BYTE btValid{0};		   // 是否有效
	DWORD dwID{0};
	DWORD dwTime{0}; // 第一次攻击的时间
	BOOL IsChaValid(); // 返回一个角色是否还有效
};

class CHateMgr {
public:
	CHateMgr() {}

	CCharacter* GetCurTarget();
	void AddHarm(CCharacter* pAtk, short sHarm, DWORD dwID);
	void AddHate(CCharacter* pAtk, short sHate, DWORD dwID);
	void UpdateHarmRec(CCharacter* pSelf);
	void ClearHarmRec();
	void ClearHarmRecByCha(CCharacter* pAtk);
	void DebugNotice(CCharacter* pSelf);
	SHarmRec* GetHarmRec(int nNo) { return &_HarmRec[nNo]; }

protected:
	SHarmRec _HarmRec[MAX_HARM_REC];
	DWORD _dwLastDecValid{0};
	DWORD _dwLastSortTick{0};
};