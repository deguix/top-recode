//
#pragma once

#include "lwHeader.h"
#include "lwDirectX.h"
#include "lwStdInc.h"
#include "lwITypes.h"

LW_BEGIN

enum lwVertexShaderRegisterInput {
	VSREG_V_POSITION = 0,
	VSREG_V_BLENDWEIGHT = 1,
	VSREG_V_BLENDINDICES = 2,
	VSREG_V_NORMAL = 3,
	VSREG_V_PSIZE = 4,
	VSREG_V_DIFFUSE = 5,
	VSREG_V_SPECULAR = 6,
	VSREG_V_TEXCOORD0 = 7,
	VSREG_V_TEXCOORD1 = 8,
	VSREG_V_TEXCOORD2 = 9,
	VSREG_V_TEXCOORD3 = 10,
	VSREG_V_TEXCOORD4 = 11,
	VSREG_V_TEXCOORD5 = 12,
	VSREG_V_TEXCOORD6 = 13,
	VSREG_V_TEXCOORD7 = 14,
	VSREG_V_POSITION2 = 15,
	VSREG_V_NORMAL2 = 16
};

enum class lwVertexShaderLightTypeEnum : DWORD {
	POINT = 1,
	SPOT = 2,
	DIRECTIONAL = 3,
	INVALID = 0xFFFFFFFF
};

enum class lwVertexShaderAnimTypeEnum : DWORD {
	_NULL = 0x0000,

	VERTEXMATRIX = _NULL, // donot need vs support

	VERTEXBLEND0 = 0x0001,
	VERTEXBLEND1 = 0x0002,
	VERTEXBLEND2 = 0x0003,
	VERTEXBLEND3 = 0x0004,

	TEXTURETRANSFORM0 = 0x0010,
	TEXTURETRANSFORM1 = 0x0020,
	TEXTURETRANSFORM2 = 0x0030,
	TEXTURETRANSFORM3 = 0x0040,

	INVALID = 0xFFFFFFFF
};

struct lwShaderDeclQueryInfo {
	VertexDeclarationType decl_type{VertexDeclarationType::INVALID};
	lwVertexShaderLightTypeEnum light_type{lwVertexShaderLightTypeEnum::INVALID};
	lwVertexShaderAnimTypeEnum anim_type{lwVertexShaderAnimTypeEnum::INVALID};
};

struct lwShaderDeclCreateInfo {
	VertexShaderType shader_id{VertexShaderType::INVALID};
	VertexDeclarationType decl_type{VertexDeclarationType::INVALID};
	lwVertexShaderLightTypeEnum light_type{lwVertexShaderLightTypeEnum::INVALID};
	lwVertexShaderAnimTypeEnum anim_type{lwVertexShaderAnimTypeEnum::INVALID};
	char file[LW_MAX_NAME];
};

inline lwShaderDeclCreateInfo SDCI_VALUE(VertexShaderType shader, VertexDeclarationType decl, lwVertexShaderLightTypeEnum light, lwVertexShaderAnimTypeEnum anim, const char* file) {
	lwShaderDeclCreateInfo i;
	i.shader_id = shader;
	i.decl_type = decl;
	i.light_type = light;
	i.anim_type = anim;
	_tcsncpy_s(i.file, file, _TRUNCATE);

	return i;
}

#if (defined LW_USE_DX8)
#endif

LW_END