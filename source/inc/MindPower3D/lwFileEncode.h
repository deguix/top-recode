//
#pragma once

#include "lwHeader.h"
#include "lwStdInc.h"
#include "lwErrorCode.h"
#include "lwInterfaceExt.h"

//////////////
//
LW_BEGIN

class lwTexEncode {
private:
	LW_RESULT _Encode0(std::shared_ptr<lwIBuffer> buf);
	LW_RESULT _Decode0(std::shared_ptr<lwIBuffer> buf);
	LW_RESULT _Encode1(std::shared_ptr<lwIBuffer> buf);
	LW_RESULT _Decode1(std::shared_ptr<lwIBuffer> buf);

public:
	LW_RESULT Encode(std::shared_ptr<lwIBuffer> buf);
	LW_RESULT Decode(std::shared_ptr<lwIBuffer> buf);
};

LW_END