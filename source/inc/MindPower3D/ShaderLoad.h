//
#pragma once

#include "lwHeader.h"
#include "lwStdInc.h"
#include "lwClassDecl.h"
#include "lwInterface.h"
#include "lwShaderTypes.h"
#include "lwIUtil.h"
LW_USING

LW_RESULT MINDPOWER_API LoadShader0(lwISysGraphics* sys_graphics);