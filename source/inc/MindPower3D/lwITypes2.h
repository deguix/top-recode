//
#pragma once

#include "lwHeader.h"
#include "lwStdInc.h"
#include "lwMath.h"
#include "lwDirectX.h"
#include "lwClassDecl.h"
#include "lwITypes.h"

#include <memory>

LW_BEGIN

template <DWORD set_size, DWORD seq_size>
struct lwRenderStateSetTemplate {
	enum {
		SET_SIZE = set_size,
		SEQUENCE_SIZE = seq_size,
	};

	lwRenderStateValue rsv_seq[set_size][seq_size];
};

template <DWORD set_size, DWORD seq_size>
inline void lwRenderStateSetTemplate_Construct(lwRenderStateSetTemplate<set_size, seq_size>* obj) {
	using type_value = lwRenderStateSetTemplate<set_size, seq_size>;

	for (DWORD i = 0; i < type_value::SET_SIZE; i++) {
		for (DWORD j = 0; j < type_value::SEQUENCE_SIZE; j++) {
			obj->rsv_seq[i][j].state = LW_INVALID_INDEX;
			obj->rsv_seq[i][j].state = LW_INVALID_INDEX;
		}
	}
}
template <DWORD set_size, DWORD seq_size>
LW_RESULT lwSetRenderStateSet(lwRenderStateSetTemplate<set_size, seq_size>* obj, DWORD set_id, DWORD state, DWORD value) {
	using type_value = lwRenderStateSetTemplate<set_size, seq_size>;

	DWORD i;
	lwRenderStateValue* rsv_seq = obj->rsv_seq[set_id];
	lwRenderStateValue* rsv;

	for (i = 0; i < type_value::SEQUENCE_SIZE; i++) {
		rsv = &rsv_seq[i];

		if (rsv->state == state) {
			rsv->value = value;
			goto __ret;
		}
	}

	for (i = 0; i < type_value::SEQUENCE_SIZE; i++) {
		rsv = &rsv_seq[i];

		if (rsv->state == LW_INVALID_INDEX) {
			rsv->state = state;
			rsv->value = value;
			goto __ret;
		}
	}

__ret:
	return i == type_value::SEQUENCE_SIZE ? LW_RET_FAILED : LW_RET_OK;
}

template <DWORD set_size, DWORD seq_size>
LW_RESULT lwClearRenderStateSet(lwRenderStateSetTemplate<set_size, seq_size>* obj, DWORD set_id, DWORD state) {
	using type_value = lwRenderStateSetTemplate<set_size, seq_size>;

	DWORD i;
	lwRenderStateValue* rsv;
	lwRenderStateValue* rsv_seq = obj->rsv_seq[set_id];

	for (i = 0; i < type_value::SEQUENCE_SIZE; i++) {
		rsv = &rsv_seq[i];

		if (rsv->state == state) {
			for (DWORD j = i; j < type_value::SEQUENCE_SIZE - 1; j++) {
				if (rsv_seq[j + 1].state == LW_INVALID_INDEX)
					goto __ret;

				rsv_seq[j] = rsv_seq[j + 1];
			}

			goto __ret;
		}
	}

__ret:

	return i == type_value::SEQUENCE_SIZE ? LW_RET_FAILED : LW_RET_OK;
}

using lwRenderStateSetMesh2 = lwRenderStateSetTemplate<2, LW_MESH_RS_NUM>;
using lwRenderStateSetMtl2 = lwRenderStateSetTemplate<2, LW_MTL_RS_NUM>;
using lwTextureStageStateSetTex2 = lwRenderStateSetTemplate<2, LW_TEX_TSS_NUM>;
using lwRenderStateSetTex2 = lwRenderStateSetTemplate<2, LW_TEX_RS_NUM>;

using lwMtlDRS = lwRenderStateSetMtl2;
using lwTexDTSS = lwRenderStateSetTemplate<2, LW_TEX_DTSS_NUM>;
using lwTexDRS = lwRenderStateSetTemplate<2, LW_TEX_DRS_NUM>;

struct lwRenderStateAtom {
	DWORD state;
	DWORD value0;
	DWORD value1;
};
void inline lwRenderStateAtom_Construct(lwRenderStateAtom* obj) {
	obj->state = LW_INVALID_INDEX;
	obj->value0 = 0;
	obj->value1 = 0;
}
void inline lwRenderStateAtom_Construct_A(lwRenderStateAtom* obj_seq, DWORD num) {
	for (DWORD i = 0; i < num; i++) {
		lwRenderStateAtom_Construct(&obj_seq[i]);
	}
}

inline void RSA_VALUE(lwRenderStateAtom* obj, DWORD state, DWORD value) {
	obj->state = state;
	obj->value0 = value;
	obj->value1 = value;
}

enum class lwTexInfoTypeEnum : DWORD {
	FILE = 0,
	SIZE,
	DATA,
	INVALID = LW_INVALID_INDEX,
};

struct lwTexFileInfo {
};
struct lwTexMemInfo {
};

struct lwTexInfo {
	DWORD stage{LW_INVALID_INDEX};
	DWORD level{0};
	DWORD usage{0};
	D3DFORMAT format{D3DFMT_UNKNOWN};
	D3DPOOL pool{D3DPOOL_FORCE_DWORD};
	DWORD byte_alignment_flag{0};
	lwTexInfoTypeEnum type{lwTexInfoTypeEnum::INVALID}; // file texture or user-defined texture
	// user-defined texture
	DWORD width{0}; // 当使用lwTexInfoTypeEnum::DATA的时候，width表示data size
	DWORD height{0};
	// file texture
	lwColorKeyTypeEnum colorkey_type{lwColorKeyTypeEnum::NONE};
	lwColorValue4b colorkey{.color=0};
	char file_name[LW_MAX_NAME]{'\0'};
	void* data{nullptr};

	lwRenderStateAtom tss_set[LW_TEX_TSS_NUM]{
		{LW_INVALID_INDEX, 0, 0},
		{LW_INVALID_INDEX, 0, 0},
		{LW_INVALID_INDEX, 0, 0},
		{LW_INVALID_INDEX, 0, 0},
		{LW_INVALID_INDEX, 0, 0},
		{LW_INVALID_INDEX, 0, 0},
		{LW_INVALID_INDEX, 0, 0},
		{LW_INVALID_INDEX, 0, 0}
	};
};

struct lwTexInfo_nopointer {
	DWORD stage{LW_INVALID_INDEX};
	DWORD level{0};
	DWORD usage{0};
	D3DFORMAT format{D3DFMT_UNKNOWN};
	D3DPOOL pool{D3DPOOL_FORCE_DWORD};
	DWORD byte_alignment_flag{0};
	lwTexInfoTypeEnum type{lwTexInfoTypeEnum::INVALID}; // file texture or user-defined texture
	// user-defined texture
	DWORD width{0}; // 当使用lwTexInfoTypeEnum::DATA的时候，width表示data size
	DWORD height{0};
	// file texture
	lwColorKeyTypeEnum colorkey_type{lwColorKeyTypeEnum::NONE};
	lwColorValue4b colorkey{.color = 0};
	char file_name[LW_MAX_NAME]{'\0'};
	uint32_t padding{};

	lwRenderStateAtom tss_set[LW_TEX_TSS_NUM]{
		{LW_INVALID_INDEX, 0, 0},
		{LW_INVALID_INDEX, 0, 0},
		{LW_INVALID_INDEX, 0, 0},
		{LW_INVALID_INDEX, 0, 0},
		{LW_INVALID_INDEX, 0, 0},
		{LW_INVALID_INDEX, 0, 0},
		{LW_INVALID_INDEX, 0, 0},
		{LW_INVALID_INDEX, 0, 0}};
};

struct lwMtlInfo {
	lwMaterial mtl;

	lwRenderStateSetMtl2 rs_set;
};

struct lwBlendInfo {
	union {
		BYTE index[4];
		DWORD indexd;
	};
	float weight[4];
};

struct lwSubsetInfo {
	DWORD primitive_num;
	DWORD start_index;
	DWORD vertex_num;
	DWORD min_index;
};

inline void lwSubsetInfo_Construct(lwSubsetInfo* obj, DWORD pri_num, DWORD start_index, DWORD vertex_num, DWORD min_index) {
	obj->primitive_num = pri_num;
	obj->start_index = start_index;
	obj->vertex_num = vertex_num;
	obj->min_index = min_index;
}
struct lwBoundingBoxInfo {
	DWORD id;
	lwBox box;
	lwMatrix44 mat;
};

struct lwBoundingSphereInfo {
	DWORD id;
	lwSphere sphere;
	lwMatrix44 mat;
};

struct lwIndexMatrix44 {
	DWORD id;
	lwMatrix44 mat;
};

struct lwHelperDummyInfo {
	DWORD id;
	lwMatrix44 mat;
	lwMatrix44 mat_local;
	DWORD parent_type; // 0: default, 1: bone parent, 2: bone dummy parent
	DWORD parent_id;
};

struct lwHelperMeshFaceInfo {
	DWORD vertex[3];
	DWORD adj_face[3];

	lwPlane plane;
	lwVector3 center;
};

struct lwHelperMeshInfo {
	lwHelperMeshInfo()
		: id(LW_INVALID_INDEX), type(0), sub_type(0), state(1),
		  vertex_seq(nullptr), face_seq(nullptr), vertex_num(0), face_num(0) {
		name[0] = '\0';
	}

	~lwHelperMeshInfo() {
		LW_SAFE_DELETE_A(vertex_seq);
		LW_SAFE_DELETE_A(face_seq);
	}

	LW_RESULT Copy(const lwHelperMeshInfo* src);

	DWORD id;
	DWORD type; // helper mesh type
	char name[LW_CHAR_32];
	DWORD state;
	DWORD sub_type;
	lwMatrix44 mat;
	lwBox box;
	lwVector3* vertex_seq;
	lwHelperMeshFaceInfo* face_seq;

	DWORD vertex_num;
	DWORD face_num;
};

struct lwHelperBoxInfo {
	lwHelperBoxInfo()
		: id(0), type(0), state(1) {
		name[0] = '\0';
	}

	DWORD id;
	DWORD type;
	DWORD state;
	lwBox box;
	lwMatrix44 mat;
	char name[LW_CHAR_32];

	LW_RESULT Copy(const lwHelperBoxInfo* src) {
		memcpy(this, src, sizeof(lwHelperBoxInfo));
		return LW_RET_OK;
	}
};

struct lwMeshInfo {
	struct lwMeshInfoHeader {
		DWORD fvf;
		D3DPRIMITIVETYPE pt_type;

		DWORD vertex_num;
		DWORD index_num;
		DWORD subset_num;
		DWORD bone_index_num;
		DWORD bone_infl_factor;
		DWORD vertex_element_num;

		lwRenderStateAtom rs_set[LW_MESH_RS_NUM];
	};

	union {
		lwMeshInfoHeader header;

		struct
		{
			DWORD fvf;
			D3DPRIMITIVETYPE pt_type;

			DWORD vertex_num;
			DWORD index_num;
			DWORD subset_num;
			DWORD bone_index_num;
			DWORD bone_infl_factor;
			DWORD vertex_element_num;

			lwRenderStateAtom rs_set[LW_MESH_RS_NUM];
		};
	};

	lwVector3* vertex_seq{nullptr};
	lwVector3* normal_seq{nullptr};
	union {
		lwVector2* texcoord_seq[LW_MAX_TEXTURESTAGE_NUM];
		struct
		{
			lwVector2* texcoord0_seq;
			lwVector2* texcoord1_seq;
			lwVector2* texcoord2_seq;
			lwVector2* texcoord3_seq;
		};
	};
	DWORD* vercol_seq{nullptr};
	DWORD* index_seq{nullptr};
	DWORD* bone_index_seq{nullptr};
	lwBlendInfo* blend_seq{nullptr};
	lwSubsetInfo* subset_seq{nullptr};
	D3DVERTEXELEMENTX* vertex_element_seq{nullptr};

	lwMeshInfo() //need to initialize unions here
		: texcoord0_seq(nullptr), texcoord1_seq(nullptr), texcoord2_seq(nullptr), texcoord3_seq(nullptr),
		  vertex_num(0), index_num(0), subset_num(0), bone_index_num(0),
		  bone_infl_factor(0), vertex_element_num(0) {
		lwRenderStateAtom_Construct_A(rs_set, LW_MESH_RS_NUM);
	}

	~lwMeshInfo() {
		LW_IF_DELETE_A(vertex_seq);
		LW_IF_DELETE_A(normal_seq);
		LW_IF_DELETE_A(vercol_seq);
		LW_IF_DELETE_A(texcoord0_seq);
		LW_IF_DELETE_A(texcoord1_seq);
		LW_IF_DELETE_A(texcoord2_seq);
		LW_IF_DELETE_A(texcoord3_seq);
		LW_IF_DELETE_A(index_seq);
		LW_IF_DELETE_A(blend_seq);
		LW_IF_DELETE_A(bone_index_seq);
		LW_IF_DELETE_A(subset_seq);
		LW_IF_DELETE_A(vertex_element_seq);
	}

	void ResetIndexBuffer(const DWORD* buf) {
		memcpy(index_seq, buf, sizeof(DWORD) * index_num);
	}
};
using lwMeshInfoHeader = lwMeshInfo::lwMeshInfoHeader;

inline void lwMeshInfo_Construct(lwMeshInfo* obj) {
	obj->fvf = 0;
	obj->pt_type = D3DPT_FORCE_DWORD;
	obj->vertex_num = 0;
	obj->subset_num = 0;
	obj->index_num = 0;
	obj->bone_index_num = 0;
	obj->bone_infl_factor = 0;
	obj->vertex_seq = nullptr;
	obj->normal_seq = nullptr;
	obj->vercol_seq = nullptr;
	obj->blend_seq = nullptr;
	obj->index_seq = nullptr;
	obj->texcoord0_seq = nullptr;
	obj->texcoord1_seq = nullptr;
	obj->texcoord2_seq = nullptr;
	obj->texcoord3_seq = nullptr;
	obj->subset_seq = nullptr;
	obj->bone_index_seq = nullptr;
	obj->vertex_element_seq = nullptr;
	obj->vertex_element_num = 0;
	lwRenderStateAtom_Construct_A(obj->rs_set, LW_MESH_RS_NUM);
}

inline void lwMeshInfo_Destruct(lwMeshInfo* obj) {
	LW_IF_DELETE_A(obj->vertex_seq);
	LW_IF_DELETE_A(obj->normal_seq);
	LW_IF_DELETE_A(obj->vercol_seq);
	LW_IF_DELETE_A(obj->texcoord0_seq);
	LW_IF_DELETE_A(obj->texcoord1_seq);
	LW_IF_DELETE_A(obj->texcoord2_seq);
	LW_IF_DELETE_A(obj->texcoord3_seq);
	LW_IF_DELETE_A(obj->index_seq);
	LW_IF_DELETE_A(obj->blend_seq);
}

//enum class lwMtlTexInfoTransparencyTypeEnum : DWORD {
//    FILTER =          0,
//    ADDITIVE =        1,
//    SUBTRACTIVE =     2,
//};

enum class lwMtlTexInfoTransparencyTypeEnum : DWORD {
	FILTER = 0,
	ADDITIVE = 1,
	ADDITIVE1 = 2,
	ADDITIVE2 = 3,
	ADDITIVE3 = 4,
	SUBTRACTIVE = 5,
	SUBTRACTIVE1 = 6,
	SUBTRACTIVE2 = 7,
	SUBTRACTIVE3 = 8,
};
DEFINE_BITMASK_OPERATORS(lwMtlTexInfoTransparencyTypeEnum)

struct lwMtlTexInfo {
	float opacity;
	lwMtlTexInfoTransparencyTypeEnum transp_type;
	lwMaterial mtl;
	//lwRenderStateSetMtl2 rs_set;
	lwRenderStateAtom rs_set[LW_MTL_RS_NUM];
	lwTexInfo tex_seq[LW_MAX_TEXTURESTAGE_NUM];

	lwMtlTexInfo() {
		opacity = 1.0f;
		transp_type = lwMtlTexInfoTransparencyTypeEnum::FILTER;
		lwMaterial_Construct(&mtl);
		lwRenderStateAtom_Construct_A(rs_set, LW_MTL_RS_NUM);
	}
};

//TODO: Make anything that uses this safe
enum class lwObjectStateEnum : DWORD {
	VISIBLE = 0,
	ENABLE = 1,
	UPDATETRANSPSTATE = 3,
	_TRANSPARENT = 4,
	FRAMECULLING = 5,

	INVALID = LW_INVALID_INDEX,
	OBJECT_STATE_NUM = 8
};

class lwStateCtrl {
public:
	BYTE _state_seq[(DWORD)lwObjectStateEnum::OBJECT_STATE_NUM];

public:
	lwStateCtrl() {
		SetDefaultState();
	}
	void SetDefaultState() {
		memset(_state_seq, 0, sizeof(_state_seq));
		_state_seq[(DWORD)lwObjectStateEnum::VISIBLE] = 1;
		_state_seq[(DWORD)lwObjectStateEnum::ENABLE] = 1;
		_state_seq[(DWORD)lwObjectStateEnum::UPDATETRANSPSTATE] = 1;
		_state_seq[(DWORD)lwObjectStateEnum::_TRANSPARENT] = 0;
		_state_seq[(DWORD)lwObjectStateEnum::FRAMECULLING] = 0;
	}
	inline void SetState(DWORD state, BYTE value) { _state_seq[state] = value; }
	inline BYTE GetState(DWORD state) const { return _state_seq[state]; }
};

class lwIBuffer;
struct lwSysMemTexInfo {
	DWORD level;
	DWORD usage;
	D3DFORMAT format;
	DWORD width;
	DWORD height;
	DWORD filter;
	DWORD mip_filter;
	DWORD colorkey;
	char file_name[LW_MAX_PATH];
	IDirect3DTextureX* sys_mem_tex{nullptr};
	std::shared_ptr<lwIBuffer> buf;
	//BYTE* data;
	//DWORD data_size;
};

LW_END