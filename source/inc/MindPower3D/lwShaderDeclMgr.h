//
#pragma once

#include "lwHeader.h"
#include "lwDirectX.h"
#include "lwFrontAPI.h"
#include "lwShaderTypes.h"
#include "lwInterfaceExt.h"

LW_BEGIN

// ===============================================
enum {
	SHADER_DECL_SET_NUM = 16,
	SHADER_DECL_NUM = 32,
};

class lwShaderDeclInfo {
public:
	VertexShaderType shader_id{VertexShaderType::INVALID};
	lwVertexShaderLightTypeEnum light_type{lwVertexShaderLightTypeEnum::INVALID};
	lwVertexShaderAnimTypeEnum anim_type{lwVertexShaderAnimTypeEnum::INVALID};
	char file[LW_MAX_NAME];
};

class lwShaderDeclSet {
public:
	VertexDeclarationType decl_id;
	DWORD decl_num;
	lwShaderDeclInfo* decl_seq;

public:
	lwShaderDeclSet(VertexDeclarationType decl_type, DWORD decl_size) {
		decl_id = decl_type;
		decl_num = decl_size;
		decl_seq = LW_NEW(lwShaderDeclInfo[decl_num]);
	}
	~lwShaderDeclSet() {
		LW_SAFE_DELETE_A(decl_seq);
	}
};

class lwShaderDeclMgr : public lwIShaderDeclMgr {
	LW_STD_DECLARATION()

private:
	lwIShaderMgr* _shader_mgr;
	lwShaderDeclSet* _decl_set[SHADER_DECL_NUM];

public:
	lwShaderDeclMgr(lwIShaderMgr* shader_mgr);
	virtual ~lwShaderDeclMgr();

	LW_RESULT CreateShaderDeclSet(VertexDeclarationType decl_type, DWORD buf_size) override;
	LW_RESULT SetShaderDeclInfo(lwShaderDeclCreateInfo* info) override;
	LW_RESULT QueryShaderHandle(VertexShaderType* shader_handle, const lwShaderDeclQueryInfo* info) override;
};

LW_END