//
#pragma once

#include "lwHeader.h"
#include "lwStdInc.h"
#include "lwDirectX.h"
#include "lwMath.h"
#include "lwErrorCode.h"
#include "EnumBitmask.hpp"

LW_BEGIN

#define LW_MAX_SUBSET_NUM 16
#define LW_MAX_BLENDWEIGHT_NUM 4
#define LW_MAX_BONE_NUM 25
#define LW_MAX_BONEDUMMY_NUM 64
#define LW_MAX_MODEL_GEOM_OBJ_NUM 32
#define LW_MAX_MODEL_OBJ_NUM LW_MAX_MODEL_GEOM_OBJ_NUM + 1 // geometry_num + helper
#define LW_MAX_GEOM_DUMMY_NUM 8
#define LW_MAX_OBJDUMMY_NUM 8
#define LW_MAX_HELPER_MESH_NUM 8
#define LW_MAX_HELPER_BOX_NUM 8
#define LW_MAX_LINK_ITEM_NUM 16

#define LW_MAX_RENDERSTATE_NUM 210 // dx9
#define LW_MAX_TEXTURESTAGE_NUM 4  // max 8
#define LW_MAX_SAMPLESTAGE_NUM 4
#define LW_MAX_TEXTURESTAGESTATE_NUM 33
#define LW_MAX_SAMPLESTATE_NUM 14

#define LW_INVALID_RS_VALUE D3DRS_FORCE_DWORD
#define LW_INVALID_TSS_VALUE D3DTSS_FORCE_DWORD
#define LW_INVALID_SS_VALUE D3DSAMP_FORCE_DWORD

#define LW_TEX_TSS_NUM 8
#define LW_TEX_RS_NUM 2
#define LW_MTL_RS_NUM 8
#define LW_MESH_RS_NUM 8
#define LW_TEX_DTSS_NUM 4
#define LW_TEX_DRS_NUM 4
#define LW_MESH_DRS_NUM 4
#define LW_MTL_RSA_NUM 4
#define LW_MAX_STREAM_NUM 8
#define LW_MAX_LIGHT_NUM 8
#define LW_MAX_SUBSKIN_NUM 10
#define LW_MAX_MTL_TEX_NUM 4

#define LW_MAX_BOUNDING_OBJ_NUM 4
#define LW_MAX_BOUNDING_BOX_NUM 8
#define LW_MAX_BOUNDING_SPHERE_NUM 8

#define LW_MAX_ITEM_LINK_NUM 8

#define LW_RENDER_CTRL_PROC_NUM 512

// 每个Primitive最多可以有LW_MAX_OBJ_DUMMY_NUM个Dummy，其中用于得到逆矩阵的Dummy个数为
// LW_MAX_OBJ_DUMMY_INV_NUM个，所以这里规定在max中制作dummy的时候dummy的id具有范围限制
// 具体数值如下：dummy_0 - dummy_3用来作为LW_MAX_OBJ_DUMMY_INV_NUM
//               dummy_4 - dummy_7用来作为特效或者其他不需要Inverse Matrix的链接id
#define LW_MAX_OBJ_DUMMY_NUM 16
#define LW_MAX_OBJ_DUMMY_INV_NUM 4

// Pose Ctrl Info
#define MAX_KEY_FRAME_NUM 8

enum class HelperType : DWORD {
	DUMMY = 0x0001,
	BOX = 0x0002,
	MESH = 0x0004,
	BOUNDINGBOX = 0x0010,
	BOUNDINGSPHERE = 0x0020,
	INVALID = 0x0000
};
DEFINE_BITMASK_OPERATORS(HelperType)

enum class GeoMObjType : DWORD {
	GENERIC = 0,
	CHECK_BB = 1,  //无方向判断
	CHECK_BB2 = 2, //有方向判断，这里定义有效方向为( 0, 1, 0 )
};
DEFINE_BITMASK_OPERATORS(GeoMObjType)

enum {
	TSS_BEGIN = 0,
	TSS_END = 1,
	RS_BEGIN = 0,
	RS_END = 1,
};

enum class AnimCtrlType : DWORD {
	MATRIX = 0,
	BONE = 1,
	TEXCOORD = 2,
	TEXIMG = 3,
	MTLOPACITY = 4,

	NUM,

	MAT = MATRIX,
	TEXUV = TEXCOORD,

	INVALID = 0xffffffff
};
DEFINE_BITMASK_OPERATORS(AnimCtrlType)

// resource file type enumulation
// *.lmo, *.lgo, *.lab, *.lam, *.lat
enum class lwResourceFileType : DWORD {
	GENERIC = 0,
	MODEL = 1,
	GEOMETRY = 2,
	INVALID = 0xffffffff
};
DEFINE_BITMASK_OPERATORS(lwResourceFileType)

enum class VertexShaderType : DWORD {
	// vertex shader
	__begin_vs_type = 0,

	PU4NT0_LD,
	PB1U4NT0_LD,
	PB2U4NT0_LD,
	PB3U4NT0_LD,

	PNT0_LD,
	PNT0_LD_TT0,
	PNT0_TT0,

	PNDT0,
	PNDT0_TT0,
	PNDT0_LD,
	PNDT0_LD_TT0,

	PB4U4NT0_LD_DX9,

	USER_DEFINE = 0xff,

	BUFFER_SIZE = 1024,

	BEGIN = USER_DEFINE, // lwVertexShaderTypesEnum::VST_USER_DEFINE == 255 //TODO: Fix this (join with regular ShaderType)

	SKINMESH0_TT1,
	SKINMESH1_TT1,
	SKINMESH2_TT1,
	SKINMESH3_TT1,

	SKINMESH0_TT2,
	SKINMESH1_TT2,
	SKINMESH2_TT2,
	SKINMESH3_TT2,

	SKINMESH0_TT3,
	SKINMESH1_TT3,
	SKINMESH2_TT3,
	SKINMESH3_TT3,

	EFFECT_E1,
	EFFECT_E2,
	EFFECT_E3,
	EFFECT_E4,

	FONT_E5,
	SHADE_E6,

	MINIMAP_E6,

	INVALID = 0xffffffff
};

enum class VertexDeclarationType : DWORD {
	// vertex declaration
	__begin_decl_type = 0,
	PD = 0,
	PDT0,
	PDT1,
	PDT2,
	PDT3,
	PNT0,
	PND,
	PNDT0,
	PNDT1,
	PNDT2,
	PNDT3,

	PU4NT0,
	PB1U4NT0,
	PB2U4NT0,
	PB3U4NT0,

	PB4U4NT0_DX9,

	USER_DEFINE = 0xff,

	INVALID = 0xFFFFFFFF
};

struct lwRenderCtrlCreateInfo {
	DWORD ctrl_id{LW_INVALID_INDEX};
	VertexDeclarationType decl_id{VertexDeclarationType::INVALID};
	VertexShaderType vs_id{VertexShaderType::INVALID};
	DWORD ps_id{LW_INVALID_INDEX};
};

class lwIRenderCtrlVS;
using lwRenderCtrlVSCreateProc = lwIRenderCtrlVS* (*)();

enum class lwPathInfoType : DWORD {
	DEFAULT = 0x0000,
	WORKINGDIRECTORY,
	DATA,

	SCRIPT,
	SOUND,
	SHADER,

	ANIMATION,

	MODEL,
	MODEL_CHARACTER,
	MODEL_EFFECT,
	MODEL_SCENE,
	MODEL_ITEM,

	TEXTURE,
	TEXTURE_CHARACTER,
	TEXTURE_SCENE,
	TEXTURE_ITEM,
	TEXTURE_EFFECT,

	EFFECT,

	NUM,
};
DEFINE_BITMASK_OPERATORS(lwPathInfoType)

enum class lwColorKeyTypeEnum : DWORD {
	NONE = 0,
	COLOR = 1,
	PIXEL = 2,
};

#define LW_COLOR_R(c) (BYTE)((c)&0xff)
#define LW_COLOR_G(c) (BYTE)(((c) >> 8) & 0xff)
#define LW_COLOR_B(c) (BYTE)(((c) >> 16) & 0xff)
#define LW_COLOR_A(c) (BYTE)(((c) >> 24) & 0xff)

enum class lwPlayPoseEnum : DWORD {
	ONCE = 1,
	LOOP = 2,
	FRAME = 3,
	ONCE_SMOOTH = 4,
	LOOP_SMOOTH = 5,
	PAUSE = 6,
	CONTINUE = 7,
	INVALID = 0,
};
DEFINE_BITMASK_OPERATORS(lwPlayPoseEnum)

enum class KeyframeType : DWORD {
	BEGIN = 0,
	END = 1,
	KEY = 2,
	INVALID = 0xffffffff,
};
DEFINE_BITMASK_OPERATORS(KeyframeType)

enum class ObjType : DWORD {
	MODEL = 0,
	CHARACTER = 1,
	ITEM = 2,
	INVALID = 0xffffffff,
};
DEFINE_BITMASK_OPERATORS(ObjType)

struct lwPickInfo {
	DWORD obj_id;
	DWORD face_id;
	float dis;
	lwVector3 pos;
	DWORD data;
};

struct lwPoseInfo {
	DWORD charType;
	DWORD start;
	DWORD end;
	DWORD key_frame_seq[MAX_KEY_FRAME_NUM];
	DWORD key_frame_num;
};

using lwPoseKeyFrameProc = void (*)(KeyframeType type, DWORD pose_id, DWORD key_id, DWORD key_frame, void* param);

enum class lwPlayPoseInfoMask : DWORD {
	POSE = 0x0001,
	TYPE = 0x0002,
	VELOCITY = 0x0004,
	FRAME = 0x0008,
	PROC = 0x0010,
	DATA = 0x0020,
	BLENDINFO = 0x0100,
	BLENDINFO_SRCFRAMENUM = 0x0200,
	BLENDINFO_DSTFRAMENUM = 0x0400,

	INVALID = 0x0000,
	DEFAULT = (POSE | TYPE | VELOCITY | FRAME | DATA),
	_ABSOLUTE = (DEFAULT | PROC),
};
DEFINE_BITMASK_OPERATORS(lwPlayPoseInfoMask)

//enum
//{
//    POSEBLEND_STATE_INVALID =       0x0000,
//    POSEBLEND_STATE_BLEND =         0x0001,
//    POSEBLEND_STATE_SRCFRAMENUM =   0x0002,
//    POSEBLEND_STATE_DSTFRAMENUM =   0x0004,
//    POSEBLEND_STATE_PARTIALBONE =   0x0008,
//};

struct lwPoseBlendInfo {
	DWORD op_state;
	float op_frame_length;
	float weight;
	float factor;
	float src_frame_num;
	float dst_frame_num;
	BYTE* bone_data;
	//
	DWORD blend_pose;
	lwPlayPoseEnum blend_type;
	float blend_frame;
	float blend_ret_frame;
};

struct lwPlayPoseInfo {
	lwPlayPoseInfoMask bit_mask;

	DWORD pose;
	lwPlayPoseEnum type;
	lwPlayPoseEnum data;
	float velocity;
	float frame;
	float ret_frame;
	lwPoseKeyFrameProc proc;
	void* proc_param;
	lwPoseBlendInfo blend_info;
};

//struct lwPoseKeyFrameInfo
//{
//    DWORD type;
//    DWORD pose_id;
//    DWORD key_id;
//    DWORD key_frame;
//    DWORD param;
//};

inline void lwPlayPoseInfo_Construct(lwPlayPoseInfo* info) {
	memset(info, 0, sizeof(lwPlayPoseInfo));
	info->bit_mask = lwPlayPoseInfoMask::INVALID;
}

struct lwColorValue4f {
	float r;
	float g;
	float b;
	float a;
};

inline void lwColorValue4f_Construct(lwColorValue4f* obj, float r, float g, float b, float a) {
	obj->r = r;
	obj->g = g;
	obj->b = b;
	obj->a = a;
}

inline lwColorValue4f lwColorValue4f_Construct(float r, float g, float b, float a) {
	lwColorValue4f c;
	c.r = r;
	c.g = g;
	c.b = b;
	c.a = a;
	return c;
}

struct lwColorValue4b {
	union {
		struct
		{
			BYTE b;
			BYTE g;
			BYTE r;
			BYTE a;
		};

		DWORD color;
	};
};

struct lwMaterial {
	lwColorValue4f dif;
	lwColorValue4f amb;
	lwColorValue4f spe;
	lwColorValue4f emi;
	float power;
};

inline void lwMaterial_Construct(lwMaterial* mtl) {
	memset(mtl, 0, sizeof(lwMaterial));
	mtl->amb.a = mtl->amb.r = mtl->amb.g = mtl->amb.b = 1.0f;
	mtl->dif.a = mtl->dif.r = mtl->dif.g = mtl->dif.b = 1.0f;
}

struct lwRenderStateValue {
	DWORD state;
	DWORD value;
};

inline lwRenderStateValue RS_VALUE(DWORD state, DWORD value) {
	lwRenderStateValue ret = {state, value};
	return ret;
}

class lwIItem;
struct lwItemLinkInfo {
	lwIItem* obj;
	DWORD id;
	DWORD link_item_id;
	DWORD link_parent_id;
};

struct lwResFile {
	char file_name[LW_MAX_PATH];
	lwResourceFileType res_type{lwResourceFileType::INVALID};
	DWORD obj_id{0};

	lwResFile() {
		file_name[0] = '\0';
	}
};

struct lwResFileMesh {
	char file_name[LW_MAX_PATH];
	lwResourceFileType res_type{lwResourceFileType::INVALID};
	DWORD obj_id{0};

	lwResFileMesh() {
		file_name[0] = '\0';
	}

	int Compare(const lwResFileMesh* src) {
		return (obj_id == src->obj_id) && (res_type == src->res_type) && (_tcscmp(file_name, src->file_name) == 0);
	}
};

struct lwResFileAnimData {
	char file_name[LW_MAX_PATH];
	lwResourceFileType res_type{lwResourceFileType::INVALID};
	DWORD obj_id{0};
	AnimCtrlType anim_type{AnimCtrlType::INVALID};

	lwResFileAnimData() {
		file_name[0] = '\0';
	}

	int Compare(const lwResFileAnimData* src) {
		return (obj_id == src->obj_id) && (anim_type == src->anim_type) && (res_type == src->res_type) && (_tcscmp(file_name, src->file_name) == 0);
	}
};

enum lwAnimCtrlAgentSetType {
	ACA_GLOBAL_SET,
	ACA_SUBSET_SET,
};

using lwOutputLoseDeviceProc = LW_RESULT (*)();
using lwOutputResetDeviceProc = LW_RESULT (*)();

// -------------------
// for lwAnimKeySetPRS2
enum class lwAnimKeySetMask : DWORD {
	KEY = 0x0001,
	DATA = 0x0002,
	SLERPTYPE = 0x0004,
};
DEFINE_BITMASK_OPERATORS(lwAnimKeySetMask)

enum class lwAnimKeySetSlerpType : DWORD {
	INVALID = 0,

	begin = 0,

	LINEAR,
	SIN1,  // 0 - 90
	SIN2,  // 90 - 180
	SIN3,  // 180 - 270
	SIN4,  // 270 - 360
	COS1,  // 0 - 90
	COS2,  // 90 - 180
	COS3,  // 180 - 270
	COS4,  // 270 - 360
	TAN1,  // 0 - 45
	CTAN1, // 0 - 45

	end
};
DEFINE_BITMASK_OPERATORS(lwAnimKeySetSlerpType)

struct lwKeyFloat {
	using DATA_TYPE = float;

	DWORD key;
	lwAnimKeySetSlerpType slerp_type;
	DATA_TYPE data;
};
struct lwKeyVector3 {
	DWORD key;
	lwAnimKeySetSlerpType slerp_type;
	lwVector3 data;
};

struct lwKeyQuaternion {
	DWORD key;
	lwAnimKeySetSlerpType slerp_type;
	lwQuaternion data;
};
struct lwKeyMatrix43 {
	DWORD key;
	DWORD slerp_type;
	lwMatrix43 data;
};
// end

enum class lwStreamTypeEnum : DWORD {
	STATIC = 0,
	DYNAMIC = 1,
	LOCKABLE = 2,
	GENERIC = 3,
};

struct lwWndInfo {
	HWND hwnd;
	DWORD left;
	DWORD top;
	DWORD width;
	DWORD height;
	DWORD windowed_style;
};

struct lwWatchDevVideoMemInfo {
	int alloc_vb_size;
	int alloc_ib_size;
	int alloc_tex_size;
	int alloc_vb_cnt;
	int alloc_ib_cnt;
	int alloc_tex_cnt;
};

enum class lwModelEntityTypeEnum : DWORD {
	PRIMITIVE = 1,
	BONECTRL = 2,
	DUMMY = 3,
	HELPER = 4,
	LIGHT = 5,
	CAMERA = 6,

	INVALID = LW_INVALID_INDEX,
};

enum class lwTreeNodeProcTypeEnum : DWORD {
	PREORDER,
	INORDER,
	POSTORDER,
	PREORDER_IGNORE,
};

enum class lwTreeNodeProcRetTypeEnum : DWORD {
	CONTINUE = 0,
	ABORT = 1,
	IGNORECHILDREN = 2,

	FAILED = LW_INVALID_INDEX,
};

class lwITreeNode;
using lwTreeNodeEnumProc = lwTreeNodeProcRetTypeEnum (*)(lwITreeNode* node, void* param);

enum class lwModelObjectLoadType : DWORD {
	RESET,
	MERGE,  // exclude dummy root
	MERGE2, // include merge dummy root
};

enum class lwModelNodeQueryMask : DWORD {
	ID = 0x0001,
	TYPE = 0x0002,
	DATA = 0x0004,
	DESCRIPTOR = 0x0008,
	USERPROC = 0x0010,
};
DEFINE_BITMASK_OPERATORS(lwModelNodeQueryMask)

struct lwModelNodeQueryInfo {
	lwModelNodeQueryMask mask;
	DWORD id;
	lwModelEntityTypeEnum type;
	void* data;
	char descriptor[64];
	lwTreeNodeEnumProc proc;
	void* proc_param;
	lwITreeNode* node;
};

enum class lwSurfaceStreamType : DWORD {
	RENDERTARGET = 1,
	DEPTHSTENCIL = 2,
	INVALID = LW_INVALID_INDEX,
};

struct lwD3DCreateParam {
	UINT adapter;
	D3DDEVTYPE dev_type;
	HWND hwnd;
	DWORD behavior_flag;
	D3DPRESENT_PARAMETERS present_param;
};

struct lwD3DCreateParamAdjustInfo {
	D3DMULTISAMPLE_TYPE multi_sample_type;
};

enum class lwOptionByteFlag : DWORD {
	CREATEHELPERPRIMITIVE = 0,
	CULLPRIMITIVE_MODEL = 1,
	MAX_OPTION_BYTE_FLAG = 64,
};

enum class lwRenderStateAtomType : DWORD {
	RS = 0,
	TSS = 1,
	SS = 2,
	INVALID = LW_INVALID_INDEX,
};

using lwTimerProc = void (*)(DWORD time); // 1/1000.0f

using lwDirectoryBrowserProc = LW_RESULT (*)(const char* file_path, const WIN32_FIND_DATA* wfd, void* param);

enum class lwDirBrowserType : DWORD {
	FILE = 0x0001,
	DIRECTORY = 0x0002,
	INVALID = 0,
};
DEFINE_BITMASK_OPERATORS(lwDirBrowserType)

using lwThreadProc = unsigned int(__stdcall*)(void* param);

enum class lwThreadPoolType : DWORD {
	LOADRES = 0,
#ifdef DYNAMIC_LOADING
	LOADPHY = 1,
#endif
	SIZE,
};

enum class lwCriticalSectionType : DWORD {
	SYSMEMTEX = 0,
};

struct lwDwordByte4 {
	union {
		DWORD d;
		BYTE b[4];
	};
};

struct lwVertexBufferInfo {
	DWORD size;
	DWORD usage;
	DWORD fvf;
	DWORD stride;
	D3DPOOL pool;
};

struct lwIndexBufferInfo {
	DWORD size;
	DWORD usage;
	DWORD format;
	DWORD stride;
	D3DPOOL pool;
};

class lwIPrimitive;
using lwTranspPrimitiveProc = LW_RESULT (*)(lwIPrimitive* obj, void* param);

struct lwSceneFrameInfo {
	DWORD _update_count;
	DWORD _render_count;
	DWORD _tex_cpf;
};

//TODO: Make anything that uses this safe
enum class OptResMgr : DWORD {
	LOADTEXTURE_MT = 1,
	LOADMESH_MT = 2,
	CREATE_ASSISTANTOBJECT = 3,
	TEXENCODE = 5,

	BYTESET_SIZE
};

enum class TimerHit : DWORD {
	ADDITIVE = 1,
	FILTER = 2,
};

enum class AssObjMask : DWORD {
	SIZE = 0x0001,
	COLOR = 0x0002,
};
DEFINE_BITMASK_OPERATORS(AssObjMask)

struct lwAssObjInfo {
	lwVector3 size;
	DWORD color;
};

struct lwStaticStreamMgrDebugInfo {
	DWORD vbs_size;
	DWORD vbs_used_size;
	DWORD vbs_free_size;
	DWORD vbs_unused_size;
	DWORD vbs_locked_size;
	DWORD ibs_size;
	DWORD ibs_used_size;
	DWORD ibs_free_size;
	DWORD ibs_unused_size;
	DWORD ibs_locked_size;
};

#define DX_VER_MAJOR(id) (id << 16)
#define DX_VER_MINOR(id) (id << 8)
enum DXVersion {
	DX_VERSION_INVALID = 0,

	DX_VERSION_8_0 = DX_VER_MAJOR(8),
	DX_VERSION_8_1 = DX_VER_MAJOR(8) + DX_VER_MINOR(1),
	DX_VERSION_8_1_a = DX_VER_MAJOR(8) + DX_VER_MINOR(1) + 1,
	DX_VERSION_8_1_b = DX_VER_MAJOR(8) + DX_VER_MINOR(1) + 2,
	DX_VERSION_8_2 = DX_VER_MAJOR(8) + DX_VER_MINOR(2),

	DX_VERSION_9_0 = DX_VER_MAJOR(9),
	DX_VERSION_9_0_a = DX_VER_MAJOR(9) + 1,
	DX_VERSION_9_0_b = DX_VER_MAJOR(9) + 2,
	DX_VERSION_9_0_c = DX_VER_MAJOR(9) + 3,

	DX_VERSION_UNKNOWN = 0x0007ffff,
	DX_VERSION_X_X = 0xffffffff,

};

struct lwDxVerInfo {
	DWORD version;
	DWORD revision;
};

struct lwCullPriInfo {
	DWORD total_cnt;
	DWORD culling_cnt;
};

enum class lwBackBufferFormatsItemMaxType : DWORD {
	MULTISAMPLE = 0,
	DEPTHSTENCIL,
	RENDERTARGET,
	TEXTURE,
	CUBETEXTURE,
	VOLUMETEXTURE,

	MAX_MULTISAMPLE = 16,
	MAX_DEPTHSTENCIL = 8,
	MAX_RENDERTARGET = 8,
	MAX_TEXTURE = 32,
	MAX_CUBETEXTURE = 32,
	MAX_VOLUMETEXTURE = 32,
};

struct lwBackBufferFormatsItemInfo {
	BOOL windowed;
	D3DFORMAT format;
	D3DFORMAT fmt_multisample[(DWORD)lwBackBufferFormatsItemMaxType::MAX_MULTISAMPLE];
	D3DFORMAT fmt_depthstencil[(DWORD)lwBackBufferFormatsItemMaxType::MAX_DEPTHSTENCIL];
	D3DFORMAT fmt_rendertarget[(DWORD)lwBackBufferFormatsItemMaxType::MAX_RENDERTARGET];
	D3DFORMAT fmt_texture[(DWORD)lwBackBufferFormatsItemMaxType::MAX_TEXTURE];
	D3DFORMAT fmt_cubetexture[(DWORD)lwBackBufferFormatsItemMaxType::MAX_CUBETEXTURE];
	D3DFORMAT fmt_volumetexture[(DWORD)lwBackBufferFormatsItemMaxType::MAX_VOLUMETEXTURE];
};

struct lwBackBufferFormatsInfo {
	lwBackBufferFormatsItemInfo fmt_seq[4];
};

enum class LoadingResType : DWORD {
	RUNTIME = 1,
	RUNTIME_MT = 2
};

enum class LoadingResMask : DWORD {
	NONE = 0x0000,
	RT0 = 0x0001,
	
	RTMT0 = 0x0001,
	RTMT1 = 0x0002,
	LOADSM_FAILED = 0x0010,
	LOADVM_FAILED = 0x0020,

	RTMTREG_FAILED = 0x0100
};
DEFINE_BITMASK_OPERATORS(LoadingResMask)

enum class ResPass : DWORD {
	DEFAULT = 0,
	SKIPTHISDRAW = 1,
	_ERROR = 0xFFFFFFFF
};

enum lwRenderCtrlVSTypesEnum {
	FIXEDFUNCTION = 1, //RENDERCTRL_GENERIC =       0x0001,
	VERTEXBLEND = 2,   //RENDERCTRL_VSBONE =        0x0002,
	VERTEXBLEND_DX9 = 3,

	// user defined render ctrl index based from 256 - 512
	USER = 0x100,

	INVALID = LW_INVALID_INDEX
};

#define LW_ARGB_A(v) (((v) >> 24) / 255.0f)
#define LW_ARGB_R(v) ((((v)&0x00ff0000) >> 16) / 255.0f)
#define LW_ARGB_G(v) ((((v)&0x0000ff00) >> 8) / 255.0f)
#define LW_ARGB_B(v) (((v)&0x000000ff) / 255.0f)

#if (defined LW_USE_DX9)
struct lwVertexShaderInfo9 {
	BYTE* data;
	DWORD size;
	IDirect3DVertexShaderX* handle;
};
typedef lwVertexShaderInfo9 lwVertexShaderInfo;

struct lwVertDeclInfo9 {
	D3DVERTEXELEMENT9* data;
	IDirect3DVertexDeclarationX* handle;
};
typedef lwVertDeclInfo9 lwVertDeclInfo;

#endif

enum class VSFile : DWORD {
	OBJECT = 1,
	ASM = 2,
	HLSL = 3,
};

class lwAnimCtrlTypeInfo {
public:
	AnimCtrlType type{AnimCtrlType::INVALID};
	DWORD data[4]{
		LW_INVALID_INDEX,
		LW_INVALID_INDEX,
		LW_INVALID_INDEX,
		LW_INVALID_INDEX
	};

	BOOL Check(const lwAnimCtrlTypeInfo* info) {
		return (type == info->type && data[0] == info->data[0] && data[1] == info->data[1]);
	}
};
using lwAnimCtrlObjTypeInfo = lwAnimCtrlTypeInfo;

using lwHeapCompProc = BOOL (*)(const void*& i, const void*& j);
using lwHeapFilterProc = void (*)(const void*& i, DWORD j);

enum class lwFileStreamOpenInfoTypeAdapter : DWORD {
	// adapter flag
	FILE = 0x0001,
	PACKET = 0x0002, // now invalid
};

enum class lwFileStreamOpenInfoType : DWORD {
	// access flag
	FS_ACCESS_READ = ACCESS_READ,
	FS_ACCESS_WRITE = ACCESS_WRITE,

	// create flag
	FS_CREATE_NEW = CREATE_NEW,
	FS_CREATE_ALWAYS = CREATE_ALWAYS,
	FS_OPEN_EXISTING = OPEN_EXISTING,
	FS_OPEN_ALWAYS = OPEN_ALWAYS,
	FS_TRUNCATE_EXISTING = TRUNCATE_EXISTING,

	// attributes flag
	FS_SEQUENTIAL_SCAN = FILE_FLAG_SEQUENTIAL_SCAN,

	// seek
	FS_FILE_BEGIN = FILE_BEGIN,
	FS_FILE_CURRENT = FILE_CURRENT,
	FS_FILE_END = FILE_END,
};

struct lwFileStreamOpenInfo {
	lwFileStreamOpenInfoTypeAdapter adapter_type;
	lwFileStreamOpenInfoType access_flag;
	lwFileStreamOpenInfoType create_flag;
	lwFileStreamOpenInfoType attributes_flag;
};

enum class lwResStateEnum : DWORD {
	INVALID = 0x000,
	INIT = 0x0001,
	SYSTEMMEMORY = 0x0002,
	VIDEOMEMORY = 0x0004,
	LOADTEST = 0x0010,
	LOADTEST_0 = 0x0020,
	LOADING_VM = 0x1000,
};
DEFINE_BITMASK_OPERATORS(lwResStateEnum)

enum class lwViewFrustumPlaneEnum : DWORD {
	TOP = 0,
	BOTTOM = 1,
	LEFT = 2,
	RIGHT = 3,
	FRONT = 4,
	BACK = 5,
};

LW_END