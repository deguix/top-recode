
#ifndef _ALGO_H_
#define _ALGO_H_

unsigned char const* sha1(unsigned char const* msg, unsigned int len, unsigned char* md);

bool dbpswd_out(char const* ctxt, int ctxt_len, std::string& pswd);
bool dbpswd_in(char const* pswd, int pswd_len, std::string& ctxt);

std::string sha512string(std::string msg);

#include <string>

#define KCHAP_SESSION_KEY_LEN 6

class KCHAPc {
public:
	KCHAPc(const std::string name, const std::string password);
	~KCHAPc();

	std::string GetPwdDigest() const {
		return m_strPwdDig;
	}
	void SetChapString(std::string chap_string);
	std::string GetStep1Data();
	std::string GetSessionClrKey(std::string encrypted_string);

private:
	std::string m_strName{""};
	std::string m_strPwd{""};
	std::string m_strChapString{""};

	unsigned char skey[KCHAP_SESSION_KEY_LEN] = {0};

public:
	std::string m_strPwdDig;
};

class KCHAPs {
public:
	KCHAPs(const std::string passwd_digest = "");
	~KCHAPs();

	bool DoAuth(const std::string player_name, const std::string data,
				const std::string pdat, const std::string key);
	void NewSessionKey();
	std::string GetSessionClrKey();
	std::string GetSessionEncKey();

private:
	std::string m_strName{""};
	std::string m_strPwdDig{""};
	std::string m_strChapString{""};

	unsigned char skey[KCHAP_SESSION_KEY_LEN]{0};
};

#endif
