//
#include "lwPathInfo.h"
#include "lwStdInc.h"

LW_BEGIN

LW_STD_IMPLEMENTATION(lwPathInfo)

lwPathInfo::lwPathInfo() {
}

std::string lwPathInfo::SetPath(lwPathInfoType type, std::string path) {
	_path_buf[(DWORD)type] = path;
	return _path_buf[(DWORD)type];
}

std::string lwPathInfo::GetPath(lwPathInfoType type) {
	return _path_buf[(DWORD)type];
}

// lwOptionMgr
LW_STD_IMPLEMENTATION(lwOptionMgr)

lwOptionMgr::lwOptionMgr() {
	memset(_byte_flag_seq, 0, sizeof(_byte_flag_seq));

	_ignore_model_tex_flag = 0;
}
lwOptionMgr::~lwOptionMgr() {
}

LW_END