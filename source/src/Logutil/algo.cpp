#include "util.h"
#include "algo.h"
#include <openssl/evp.h>
#include <sstream>
#include <iomanip>
#include "rng.h"

#include <string.h>				/* strlen               */
#include <openssl/core_names.h> /* OSSL_KDF_*           */
#include <openssl/params.h>		/* OSSL_PARAM_*         */
#include <openssl/thread.h>		/* OSSL_set_max_threads */
#include <openssl/kdf.h>		/* EVP_KDF_*            */
#include <openssl/rand.h>

//TODO: Accept an encryption from text file.
bool dbpswd_out(char const* ctxt, int ctxt_len, std::string& pswd) {
	pswd = ctxt;
	return true;
}

bool dbpswd_in(char const* pswd, int pswd_len, std::string& ctxt) {
	ctxt = pswd;
	return true;
}

//////////////////////////////////////////////////////////////////////////
//
// Message Digest 5
//
std::string DES_ecb(const std::string key, const std::string data, bool encrypt = true) {
	EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();

	unsigned char buf[EVP_MAX_MD_SIZE];
	int md_len, out_len;
	unsigned char key_cu[EVP_MAX_MD_SIZE];
	unsigned char data_cu[EVP_MAX_MD_SIZE];
	unsigned char iv[EVP_MAX_MD_SIZE] = {"abcdefghijklmnopqrstuvwxyz"};

	std::copy_n(key.begin(), min(key.length(), EVP_MAX_MD_SIZE), key_cu);
	std::copy_n(data.begin(), min(data.length(), EVP_MAX_MD_SIZE), data_cu);

	if (encrypt) {
		EVP_EncryptInit_ex(ctx, EVP_des_ecb(), nullptr, key_cu, iv);
		EVP_EncryptUpdate(ctx, buf, &md_len, data_cu, static_cast<int>(data.length()));
		EVP_EncryptFinal_ex(ctx, buf + md_len, &out_len);
	} else {
		EVP_DecryptInit_ex(ctx, EVP_des_ecb(), nullptr, key_cu, iv);
		EVP_DecryptUpdate(ctx, buf, &md_len, data_cu, static_cast<int>(data.length()));
		EVP_DecryptFinal_ex(ctx, buf + md_len, &out_len);
	}
	EVP_CIPHER_CTX_cleanup(ctx);

	return std::string(reinterpret_cast<char*>(buf), out_len);
}

std::string sha512string(std::string input) {
	//Taken from: http://ivbel.blogspot.com/2012/02/how-to-calculate-md5-hash-value-using.html adapted to updated example in OpenSSL 1.1.0 manual with SHA512.

	EVP_MD_CTX* mdctx;
	unsigned char md_value[EVP_MAX_MD_SIZE] = {0};
	unsigned int md_len, i;

	/* You can pass the name of another algorithm supported by your version of OpenSSL here */
	/* For instance, MD2, MD4, SHA1, RIPEMD160 etc. Check the OpenSSL documentation for details */
	const EVP_MD* md = EVP_sha512();
	md_len = EVP_MD_size(md);

	if (!md) {
		return "";
	}

	mdctx = EVP_MD_CTX_new();
	EVP_DigestInit_ex(mdctx, md, nullptr);
	EVP_DigestUpdate(mdctx, input.c_str(), input.length());
	/* to add more data to hash, place additional calls to EVP_DigestUpdate here */
	EVP_DigestFinal_ex(mdctx, md_value, &md_len);
	EVP_MD_CTX_free(mdctx);

	///* Now output contains the hash value, output_len contains length of output */
	//Taken from: https://stackoverflow.com/questions/11181251/saving-hex-values-to-a-c-string
	std::ostringstream buffer;
	buffer << std::hex << std::setfill('0') << std::uppercase;
	for (i = 0; i < md_len; i++)
		buffer << std::setw(2) << static_cast<int>(md_value[i]);
	return buffer.str();
}

std::pair<std::string, std::string> Argon2dstring(const std::string input) {
	std::ostringstream buffer, buffer2;
	int i;
	// Adapted from: https://man.archlinux.org/man/EVP_KDF-ARGON2.7ssl.en

	EVP_KDF* kdf = nullptr;
	EVP_KDF_CTX* kctx = nullptr;
	OSSL_PARAM params[6], *p = params;
	/* argon2 params, please refer to RFC9106 for recommended defaults */
	uint32_t lanes = 2, threads = 2, memcost = 65536;
	std::string pwd = input;
	const size_t inlen = 64;
	unsigned char salt[inlen];
	RAND_bytes(salt, inlen);
	/* derive result */
	const size_t outlen = 128;
	unsigned char result[outlen];
	/* required if threads > 1 */
	if (OSSL_set_max_threads(nullptr, threads) != 1)
		goto fail;
	p = params;
	*p++ = OSSL_PARAM_construct_uint32(OSSL_KDF_PARAM_THREADS, &threads);
	*p++ = OSSL_PARAM_construct_uint32(OSSL_KDF_PARAM_ARGON2_LANES,
									   &lanes);
	*p++ = OSSL_PARAM_construct_uint32(OSSL_KDF_PARAM_ARGON2_MEMCOST,
									   &memcost);
	*p++ = OSSL_PARAM_construct_octet_string(OSSL_KDF_PARAM_SALT,
											 salt,
											 strlen((const char*)salt));
	*p++ = OSSL_PARAM_construct_octet_string(OSSL_KDF_PARAM_PASSWORD,
											 pwd.data(),
											 pwd.length());
	*p++ = OSSL_PARAM_construct_end();
	if ((kdf = EVP_KDF_fetch(nullptr, "ARGON2ID", nullptr)) == nullptr)
		goto fail;
	if ((kctx = EVP_KDF_CTX_new(kdf)) == nullptr)
		goto fail;
	if (EVP_KDF_derive(kctx, &result[0], outlen, params) != 1)
		goto fail;
	// printf("Output = %s\n", OPENSSL_buf2hexstr(result, outlen));

	///* Now output contains the hash value, output_len contains length of output */
	// Taken from: https://stackoverflow.com/questions/11181251/saving-hex-values-to-a-c-string
	buffer << std::hex << std::setfill('0') << std::uppercase;
	for (i = 0; i < outlen; i++)
		buffer << std::setw(2) << static_cast<int>(result[i]);
	EVP_KDF_free(kdf);
	EVP_KDF_CTX_free(kctx);
	OSSL_set_max_threads(NULL, 0);

	buffer2 << std::hex << std::setfill('0') << std::uppercase;
	for (i = 0; i < inlen; i++)
		buffer2 << std::setw(2) << static_cast<int>(salt[i]);

	return std::make_pair(buffer.str(), buffer2.str());
fail:
	EVP_KDF_free(kdf);
	EVP_KDF_CTX_free(kctx);
	OSSL_set_max_threads(NULL, 0);
	return std::make_pair("", "");
}

//////////////////////////////////////////////////////////////////////////
//
// KOP CHAP class (client and server)
//

KCHAPc::KCHAPc(const std::string name, const std::string password) {
	m_strName = name;
	m_strPwd = password;
	m_strPwdDig = sha512string(password);
}

KCHAPc::~KCHAPc() {
}

void KCHAPc::SetChapString(std::string chap_string) {
	m_strChapString = chap_string;
}

std::string KCHAPc::GetStep1Data() { // size is set to EVP_MAX_MD_SIZE
	return DES_ecb(m_strPwdDig, m_strChapString);
}

std::string KCHAPc::GetSessionClrKey(std::string encrypted_string) {
	return DES_ecb(m_strPwdDig, encrypted_string, false);
}

KCHAPs::KCHAPs(const std::string passwd_digest) {
	m_strPwdDig = passwd_digest;
}

KCHAPs::~KCHAPs() {
}

bool KCHAPs::DoAuth(const std::string player_name, const std::string data,
					const std::string pdat, const std::string key) {
	// 保存相关数据
	m_strName = player_name;
	m_strPwdDig = key;
	m_strChapString = data;

	// compare
	bool ret = (pdat.compare(DES_ecb(m_strPwdDig, m_strChapString)) == 0) ? true : false;
	if (ret) {
		// 认证成功，则立即产生会话密钥
		NewSessionKey();
	}

	return ret;
}

void KCHAPs::NewSessionKey() {
	RAND_bytes(skey, KCHAP_SESSION_KEY_LEN);
}

std::string KCHAPs::GetSessionClrKey() {
	return std::string(reinterpret_cast<const char*>(skey), KCHAP_SESSION_KEY_LEN);
}

std::string KCHAPs::GetSessionEncKey() {
	return DES_ecb(m_strPwdDig, m_strChapString);
}