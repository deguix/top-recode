@echo off

echo Compiling top-recode game sources
echo ----------------------------------------
echo Usage: compile_deps.bat ARCH BUILD_METHOD REBUILD_METHOD
echo - Compiles/Updates all dependencies.
echo - Requires admin rights only for first run only (SDL requires it).
echo - OpenSSL requires Strawberry Perl and NASM installed and on PATH
echo for the compilation to work.

set "ICUVERSION=74"
set "BUILDICU=y"
set "BUILDOPENSSL=y"
set "DEBUG="
set "BUILD_METHOD=%2"
set "GIT_PULL=n"
set "ARCH=%1"
set "ARCH_BUILD=Win32"
set "REBUILD_METHOD=%3"
set "OPENSSL_BUILD=WIN32"

:params_parse
if "%ARCH%"=="" (
	goto :exit
)
if "%BUILD_METHOD%"=="" (
	goto :exit
)
if "%REBUILD_METHOD%"=="" (
	goto :exit
)


if "%BUILD_METHOD%"=="Debug" (
	set "DEBUG=debug"
)
if "%ARCH%"=="x64" (
	set "ARCH_BUILD=x64"
	set "OPENSSL_BUILD=WIN64A"
)

rem Check if this bat file is going to be self-modified by a git pull
rem if "%GIT_PULL%"=="y" (
rem 	echo Git pulling to make sure you're using latest files
rem 
rem 	call git diff --exit-code %~f0
rem 	if "%ERRORLEVEL%" NEQ "0" (
rem 		echo This script gets modified on git pull. Git pulling on continue.
rem 		echo Please, restart this script once it finishes.
rem 		pause
rem 		call git pull
rem 		goto :exit
rem 	) else (
rem 		call git pull
rem 	)
rem )

rem From https://stackoverflow.com/questions/4051883/batch-script-how-to-check-for-admin-rights
set "ADMIN=n"
net session >nul 2>&1
if "%errorLevel%" == "0" (
    set "ADMIN=y"
)

set "FILE="
set "SOURCE_FILE="

echo Loading Microsoft Visual Studio 2022 variables

:: Script to detect proper VS folder
if exist "%PROGRAMFILES%\Microsoft Visual Studio\2022\Community" (
	set "MYEDITION=Community"
) else (
	if exist "%PROGRAMFILES%\Microsoft Visual Studio\2022\Professional" (
		set "MYEDITION=Professional"
	) else (
		if exist "%PROGRAMFILES%\Microsoft Visual Studio\2022\Enterprise" set "MYEDITION=Enterprise"
	)
)

rem Strips quotes from paths in %PATH% - https://stackoverflow.com/questions/33116937/call-with-spaces-and-parenthesis-in-path
@set "PATHTEMP=%PATH%"
@set PATH=%PATHTEMP:"=%

rem if block fixes running vcversall many times in a row in same cmd prompt window.
if not defined DevEnvDir (
    call "%PROGRAMFILES%\Microsoft Visual Studio\2022\%MYEDITION%\VC\Auxiliary\Build\vcvarsall.bat" %ARCH%
)

:: (fix for Windows 8+) Change directory to the one script is on
cd /d %~dp0 

call git clone https://gist.github.com/540829265469e673d045.git randutils

echo Cloning luajit
call git clone http://luajit.org/git/luajit-2.0.git luajit
cd luajit
if not exist "src\" (
	call git checkout v2.1
) else (
	call git pull
)
cd src
copy /y "..\..\compatlj52-msvcbuild.bat" "."

echo Compiling luajit
call compatlj52-msvcbuild.bat %DEBUG%
cd ..

mkdir lib
cd lib
mkdir %ARCH_BUILD%
cd %ARCH_BUILD%
mkdir %BUILD_METHOD%
cd %BUILD_METHOD%
copy /y "..\..\..\src\lua51.dll" "."
copy /y "..\..\..\src\lua51.lib" "."
copy /y "..\..\..\src\lua51.pdb" "."
copy /y "..\..\..\src\luajit.lib" "."
copy /y "..\..\..\src\luajit.exe" "."
copy /y "..\..\..\src\luajit.pdb" "."
copy /y "..\..\..\src\luajit.ilk" "."
cd ..\..\..

mkdir inc
cd inc
copy /y "..\src\lauxlib.h" "."
copy /y "..\src\lua.h" "."
copy /y "..\src\lua.hpp" "."
copy /y "..\src\luaconf.h" "."
copy /y "..\src\luajit.h" "."
copy /y "..\src\lualib.h" "."
cd ..\..

rem if "%BUILDSDL%"=="n" (
rem 	goto SDLSYMBOLICLINKS
rem )
echo Cloning sdl
call git clone https://github.com/libsdl-org/SDL.git sdl
cd sdl
call git pull
rem call git fetch
rem call git checkout remotes/origin/maint/maint-%ICUVERSION%

echo Compiling sdl
cd VisualC
devenv /%REBUILD_METHOD% "%BUILD_METHOD%|%ARCH_BUILD%" ".\SDL.sln" /Project "SDL3"
cd ..\..


rem if "%BUILDSDLMIXER%"=="n" (
rem 	goto SDLMIXERSYMBOLICLINKS
rem )
echo Cloning sdl_mixer
call git clone https://github.com/libsdl-org/SDL_mixer.git sdl_mixer
cd sdl_mixer
call git pull
rem call git fetch
rem call git checkout remotes/origin/maint/maint-%ICUVERSION%

echo Compiling sdl_mixer
cd external
rmdir /q sdl
call :MKJUNCTION_DIFFNAME ".\sdl" "..\..\sdl"
cd ..
cd VisualC
devenv /%REBUILD_METHOD% "%BUILD_METHOD%|%ARCH_BUILD%" ".\SDL_mixer.sln" /Project "SDL3_mixer"
cd ..\..


rem https://unicode-org.github.io/icu/userguide/icu4c/faq.html#opening-icu-services-fails-with-u_missing_resource_error-and-u_init-returns-failure
rem Why am I seeing a small ( only a few K ) instead of a large ( several megabytes ) data shared library (icudt)?
rem Opening ICU services fails with U_MISSING_RESOURCE_ERROR and u_init() returns failure.

rem Who designs a project to bootstrap AND to be built again to get the proper library?

if "%BUILDICU%"=="n" (
	goto SKIPICU
)
echo Cloning icu
call git clone https://github.com/unicode-org/icu.git icu
cd icu
call git fetch
call git checkout remotes/origin/maint/maint-%ICUVERSION%

echo Compiling icu (this will take a long time!)
cd icu4c\source\allinone
devenv /Rebuild "%BUILD_METHOD%|%ARCH_BUILD%" ".\allinone.sln"
devenv /Rebuild "%BUILD_METHOD%|%ARCH_BUILD%" ".\allinone.sln" /Project "makedata"
devenv /Build "%BUILD_METHOD%|%ARCH_BUILD%" ".\allinone.sln" /Project "makedata"
cd ..\..\..\..

:SKIPICU
if "%BUILDOPENSSL%"=="n" (
	goto SKIPOPENSSL
)
echo Cloning openssl
call git clone https://github.com/openssl/openssl.git openssl
cd openssl
call git pull
rem call git checkout remotes/origin/maint/maint-%ICUVERSION%

echo Compiling openssl (this will take a very long time!)
call perl Configure VC-%OPENSSL_BUILD%
call nmake clean
call nmake
call nmake test
mkdir lib
cd lib
mkdir %ARCH_BUILD%
cd %ARCH_BUILD%
copy /y "..\..\libssl-3%OPENSSLPOSTFIX%.dll" "."
copy /y "..\..\libssl-3%OPENSSLPOSTFIX%.pdb" "."
copy /y "..\..\libssl-3%OPENSSLPOSTFIX%.ilk" "."
copy /y "..\..\libssl.lib" "."
copy /y "..\..\libssl.def" "."
copy /y "..\..\libssl-static.lib" "."

copy /y "..\..\libcrypto-3%OPENSSLPOSTFIX%.dll" "."
copy /y "..\..\libcrypto-3%OPENSSLPOSTFIX%.pdb" "."
copy /y "..\..\libcrypto-3%OPENSSLPOSTFIX%.ilk" "."
copy /y "..\..\libcrypto.lib" "."
copy /y "..\..\libcrypto.def" "."
copy /y "..\..\libcrypto-static.lib" "."
cd ..\..
mkdir inc
cd inc
mkdir %ARCH_BUILD%
cd %ARCH_BUILD%
mkdir openssl
cd openssl
copy /y "..\..\..\include\openssl\*.h" "."
cd ..\..\..
call nmake clean
cd ..

:SKIPOPENSSL
echo Cloning enum_bitmask
call git clone https://github.com/Reputeless/EnumBitmask enum_bitmask
cd enum_bitmask
if not exist "enum_bitmask.hpp" (
	call git checkout master
) else (
	call git pull
)
cd ..

echo Finished.
:exit
@echo on
exit /B

rem Procedure definitions

rem Makes additional checks on mklink to make sure script doesn't spit errors non-stop
rem Also prevents script execution if files are missing
rem Also deletes old link if admin to make sure x86/x64 links are appropriate
:MKJUNCTION_DIFFNAME
	if exist %1 (
		if %ADMIN%==y (
			rmdir /S /Q %1
			mklink /J %1 %2
		)
	) else (
		if %ADMIN%==n (
			goto :FILE_DOESNT_EXIST
		) else mklink /J %1 %2
	)
	exit /B

rem https://stackoverflow.com/questions/1645843/resolve-absolute-path-from-relative-path-and-or-file-name
:NORMALIZEPATH
  set "RETVAL=%~dpfn1"
  exit /B

:FILE_DOESNT_EXIST
	call :NORMALIZEPATH %2
	echo "File %RETVAL% doesn't exist! Run the script as admin for the symlinking to work!"
	pause
	exit